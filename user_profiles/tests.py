from django.test import TestCase
from django.test.client import Client
from django.contrib.auth.models import User
from django.core.urlresolvers import reverse
from user_profiles.models import Profile, Alias, get_alias_type, get_user_type
from user_profiles.utils import reformat_id

from django.core import mail

import validictory
from validictory.validator import FieldValidationError
from user_profiles.schemata import UserSchema, UserListSchema, \
    AliasedUserSchema, ProfilePutSchema
from user_profiles.utils import get_auth_token_for_alias
from user_profiles.forms import RegisterForm

import json
import os  # for removing dead avatars


class UserProfileTest(TestCase):

    def setUp(self):
        u = User(username="test_user")
        u.save()
        profile = Profile(identity=u, avatar="terrible.jpg", signature="Test")
        profile.save()

        u = User(username="jane_doe")
        u.save()
        profile = Profile(identity=u)
        profile.save()

        u = User(username="john_doe")
        u.save()

        self.client = Client()

    def test_user_profile(self):
        try:
            response = self.client.get("/user/profiles/dne/")
            self.assertEqual(response.status_code, 404)

            response = self.client.get("/user/profiles/test_user/")
            data = json.loads(response.content.decode('utf-8'))
            validictory.validate(data, UserSchema)
            self.assertTrue(True, msg="Valid User Profile Data")
            self.assertEqual(data['name'], "test_user")
            self.assertEqual(data['avatar'], "http://localhost/~emallson/forumite_media/terrible.jpg")
            self.assertEqual(data['signature'], "Test")

            response = self.client.get("/user/profiles/jane_doe/")
            data = json.loads(response.content.decode('utf-8'))
            validictory.validate(data, UserSchema)
            self.assertTrue(True, msg="Valid User Profile Data - 2")
            self.assertEqual(data['name'], "jane_doe")
            self.assertFalse('avatar' in data, "Empty Avatar field not added")
            self.assertFalse('signature' in data, "Empty Signature field not added")

            response = self.client.get("/user/profiles/john_doe/")
            data = json.loads(response.content.decode('utf-8'))
            validictory.validate(data, UserSchema)
            self.assertTrue(True, msg="Valid User Profile Data w/o Explicit Creation")
        except TypeError:
            self.assertTrue(False, msg="Bad response")
        except FieldValidationError:
            self.assertTrue(False, msg="Invalid User Profile Data")

    def test_user_profiles(self):
        try:
            response = self.client.post("/user/profiles/", {'usernames': json.dumps(['test_user', 'jane_doe', 'dne', ])})
            data = json.loads(response.content.decode('utf-8'))
            validictory.validate(data, UserListSchema)
            self.assertTrue(True, msg="Valid User List Data")
        except TypeError:
            self.assertTrue(False, msg="Bad response")
        except FieldValidationError:
            self.assertTrue(False, msg="Invalid User List Data")


class UserAliasTest(TestCase):

    def test_aliased_profile(self):
        c = Client()
        response = c.get("/user/profiles/user1601/")
        self.assertEqual(response.status_code, 404, msg="Alias is not treated as username")

        try:
            response = c.get("/user/aliases/lol/user/")
            self.assertEqual(response.status_code, 200, msg="Alias exists - LoL")
            data = json.loads(response.content.decode('utf-8'))
            validictory.validate(data, AliasedUserSchema)
            self.assertTrue(True, msg="Response matches schema")

            self.assertEqual(data['name'], "User")
            self.assertEqual(data['avatar'], "http://localhost/~emallson/forumite_media/avatars/2013/10/08/buildings_have_eyes_1.png")
            self.assertEqual(data['signature'], "Trolololol")

            response = c.get("/user/aliases/d3/user9001/")
            self.assertEqual(response.status_code, 200)
            data = json.loads(response.content.decode('utf-8'))
            validictory.validate(data, AliasedUserSchema)
            self.assertTrue(True, msg="Response matches schema")

            self.assertEqual(data['name'], "User#9001")
            self.assertEqual(data['avatar'], "http://localhost/~emallson/forumite_media/avatars/2013/10/08/Avatar_of_Grenth_concept_art_1.jpg")
            self.assertEqual(data['signature'], "Trolololol")

            response = c.get("/user/aliases/forumite/user/")
            self.assertEqual(response.status_code, 404)

            response = c.post("/user/aliases/lol/",
                              {'usernames': json.dumps(['User', 'Atlanis'])})
            self.assertEqual(response.status_code, 200)
            data = json.loads(response.content.decode('utf-8'))
            self.assertTrue("Atlanis" in data)
            self.assertTrue("User" in data)
            self.assertFalse("User#9001" in data)

            response = c.post("/user/aliases/d3/",
                             {'usernames': json.dumps(['User#9001', 'Atlanis#1601'])})
            self.assertEqual(response.status_code, 200)
            data = json.loads(response.content.decode('utf-8'))
            self.assertFalse("Atlanis#1601" not in data)
            self.assertTrue("User#9001" in data)

        except TypeError:
            self.assertTrue(False, msg="Bad response")
        except FieldValidationError:
            self.assertTrue(False, msg="Invalid User Data")


class ProfileViewTest(TestCase):

    AVATAR = "./user_profiles/test/avatar.jpg"

    def setUp(self):
        user = User.objects.get(username='user')
        self.user_profile = Profile.objects.get(identity_pk=user.pk,
                                                identity_type=get_user_type())

    def test_without_login(self):
        c = Client()
        response = c.get('/user/profile/')
        self.assertEqual(response.status_code, 302)

    def test_content(self):
        c = Client()
        c.login(username="user", password="password")
        response = c.get('/user/profile/')
        self.assertEqual(response.status_code, 200)
        self.assertEqual(response.context['user'].username, "user")
        self.assertEqual(len(response.context['aliases']), 3)
        self.assertEqual(response.context['profile'].avatar.url, "http://localhost/~emallson/forumite_media/avatars/2013/10/08/Avatar_of_Grenth_concept_art_1.jpg")
        self.assertEqual(response.context['path'], '/user/profile/')

    def test_alias_forms(self):
        c = Client()

        # check that it requires login
        response = c.post('/user/profile/put/4/', {
        })
        self.assertEqual(response.status_code, 302)

        c.login(username="user", password="password")

        signature = "Unit test"

        # check that bad data is rejected
        with open(self.AVATAR, "rb") as avatar:
            response = c.post('/user/profile/put/4/', {
                'avatar': avatar,
                'signature': signature*100
            })
            self.assertEqual(response.status_code, 400)

        with open(self.AVATAR, "rb") as avatar:
            response = c.post('/user/profile/put/4/', {
                'avatar': avatar,
                'signature': signature,
            })
            self.assertEqual(response.status_code, 200)

            # check that the response follows the schema
            try:
                data = json.loads(response.content.decode('utf-8'))
                validictory.validate(data, ProfilePutSchema)
                self.assertTrue(True)
            except:
                self.assertTrue(False, "Schema validation failed")

            # check that the alias still exists
            try:
                Alias.objects.get(forum=1, name='User')
                self.assertTrue(True)
            except Alias.DoesNotExist:
                self.assertTrue(False)

            # check that the profile was updated
            try:
                p = Profile.objects.get(signature=signature,
                                        avatar__endswith='avatar.jpg')
                os.remove(p.avatar.path)
                self.assertTrue(True)
            except Profile.DoesNotExist:
                self.assertTrue(False)

        # chech that cannot update other user's aliases
        response = c.post('/user/profile/put/1/', {
            'signature': signature,
        })
        self.assertEqual(response.status_code, 403)

        try:
            p = Profile.objects.get(pk=1)
            self.assertNotEqual(p.signature, signature)
        except Profile.DoesNotExist:
            self.assertTrue(False, msg="Fixture data missing")

    def test_alias_delete(self):
        c = Client()
        # attempt to delete without logging in
        response = c.get(reverse('user_profiles.views.alias_delete',
                                 args=(4,)))
        self.assertEqual(response.status_code, 403)

        c.login(username='user', password='password')

        # pk doesn't exist
        response = c.get(reverse('user_profiles.views.alias_delete',
                                 args=(1337,)))
        self.assertEqual(response.status_code, 404)

        # delete a valid alias
        response = c.get(reverse('user_profiles.views.alias_delete',
                                 args=(4,)))
        self.assertEqual(response.status_code, 200)

        try:
            Alias.objects.get(pk=4)
            self.assertTrue(False, msg="Alias not actually deleted")
        except Alias.DoesNotExist:
            self.assertTrue(True)

        try:
            Profile.objects.get(identity_type=get_alias_type(),
                                identity_pk=4)
            self.assertTrue(False, msg="Profile not actually deleted")
        except Profile.DoesNotExist:
            self.assertTrue(True)

        # delete another user's alias
        response = c.get(reverse('user_profiles.views.alias_delete',
                                 args=(1,)))
        self.assertEqual(response.status_code, 403)

        try:
            Alias.objects.get(pk=1)
            self.assertTrue(True)
        except Alias.DoesNotExist:
            self.assertTrue(False, msg="Alias actually deleted")


class AuthUserTest(TestCase):
    def setUp(self):
        self.alias = Alias.objects.get(pk=5)
        self.post_url = "/providers/forumite/post/123456/"
        self.auth_token = '53407eef41e4eac9f71103a154640174'
        self.auth_response = {
            'author': "User",
            'id': "123456",
            'content': "Some random crap\n" +
            "[auth]" + self.auth_token + "[/auth]"
        }

    def test_hash(self):
        self.assertEqual(self.alias.auth_hash,
                         self.auth_token)

    def test_check_auth(self):
        c = Client()
        response = c.get(self.post_url)
        self.assertEqual(response.status_code, 200)
        self.assertEqual(json.loads(response.content.decode('utf-8')),
                         self.auth_response)

        self.assertEqual(
            get_auth_token_for_alias(self.auth_response['content']),
            self.auth_token
        )

    def test_authenticate(self):
        c = Client()
        c.login(username='user', password='password')
        # returns 400 on: already authenticated, bad data, missing token,
        # mismatching usernames

        # bad data
        response = c.post('/user/alias/auth/', {})
        self.assertEqual(response.status_code, 400)
        # already auth'd
        response = c.post('/user/alias/auth/', {
            'alias': 1,
            'post_id': 654321
        })
        self.assertEqual(response.status_code, 400)
        self.assertFalse(Alias.objects.get(pk=self.alias.pk).authenticated)
        # missing token
        response = c.post('/user/alias/auth/', {
            'alias': self.alias.pk,
            'post_id': 321456,
        })
        self.assertEqual(response.status_code, 400)
        self.assertFalse(Alias.objects.get(pk=self.alias.pk).authenticated)
        # mismatching username
        response = c.post('/user/alias/auth/', {
            'alias': self.alias.pk,
            'post_id': 654321
        })
        self.assertEqual(response.status_code, 400)
        self.assertFalse(Alias.objects.get(pk=self.alias.pk).authenticated)
        # good data
        response = c.post('/user/alias/auth/', {
            'alias': self.alias.pk,
            'post_id': 123456,
        })
        self.assertEqual(response.status_code, 200)
        self.assertTrue(Alias.objects.get(pk=self.alias.pk).authenticated)


class RegistrationTest(TestCase):
    def test_bad_data(self):
        f = RegisterForm({
            'username': 'badbadbadbadbadbadbadbadbadbadbad',  # too long
            'password': 'blahblah',
            'repeat_password': 'blahblah',
            'email': 'email@example.com'
            })
        self.assertFalse(f.is_valid())

        f = RegisterForm({
            'username': 'good_user',
            'password': 'blahblah',
            'repeat_password': 'blahblahblah',  # mismatch
            'email': 'email@example.com'
        })
        self.assertFalse(f.is_valid())

        f = RegisterForm({
            'username': 'good_user',
            'password': 'blahblah',
            'repeat_password': 'blahblah',
            'email': 'oops'  # not an email
        })
        self.assertFalse(f.is_valid())

        f = RegisterForm({
            'username': 'user',  # duplicate name
            'password': 'blahblah',
            'repeat_password': 'blahblah',
            'email': 'email@example.com'
        })
        self.assertFalse(f.is_valid())

        f = RegisterForm({
            'username': 'user2',
            'password': 'blahblah',
            'repeat_password': 'blahblah',
            'email': 'user@example.com'  # duplicate email
        })
        self.assertFalse(f.is_valid())

    def test_save(self):
        f = RegisterForm({
            'username': 'user2',
            'password': 'blahblah',
            'repeat_password': 'blahblah',
            'email': 'user@example.com'  # duplicate email
        })

        self.assertEqual(f.save(), None)
        try:
            user = User.objects.get(username='user2')
            self.assertTrue(False, msg="Should not have saved.")
        except User.DoesNotExist:
            self.assertTrue(True)

        f = RegisterForm({
            'username': 'another_user',
            'password': 'password',
            'repeat_password': 'password',
            'email': 'an_email@gmail.com'
        })

        f.save()

        try:
            user = User.objects.get(username='another_user',
                                    email='an_email@gmail.com')
            self.assertTrue('password', user.password)
        except User.DoesNotExist:
            self.assertTrue(False, msg="Save failed")

    def test_view(self):
        c = Client()

        url = reverse('user_profiles.views.registration')
        response = c.get(url)
        self.assertEqual(response.status_code, 200)

        # bad data
        response = c.post(url, {
            'username': 'user2',
            'password': 'blahblah',
            'repeat_password': 'blahblah',
            'email': 'user@example.com'  # duplicate email
        })
        self.assertEqual(response.status_code, 200)

        # good data
        response = c.post(url, {
            'username': 'another_user',
            'password': 'password',
            'repeat_password': 'password',
            'email': 'an_email@gmail.com'
        })
        self.assertEqual(response.status_code, 302)

        userExists = False
        try:
            user = User.objects.get(username='another_user')
            self.assertTrue(user.check_password('password'))
            userExists = user is not None
        except User.DoesNotExist:
            userExists = False

        self.assertTrue(userExists, msg='POST save failed')

        self.assertEqual(len(mail.outbox), 1)
        self.assertEqual(mail.outbox[0].to[0], 'an_email@gmail.com')

    def test_complete(self):
        c = Client()

        f = RegisterForm({
            'username': 'another_user',
            'password': 'password',
            'repeat_password': 'password',
            'email': 'an_email@gmail.com'
        })
        u = f.save()

        response = c.get(reverse(
            'user_profiles.views.registration_complete',
            args=[reformat_id(u.pk)]
        ))
        self.assertEqual(response.status_code, 200)

        try:
            u = User.objects.get(username='another_user')
            self.assertTrue(u.is_active)
        except User.DoesNotExist:
            self.assertTrue(False, msg="WTF")

    def test_cancel(self):
        c = Client()

        f = RegisterForm({
            'username': 'another_user',
            'password': 'password',
            'repeat_password': 'password',
            'email': 'an_email@gmail.com'
        })
        u = f.save()

        response = c.get(reverse(
            'user_profiles.views.registration_cancel',
            args=[reformat_id(u.pk)]
        ))
        self.assertEqual(response.status_code, 200)

        try:
            u = User.objects.get(username='another_user')
            self.assertTrue(False, msg="User not deleted")
        except User.DoesNotExist:
            self.assertTrue(True)
