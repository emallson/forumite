from django.http import HttpResponse, Http404
from django.core.urlresolvers import reverse
from django.contrib.auth.models import User
from django.contrib.contenttypes.models import ContentType
from user_profiles.models import Profile, Alias
from frontend.models import Forum
from django.contrib.auth.decorators import login_required
from django.contrib.auth import login as auth_login, logout as auth_logout
from user_profiles.forms import AliasForm, ProfileForm, AliasAuthForm, \
    RegisterForm, LoginForm, ProfileEditForm
from user_profiles.utils import get_auth_token_for_alias, \
    send_confirmation_mail, reformat_id, get_global_profile
from data_provider.decorators import allow_origin, allow_methods
from django.shortcuts import render_to_response
from django.template import RequestContext
from django.views.decorators.csrf import csrf_exempt
from django.shortcuts import redirect
from django.db import IntegrityError

import urllib
import json


def make_user_profile(user_or_alias, profile):
    if type(user_or_alias) is User:
        data = {
            'id': user_or_alias.id,
            'name': user_or_alias.username,
        }
    else:
        data = {
            'name': user_or_alias.name,
        }
    if profile is not None:
        if profile.avatar:
            data['avatar'] = profile.avatar.url
        if profile.signature != '':
            data['signature'] = profile.signature

    return data


def user_profile(request, name=None):
    if name is None or name is '':
        raise Http404
    else:
        try:
            u = User.objects.get(username=name)
            p = Profile.objects.get(identity_pk=u.pk,
                                    identity_type=ContentType.objects.get_for_model(User))
        except User.DoesNotExist:
            raise Http404
        except Profile.DoesNotExist:
            p = None  # lacking a global profile is permissable
        return HttpResponse(json.dumps(make_user_profile(u, p)))


def user_profiles(request):
    if request.method == 'POST':
        usernames = json.loads(request.POST['usernames'])
        users = {}
        for username in usernames:
            try:
                u = User.objects.get(username=username)
                p = Profile.objects.get(identity_pk=u.pk,
                                        identity_type=ContentType.objects.get_for_model(User))
            except User.DoesNotExist:
                pass
            except Profile.DoesNotExist:
                p = None

            users[u.username] = make_user_profile(u, p)
        return HttpResponse(json.dumps(users))
    else:
        raise Http404


def aliased_profile(request, forum=None, name=None):
    if name is None or name is '' or forum is None or forum is '':
        raise Http404
    else:
        try:
            f = Forum.objects.get(slug=forum)
            alias = Alias.objects.get(forum=f, slug=name, authenticated=True)
            if alias.authenticated:
                return HttpResponse(
                    json.dumps(
                        make_user_profile(
                            alias,
                            alias.profile)))
            else:
                raise Http404
        except Forum.DoesNotExist:
            raise Http404
        except Alias.DoesNotExist:
            raise Http404


@csrf_exempt
@allow_origin('*')
@allow_methods('POST')
def aliased_profiles(request, forum=None):
    if request.method == 'POST':
        try:
            usernames = json.loads(request.POST['usernames'])
        except KeyError:
            raise Http404
        aliases = {}
        f = Forum.objects.get(slug=forum)
        for username in usernames:
            try:
                alias = Alias.objects.get(forum=f, name=username,
                                          authenticated=True)
                aliases[alias.name] = make_user_profile(alias, alias.profile)
            except Alias.DoesNotExist:
                aliases[username] = None
        return HttpResponse(json.dumps(aliases))
    else:
        raise Http404


@login_required
def profile_display(request):
    # get aliases for the user
    aliases = Alias.objects.filter(user=request.user)
    # get global profile if it exists
    profile = get_global_profile(request.user)
    return render_to_response('management/profile.html', {
        'aliases': [(alias, ProfileForm(instance=alias.alias_profile))
                    for alias in aliases],
        'user': request.user,
        'profile': profile,
        'global_profile_form': ProfileForm(instance=profile),
        'path': request.path,
        'authentication_url': request.build_absolute_uri(
            reverse('user_profiles.views.auth_alias')
        ),
        'add_alias_url': reverse('user_profiles.views.add_alias'),
    }, context_instance=RequestContext(request))


@login_required
def profile_edit(request):
    if request.method == 'POST':
        form = ProfileEditForm(request.POST)
        if form.is_valid():
            request.user.set_password(form.cleaned_data['password'])
            request.user.save()
            return redirect('user_profiles.views.profile_display')
    else:
        form = ProfileEditForm()
    return render_to_response('management/profile_edit.html', {
        'form': form,
    }, context_instance=RequestContext(request))


@login_required
def put_profile(request, pk):
    if request.method == 'POST':
        try:
            profile = Profile.objects.get(pk=pk)
        except Profile.DoesNotExist:
            raise Http404

        if profile.user != request.user:
            return HttpResponse(status=403)

        form = ProfileForm(request.POST, request.FILES, instance=profile)

        if form.is_valid():
            profile = form.save()
            return render_to_response('management/profile.json', {
                'profile': profile,
            })
        else:
            return HttpResponse(status=400)

    else:
        raise Http404


def alias_delete(request, pk):
    if not request.user.is_authenticated():
        return HttpResponse(status=403)
    else:
        try:
            alias = Alias.objects.get(pk=pk)
            if alias.user == request.user:
                alias.delete()
                return HttpResponse(status=200)
            else:
                return HttpResponse(status=403)
        except Alias.DoesNotExist:
            raise Http404


@allow_origin('*')
@allow_methods('POST')
def auth_alias(request):
    if request.method == 'POST':
        auth_form = AliasAuthForm(request.POST)
        if auth_form.is_valid():
            alias = auth_form.alias_object
            if not alias.authenticated:
                url = alias.forum.root + 'post/' + \
                      str(auth_form.cleaned_data['post_id']) + '/'
                try:
                    response = urllib.request.urlopen(url)
                    data = json.loads(response.read().decode('utf-8', errors='ignore'))

                    if data['author'] == alias.name and  \
                       data['id'] == auth_form.cleaned_data['post_id'] and \
                       get_auth_token_for_alias(data['content']) == alias.auth_hash:
                        alias.authenticated = True
                        alias.save()
                        return render_to_response('management/alias_put.json',
                                                  {
                                                      'alias': alias,
                                                  })
                except urllib.error.HTTPError as e:
                    print(e)
    return HttpResponse(status=400)


@login_required
def add_alias(request):
    alias = Alias(user=request.user)
    other_validation_errors = ''
    if request.method == 'POST':
        alias_form = AliasForm(request.POST, instance=alias)
        profile_form = ProfileForm(request.POST or None)

        if alias_form.is_valid():
            try:
                alias = alias_form.save()
                profile = Profile(identity=alias)
                profile_form = ProfileForm(request.POST, instance=profile)
                if profile_form.is_valid():
                    try:
                        profile = profile_form.save()
                        return redirect(reverse(profile_display))
                    except IntegrityError:
                        other_validation_errors = 'An error occured while attempting to create the profile'
                        alias.delete()
                alias.delete()
            except IntegrityError:
                other_validation_errors = 'You already have an alias for this forum or an alias already exists for this username.'
                if alias.pk:
                    alias.delete()
    else:
        alias_form = AliasForm(request.POST or None)
        profile_form = ProfileForm(request.POST or None)

    return render_to_response('management/alias_add.html', {
        'alias_form': alias_form,
        'profile_form': profile_form,
        'profile': get_global_profile(request.user),
        'other_validation_errors': other_validation_errors,
    }, context_instance=RequestContext(request))


def registration(request):
    if request.method == 'POST':
        form = RegisterForm(request.POST)
        if form.is_valid():
            user = form.save()
            send_confirmation_mail(user)
            return redirect('user_profiles.views.post_registration')
    else:
        form = RegisterForm()
    return render_to_response('registration/register.html', {
        'form': form,
    }, context_instance=RequestContext(request))


def post_registration(request):
    return render_to_response('registration/post_registration.html')


def registration_complete(request, user_hash):
    try:
        user = User.objects.get(pk=reformat_id(user_hash), is_active=False)
        user.is_active = True
        user.save()
        return render_to_response('registration/complete.html')
    except (User.DoesNotExist, OverflowError):
        raise Http404


def registration_cancel(request, user_hash):
    try:
        user = User.objects.get(pk=reformat_id(user_hash), is_active=False)
        user.delete()
        return render_to_response('registration/cancel.html')
    except (User.DoesNotExist, OverflowError):
        raise Http404


def login(request):
    if request.method == 'POST':
        form = LoginForm(request.POST)
        if form.is_valid():
            auth_login(request, form.user)
            return redirect(request.GET.get('next',
                                            reverse('user_profiles.views.profile_display')))
    else:
        form = LoginForm()
    return render_to_response('registration/login.html', {
        'form': form,
    }, context_instance=RequestContext(request))


def logout(request):
    auth_logout(request)
    return redirect(request.GET.get('next', '/'))
