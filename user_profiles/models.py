from django.contrib.auth.models import User
from django.contrib.contenttypes.models import ContentType
from django.contrib.contenttypes import generic
from frontend.models import Forum
from django.db import models
from django.utils.text import slugify
from django.db.models.signals import post_init
import hashlib


class Profile(models.Model):
    identity_type = models.ForeignKey(ContentType)
    identity_pk = models.PositiveIntegerField()
    identity = generic.GenericForeignKey('identity_type', 'identity_pk')

    avatar = models.ImageField(
        upload_to="avatars/%Y/%m/%d",
        blank=True
    )
    signature = models.CharField(blank=True, max_length=280)

    @property
    def user(self):
        if self.identity_type == get_alias_type():
            return self.identity.user
        else:
            return self.identity

    class Meta:
        unique_together = ("identity_pk", "identity_type")


class Alias(models.Model):
    user = models.ForeignKey(User)
    forum = models.ForeignKey(Forum)
    name = models.CharField(max_length=30)
    profile_manager = generic.GenericRelation(
        Profile,
        content_type_field='identity_type',
        object_id_field='identity_pk'
    )
    slug = models.SlugField(max_length=30)
    authenticated = models.BooleanField(default=False)

    class Meta:
        unique_together = [("forum", "name"), ("forum", "user")]

    class InnerProfile(object):
        def __init__(self, alias):
            self.alias = alias

        def __getattribute__(self, name):
            if name == "alias":
                return object.__getattribute__(self, name)
            if self.alias.alias_profile is None:
                if self.alias.user_profile is None:
                    return None
                else:
                    return getattr(self.alias.user_profile, name)
            else:
                attr = getattr(self.alias.alias_profile, name)
                if attr is None or attr == '':
                    if self.alias.user_profile is None:
                        return None
                    else:
                        return getattr(self.alias.user_profile, name)
                else:
                    return attr

    @property
    def user_profile(self):
        try:
            user_type = ContentType.objects.get_for_model(User)
            return Profile.objects.get(identity_pk=self.user.pk,
                                       identity_type=user_type)
        except Profile.DoesNotExist:
            return None

    @property
    def alias_profile(self):
        try:
            return self.profile_manager.get()
        except Profile.DoesNotExist:
            return None

    @property
    def auth_hash(self):
        m = hashlib.md5()
        m.update(self.name.encode('utf-8') + 
                 self.user.username.encode('utf-8') + 
                 self.forum.name.encode('utf-8'))
        return m.hexdigest()

    def save(self, *args, **kwargs):
        if self.slug is None or self.slug is '':
            self.slug = slugify(self.name)
        super(Alias, self).save(*args, **kwargs)


def alias_post_init(**kwargs):
    instance = kwargs.get('instance')
    setattr(instance, 'profile', Alias.InnerProfile(instance))

post_init.connect(alias_post_init, Alias)


def get_user_type():
    return ContentType.objects.get_for_model(User)


def get_alias_type():
    return ContentType.objects.get_for_model(Alias)
