from copy import deepcopy

UserSchema = {
    "type": "object",
    "properties": {
        "id": {
            "type": "integer",
        },
        "name": {
            "type": "string",
        },
    },
    "additionalProperties": {
        "avatar": {
            "type": "string",
        },
        "signature": {
            "type": "string",
        },
    },
    "required": ["id", "name", ],
}

UserListSchema = {
    "type": "object",
    "patternProperties": {
        "^.+$": UserSchema,
    },
}

AliasedUserSchema = deepcopy(UserSchema)
del AliasedUserSchema['properties']['id']
AliasedUserSchema['required'] = ['name', ]

AliasedUserListSchema = {
    "type": "object",
    "patternProperties": {
        "^.+$": AliasedUserSchema,
    },
}

ProfilePutSchema = {
    'type': 'object',
    'additionalProperties': {
        'avatar': {
            'type': 'string'
        },
        'signature': {
            'type': 'string'
        }
    }
}
