from django.contrib import admin
from django.contrib.auth.admin import UserAdmin
from django.contrib.auth.models import User

from user_profiles.models import Profile, Alias

admin.site.register(Profile)
admin.site.register(Alias)
