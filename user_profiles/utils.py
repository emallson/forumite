import re
from django.core import mail
from django.conf import settings
from django.core.urlresolvers import reverse

from user_profiles.models import Profile, Alias
from django.contrib.auth.models import User
from django.contrib.contenttypes.models import ContentType

auth_token_regex = re.compile(r"\[auth\](?P<token>[a-f0-9]{32})\[\/auth\]")


def get_auth_token_for_alias(response):
    token_match = auth_token_regex.search(response)
    if token_match is None:
        return None
    else:
        return token_match.group('token')


MESSAGE_TEMPLATE = """Hello, {user.username}:

This email is to inform you that this email has been registered successfully at http://forumite.atlanis.net.

To complete your registration and activate your account, please use the following link:

    http://forumite.atlanis.net{complete_link}

If you did not register this account, please click use the following link to cancel the registration:

    http://forumite.atlanis.net{cancel_link}

Thanks,
    The Forumite Team"""


def send_confirmation_mail(user):
    user_hash = reformat_id(user.pk)
    mail.send_mail("Forumite Registration Confirmation",
                   MESSAGE_TEMPLATE.format(
                       user=user,
                       complete_link=reverse('user_profiles.views.registration_complete',
                                             args=[user_hash]),
                       cancel_link=reverse('user_profiles.views.registration_cancel',
                                             args=[user_hash]),
                   ),
                   settings.EMAIL_REGISTRATION_FROM, (user.email,))


def reformat_id(id):
    if type(id) == str:
        return int(str(int(id, 16))[::-1])
    elif type(id) == int:
        return '{:x}'.format(int(str(id).zfill(32)[::-1]))
    else:
        return None


def get_global_profile(user):
    try:
        profile = Profile.objects.get(
            identity_type=ContentType.objects.get_for_model(User),
            identity_pk=user.pk
        )
    except Profile.DoesNotExist:
        profile = Profile(
            identity_type=ContentType.objects.get_for_model(User),
            identity_pk=user.pk
        )
        profile.save()

    return profile


def get_alias(user, forum):
    try:
        alias = Alias.objects.get(
            user=user,
            forum=forum
        )
    except Alias.DoesNotExist:
        alias = None

    return alias
