(function($, Forumite) {

    Forumite.Profile = {

        'displayEdit': function displayEdit(row) {
            row.find('.alias-display').hide();
            row.find('.alias-edit').show();
        },

        'displayDisplay': function displayDisplay(row) {
            row.find('.alias-edit').hide();
            row.find('.alias-display').show();
        },
        'updateDisplay': function updateDisplay(row, data) {
            row.find('.signature-display').text(
                data.signature
            );

            row.find('.avatar-display img').remove();
            row.find('.avatar-display').append($("<img>", {
                'src': data.avatar ||
                    $("#global-profile-row .avatar").prop('src'),
                'class': 'avatar'
            }));
        },
        'updateEdit': function updateEdit(row, data) {
            row.find('.signature-edit textarea').val(data.signature);

            var avatar_edit = row.find(".avatar-edit");
            avatar_edit.empty();

            if(data.hasOwnProperty('avatar')) {
                avatar_edit.append("Currently: ");
                avatar_edit.append($("<a>", {
                    'href': data.avatar
                }).text(data.avatar));
                avatar_edit.append($("<input>", {
                    'id': 'avatar-clear_id',
                    'name': 'avatar-clear',
                    'type': 'checkbox'
                }));
                avatar_edit.append($("<label>", {
                    'for': 'avatar-clear_id'
                }).text("Clear"));
            }
            avatar_edit.append('<br />Change: <input id="id_avatar" name="avatar" type="file" />');
        },
        'setupModal': function(modal, pk, hash) {
            modal.find("#authHash").val('[auth]' + hash + '[/auth]');
            modal.find("#aliasID").val(pk);
            modal.find("#authPostId").val('');
        },
        'removeAliasRow': function(row) {
            row.hide('fast');
            row.remove();
        },
        'setupDeleteModal': function(modal, row_id, action, forum_name) {
            modal.data('alias-row-id', row_id);
            modal.find("#delete-form").prop('action', action);
            modal.find("#forum-name").text(forum_name);
        },
        'init': function(authentication_url) {

            var response_queue = [];

            $("#content").removeClass("maximized-width").addClass("container");

            $(".edit").on('click', function() {
                var row = $(this).parents('.alias-row');
                Forumite.Profile.displayEdit(row);
            });

            $(".cancel").on('click', function() {
                var row = $(this).parents('.alias-row');
                Forumite.Profile.displayDisplay(row);
            });

            $(".save").on('click', function() {
                var row = $(this).parents('.alias-row');
                Forumite.Profile.displayDisplay(row);
                response_queue.push(row);
            });

            $(".alias-form").ajaxForm(function(data) {
                var row = response_queue.shift();
                data = JSON.parse(data);
                Forumite.Profile.updateDisplay(row, data);
                Forumite.Profile.updateEdit(row, data);
            });

            $(".authenticate").on('click', function() {
                var row = $(this).parents('.alias-row');
                Forumite.Profile.setupModal($("#authModal"),
                                            row.data('alias-id'),
                                           row.data('alias-hash'));
                $("#authModal").modal();
            });

            $("#auth-form").ajaxForm({
                'success': function(data) {
                    data = JSON.parse(data);
                    $("#authModal").modal('hide');
                    $("#authError").hide();
                    $("#authPostIdGroup").removeClass('has-error');
                    $("#alias-" + $("#aliasID").val() + " .authenticate").hide();
                },
                'error': function() {
                    $("#authError").show();
                    $("#authPostIdGroup").addClass('has-error');
                },
                'beforeSubmit': function(arr, $form) {
                    if(isNaN(arr[2].value) || arr[2].value === '') {
                        return false;
                    }
                    return true;
                }
            });

            $("#delete-form").ajaxForm({
                'success': function() {
                    $("#deleteModal").modal('hide');
                    Forumite.Profile.removeAliasRow(
                        $("#" + $("#deleteModal").data('alias-row-id')));
                },
                'error': function() {
                }
            });

            $(".alias-delete").on('click', function() {
                var row = $(this).parents('.alias-row');
                Forumite.Profile.setupDeleteModal(
                    $("#deleteModal"),
                    row.prop('id'),
                    $(this).data("delete-url"),
                    row.find('.forum-display strong').text()
                );
                $("#deleteModal").modal();
            });

            $("#delete-alert .cancel").on('click', function() {
                $("#delete-alert").hide('fast');
            });
        }
    };

}(jQuery, window.forumite = window.forumite || {}));