from django import forms
from user_profiles.models import Alias, Profile
from django.contrib.auth.models import User
from django.contrib.auth import authenticate
from django.utils import timezone


class AliasForm(forms.ModelForm):
    class Meta:
        model = Alias
        fields = ['name', 'forum']


class ProfileForm(forms.ModelForm):
    class Meta:
        model = Profile
        fields = ['avatar', 'signature']


class AliasAuthForm(forms.Form):
    alias = forms.IntegerField(widget=forms.HiddenInput)
    post_id = forms.CharField(required=True)

    @property
    def alias_object(self):
        if not self.is_valid():
            return None
        else:
            try:
                return Alias.objects.get(pk=self.cleaned_data['alias'])
            except Alias.DoesNotExist:
                return None


class ProfileEditForm(forms.Form):
    password = forms.CharField(
        min_length=6,
        widget=forms.PasswordInput(attrs={'class': 'form-control'})
    )
    repeat_password = forms.CharField(
        min_length=6,
        widget=forms.PasswordInput(attrs={'class': 'form-control'})
    )

    def clean(self):
        pw1 = self.cleaned_data.get('password')
        pw2 = self.cleaned_data.get('repeat_password')

        if pw1 != pw2:
            raise forms.ValidationError("Passwords do not match")

        return self.cleaned_data


class RegisterForm(forms.Form):
    username = forms.CharField(min_length=4, max_length=30, widget=forms.TextInput(attrs={'class': 'form-control'}))
    email = forms.EmailField(widget=forms.TextInput(attrs={'class': 'form-control'}))
    password = forms.CharField(min_length=6, widget=forms.PasswordInput(attrs={'class': 'form-control'}))
    repeat_password = forms.CharField(min_length=6, widget=forms.PasswordInput(attrs={'class': 'form-control'}))

    def clean(self):
        pw1 = self.cleaned_data.get('password')
        pw2 = self.cleaned_data.get('repeat_password')
        username = self.cleaned_data.get('username')
        email = self.cleaned_data.get('email')

        if pw1 != pw2:
            raise forms.ValidationError("Passwords do not match")

        try:
            user = User.objects.get(username=username)

            # not having the if feels dirty, even
            # though the following will always execute if
            # the if does
            if user is not None:
                raise forms.ValidationError("Username already exists")
        except User.DoesNotExist:
            pass

        try:
            user = User.objects.get(email=email)
            if user is not None:
                raise forms.ValidationError("Email already exists")
        except User.DoesNotExist:
            pass

        return self.cleaned_data

    def save(self):
        if self.is_valid():
            user = User(username=self.cleaned_data.get('username'),
                        email=self.cleaned_data.get('email'),
                        is_active=False,
                        date_joined=timezone.now())
            user.set_password(self.cleaned_data.get('password'))
            user.save()
            return user
        return None


class LoginForm(forms.Form):
    username = forms.CharField(min_length=4, max_length=30,
                               widget=forms.TextInput(attrs={'class': 'form-control'}))
    password = forms.CharField(min_length=6,
                               widget=forms.PasswordInput(attrs={'class': 'form-control'}))

    def clean(self):
        username = self.cleaned_data.get('username')
        password = self.cleaned_data.get('password')
        user = authenticate(username=username, password=password)

        if user is None:
            raise forms.ValidationError("Username or password incorrect")
        elif user.is_active is False:
            raise forms.ValidationError("This account has not been activated"
                                        "yet")
        else:
            self.user = user
