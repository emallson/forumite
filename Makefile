MINIFIER=yuicompressor

all: minjs cljs collect

collect:
	./manage.py collectstatic --noinput

minjs:
	cd user_profiles/static/js; $(MINIFIER) -o profile.min.js profile.js

cljs:
	cd interface; lein cljsbuild once production
	mkdir -p interface/static/js/
	cp interface/out/forumite.min.js interface/static/js/

test: test-django test-cljs

test-django:
	./manage.py test data_provider frontend user_profiles

test-cljs:
	cd interface; lein cljsbuild test

.PHONY: test-django test-cljs
