from django.conf.urls import patterns, include

urlpatterns = patterns(
    '',
    (r'^providers/', include('data_provider.urls')),
    (r'', include('frontend.urls')),
    (r'^user/', include('user_profiles.urls')),
)
