from django.test import TestCase
from django.test.client import Client
from data_provider.providers import ProviderBase, LoLProvider, test_urls
from data_provider.schemata import \
    BoardListSchema, ThreadListSchema, PostListSchema, PostSchema
import validictory
from validictory.validator import FieldValidationError

import json


class JSONProvider(ProviderBase):

    def genBoard(self, id, name):
        return {"id": str(id), "name": name}

    def boards(self):
        return {'0': self.genBoard(0, "Test")}

    def genThread(self, id, name, author):
        return {"name": "Test", "id": str(id), "author": author, "position": -1,
                "length": 1}

    def board(self, board_id, page_num):
        return {
            'threads': {
                '0': self.genThread(0, "Test", "Test"),
                '1': self.genThread(1, "Test2", "Test"),
            },
            'board_id': str(board_id),
            'page': page_num,
        }

    def genPost(self, id, content, author):
        return {"content": content, "id": str(id), "author": author, "position": -1}

    def thread(self, board_id, thread_id, page_num=1):
        return {
            'posts': {
                '0': self.genPost(0, "Test", "Test"),
            },
            'thread_id': str(thread_id),
            'board_id': str(board_id),
            'page': page_num,
        }

    def post(self, post_id):
        return self.genPost(str(post_id), "Test", "Test")


class IncompleteProvider(ProviderBase):
    pass


class SchemataTest(TestCase):

    def setUp(self):
        self.provider = JSONProvider()

    def test_board_schema(self):
        try:
            validictory.validate(self.provider.boards(),
                                 BoardListSchema)
            self.assertTrue(True)
        except FieldValidationError:
            self.assertTrue(False)

    def test_thread_schema(self):
        try:
            validictory.validate(self.provider.board(0, 1),
                                 ThreadListSchema)
            self.assertTrue(True)
        except FieldValidationError:
            self.assertTrue(False)

    def test_posts_schema(self):
        try:
            validictory.validate(self.provider.thread(0, 0, 1),
                                 PostListSchema)
            self.assertTrue(True)
        except FieldValidationError:
            self.assertTrue(False)

    def test_post_schema(self):
        try:
            validictory.validate(self.provider.post(1),
                                 PostSchema)
            self.assertTrue(True)
        except FieldValidationError:
            self.assertTrue(False)


class ProviderTest(TestCase):

    def setUp(self):
        self.lp = LoLProvider(urls=test_urls)

    def test_incomplete(self):
        self.assertRaises(TypeError, IncompleteProvider)

    def test_lol_boards(self):
        try:
            validictory.validate(self.lp.boards(), BoardListSchema)
            self.assertTrue(True, msg="Valid Board Data")
        except FieldValidationError:
            self.assertTrue(False, msg="Invalid Board Data")

    def test_lol_threads(self):
        try:
            validictory.validate(self.lp.board(0, 0), ThreadListSchema)
            self.assertTrue(True, msg="Valid Thread Data")
        except FieldValidationError:
            self.assertTrue(False, msg="Invalid Thread Data")

    def test_lol_posts(self):
        try:
            validictory.validate(self.lp.thread(0, 0), PostListSchema)
            self.assertTrue(True, msg="Valid Post Data")
        except FieldValidationError:
            self.assertTrue(False, msg="Invalid Post Data")

    def test_lol_post(self):
        try:
            validictory.validate(self.lp.post(40334921), PostSchema)
            self.assertTrue(True, msg="Valid Post Data")
        except FieldValidationError:
            self.assertTrue(False, msg="Invalid Post Data")


class RemoteProviderTest(TestCase):

    def setUp(self):
        self.client = Client()

    def test_lol_boards(self):
        try:
            response = self.client.get("/providers/lol_test/boards/")
            data = json.loads(response.content.decode('utf-8'))
            validictory.validate(data, BoardListSchema)
            self.assertTrue(True, msg="Valid Remote Board Data")
        except TypeError:
            self.assertTrue(False, msg="Bad response")
        except FieldValidationError:
            self.assertTrue(False, msg="Invalid Remote Board Data")

    def test_lol_threads(self):
        try:
            response = self.client.get("/providers/lol_test/board/1/")
            data = json.loads(response.content.decode('utf-8'))
            validictory.validate(data, ThreadListSchema)
            self.assertTrue(True, msg="Valid Remote Thread Data")
        except TypeError:
            self.assertTrue(False, msg="Bad response")
        except FieldValidationError:
            self.assertTrue(False, msg="Invalid Remote Thread Data")

    def test_lol_posts(self):
        try:
            response = self.client.get("/providers/lol_test/thread/1/")
            data = json.loads(response.content.decode('utf-8'))
            validictory.validate(data, PostListSchema)
            self.assertTrue(True, msg="Valid Remote Post Data")
        except TypeError:
            self.assertTrue(False, msg="Bad response")
        except FieldValidationError:
            self.assertTrue(False, msg="Invalid Remote Posts Data")

    def test_forumite_post(self):
        response = self.client.get("/providers/forumite/post/123456/")
        self.assertEqual(response.status_code, 200)

        try:
            data = json.loads(response.content.decode('utf-8'))
            validictory.validate(data, PostSchema)
            self.assertTrue(True)
        except:
            self.assertTrue(False, msg="Invalid Remote Post Data")
