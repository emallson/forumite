from django.http import HttpResponse, Http404
from django.shortcuts import render_to_response
from data_provider import providers
from data_provider.decorators import cache_control, allow_origin
from frontend.models import Forum
import json


@cache_control(120)  # temp value for cache time
def provider(request, forum_slug, data_type, **kwargs):
    try:
        if forum_slug == "lol_test":
            provider = providers.LoLProvider(urls=providers.test_urls)
        else:
            forum = Forum.objects.get(slug=forum_slug)
            provider = getattr(providers, forum.provider_name)()

        data = getattr(provider, data_type)(**kwargs)
        if data is None:
            raise Http404

        return HttpResponse(json.dumps(data))
    except Forum.DoesNotExist:
        raise Http404


@allow_origin('*')
def test_forumite_post(request, post_id=None):
    """ This functions is ONLY used for testing specific cases for which I don't have data elsewhere. """
    if post_id == "123456":
        return render_to_response('data/post.json', {
            'id': post_id,
            'name': 'User',
            'content': "Some random crap\\n[auth]53407eef41e4eac9f71103a154640174[/auth]"
        })
    elif post_id == "654321":
        return render_to_response('data/post.json', {
            'id': post_id,
            'name': 'Atlanis',
            'content': "blah",
        })
    elif post_id == "321456":
        return render_to_response('data/post.json', {
            'id': post_id,
            'name': 'User',
            'content': "blah",
        })
    else:
        return HttpResponse(status=404)
