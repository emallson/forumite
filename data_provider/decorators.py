import json


def simplejson(f):
    def wrapper(*args, **kwargs):
        return json.dumps(f(*args, **kwargs))
    return wrapper


class header:
    """Sets a header for the response produced by a view"""

    def __init__(self, header, value):
        self.header = header
        self.value = value

    def __call__(self, f):
        def wrapper(*args, **kwargs):
            response = f(*args, **kwargs)
            response[self.header] = self.value
            return response
        return wrapper


## some frequently-used header shortcuts ##

class allow_origin(header):
    """Sets the 'Access-Control-Allow-Origin' header on a response object"""

    def __init__(self, origin):
        super().__init__('Access-Control-Allow-Origin', origin)


class allow_methods(header):
    """Sets the 'Access-Control-Allow-Methods' header on a response object"""

    def __init__(self, method):
        super().__init__('Access-Control-Allow-Methods', method)


class cache_control(header):
    """Sets the 'Cache-Control' header on a response object. Defaults to public cache for 5 minutes."""

    def __init__(self, max_age=300, public=True):
        value = ""
        if max_age == 0:
            value = 'no-cache'
        else:
            value = 'max-age={0}'.format(max_age)

        if public:
            value += ', public'
        else:
            value += ', private'

        super().__init__('Cache-Control', value)
