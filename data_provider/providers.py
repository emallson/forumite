from abc import ABCMeta, abstractmethod
from lxml.cssselect import CSSSelector
import lxml.html as lh
import re
import math
import urllib.request as ulr
from urllib.error import HTTPError
from urllib.parse import unquote
from django.http import Http404


stripRe = re.compile(r'[\t\r\n]')

# source: http://stackoverflow.com/a/4624146/2529026
def stringify_children(node):
    from lxml.etree import tostring
    parts = ([node.text] + [tostring(c).decode('utf-8') for c in node.getchildren()] +
             [node.tail])
    # filter removes possible Nones in texts and tails
    return ''.join(filter(None, parts))


class ProviderBase(metaclass=ABCMeta):

    def fetch(self, url):
        """ Super simple, needs to be fleshed out """
        try:
            req = ulr.Request(url, headers={
                'User-Agent': 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/29.0.1547.65 Safari/537.36'
            })
            return ulr.urlopen(req).read().decode('utf-8', errors='ignore')
        except HTTPError as e:
            print(e)
            return ""

    @abstractmethod
    def boards(self):
        """ Collects a list of boards from the data store"""
        return

    @abstractmethod
    def board(self, data_id, page=1):
        """ Collects a list of boards from the data store
        along with the board containing them."""
        return

    @abstractmethod
    def thread(self, data_id, page=1):
        """ Collects a list of posts from the data store
        along with the board and thread containing them."""
        return

    @abstractmethod
    def post(self, data_id):
        """ Collects a single post from the data store. """
        return


class LoLProvider(ProviderBase):

    stripRe = re.compile(r'[\t\r\n]')
    urlRe = re.compile(r'(src|href)="((?!http://).+?)"')
    redirectRe = re.compile(r'"http://forums\.na\.leagueoflegends\.com/board/redirect\.php\?.*?redirect_url=([^"]+).*?"')

    boardSelectors = {
        'id': CSSSelector('div.default-2-3 h4 a'),
        'name': CSSSelector('div.default-2-3 h4 a')
    }
    threadSelectors = {
        'id': CSSSelector('a.thread-title-link'),
        'name': CSSSelector('a.thread-title-link'),
        'author': CSSSelector('td.author'),
        'page': CSSSelector('a.active'),
        'length': CSSSelector('td.replies span')
    }
    postSelectors = {
        'id': CSSSelector('div.content-border:not(.post-avatar):not(.margin-top-15)[id^="post"]'),
        'content': CSSSelector('div.post-message'),
        'author': CSSSelector('p.post-user')
    }

    def __init__(self, urls=None):
        if urls is not None:
            self.urls = urls
        else:
            self.urls = {
                'boards': 'http://forums.na.leagueoflegends.com/board/',
                'threads': 'http://forums.na.leagueoflegends.com/board/'
                'forumdisplay.php?f={board_id}&page={page}',
                'posts': 'http://forums.na.leagueoflegends.com/board/'
                'showthread.php?t={thread_id}&page={page}',
                'post': 'http://forums.na.leagueoflegends.com/board/'
                'showthread.php?p={post_id}'
            }

    def refactor_urls(self, content):
        content = self.urlRe.sub(r'\1="http://forums.na.leagueoflegends.com/board/\2"', content)
        content = self.redirectRe.sub(lambda url: unquote(url.group(1)),
                                      content)
        return content

    def boards(self):
        boards = {}

        html = lh.fromstring(self.fetch(self.urls['boards']))

        for (idTag, nameTag) in zip(self.boardSelectors['id'](html),
                                    self.boardSelectors['name'](html)):
            bid = str(idTag.get('href').split('f=')[1])
            boards[bid] = {
                'id': bid,
                'name': nameTag.text_content()
            }

        return boards

    def board(self, data_id, page=1):
        page_content = self.fetch(self.urls['threads'].format(
            board_id=data_id,
            page=page
        ))
        html = lh.fromstring(page_content)

        # find out what the real page number is
        # the lol forums don't care if you ask for a page > than one that
        # exists
        currentPageLinks = self.threadSelectors['page'](html)
        if len(currentPageLinks) > 0:
            page = int(currentPageLinks[0].text_content())
        else:
            page = 1

        position = 0
        threads = {}
        threads_outer = {
            'board_id': str(data_id),
            'page': int(page),
            'threads': threads
        }
        for (idTag, nameTag, authorTag, lengthTag) in zip(
                self.threadSelectors['id'](html),
                self.threadSelectors['name'](html),
                self.threadSelectors['author'](html),
                self.threadSelectors['length'](html)):
            tid = str(idTag.get('id').split('_')[2])
            position += 1

            try:
                length = math.ceil(
                    int(lengthTag.text_content().replace(',', '')) /
                    10.0
                )
            except ValueError:
                length = 1

            threads[tid] = {
                'id': tid,
                'name': stripRe.sub('', nameTag.text_content()),
                'author': authorTag.text_content(),
                'position': position,
                'length': length
            }
        return threads_outer

    def thread(self, data_id, page=1):
        page_content = self.fetch(self.urls['posts'].format(
            thread_id=data_id,
            page=page
        ))

        html = lh.fromstring(page_content)

        currentPageLinks = self.threadSelectors['page'](html)
        if len(currentPageLinks) > 0:
            page = int(currentPageLinks[0].text_content())
        else:
            page = 1

        position = 0
        posts = {}
        posts_outer = {
            'thread_id': str(data_id),
            'page': int(page),
            'posts': posts,
        }
        for (idTag, authorTag, contentTag) in zip(self.postSelectors['id'](html),
                                                  self.postSelectors['author'](html),
                                                  self.postSelectors['content'](html)):
            position += 1
            pid = str(idTag.get('id').split('post')[1])
            posts[pid] = {
                'id': pid,
                'author': stripRe.sub('', authorTag.text_content()),
                'content': self.refactor_urls(stripRe.sub('',
                                                               stringify_children(contentTag))),
                'position': position,
            }

        if posts:
            return posts_outer
        else:
            return None

    def post(self, data_id):
        page_content = self.fetch(self.urls['post'].format(
            post_id=data_id
        ))

        html = lh.fromstring(page_content)

        outer = html.cssselect('#edit' + str(data_id))
        if len(outer) == 0:
            return None
        else:
            outer = outer[0]
            idTag = self.postSelectors['id'](outer)[0]
            authorTag = self.postSelectors['author'](outer)[0]
            contentTag = self.postSelectors['content'](outer)[0]
            pid = str(idTag.get('id').split('post')[1])
            return {
                'id': pid,
                'author': stripRe.sub('',
                                           authorTag.text_content()),
                'content': stripRe.sub('',
                                            stringify_children(contentTag)),
            }


class D3Provider(ProviderBase):

    boardSelectors = {
        'id': CSSSelector('a.forum-link'),
        'name': CSSSelector('span.forum-title'),
    }
    threadSelectors = {
        'id': CSSSelector('tr.stickied-topic, tr.regular-topic'),
        'name': CSSSelector('a.topic-title span[itemprop="headline"]'),
        'author': CSSSelector('span.author-name[itemprop="author"]'),
        'page': CSSSelector('li.current a')
    }
    postSelectors = {
        'id': CSSSelector('div.topic-post'),
        'content': CSSSelector('div.post-detail[itemprop="text"]'),
        'author': CSSSelector('span.poster-name'),
        'page': CSSSelector('li.current a')
    }

    def __init__(self, urls=None):
        if urls is not None:
            self.urls = urls
        else:
            self.urls = {
                'boards': 'http://us.battle.net/d3/en/forum/',
                'threads': 'http://us.battle.net/d3/en/forum/{data_id}/'
                '?page={page}',
                'posts': 'http://us.battle.net/d3/en/forum/topic/{data_id}'
                '?page={page}'
            }

    def boards(self):
        boards = {}
        html = lh.fromstring(self.fetch(self.urls['boards']))

        found = False
        for (idTag, nameTag) in zip(self.boardSelectors['id'](html),
                                    self.boardSelectors['name'](html)):
            id = idTag.get('href')[0:-1]
            name = stripRe.sub('', nameTag.text_content())
            boards[id] = {'id': int(id), 'name': name}
            found = True

        if not found:
            raise Http404
        else:
            return boards

    def board(self, data_id, page=1):
        page_content = self.fetch(self.urls['threads'].format(
            data_id=data_id,
            page=int(page or 1)
        ))

        html = lh.fromstring(page_content)

        # find out what the real page number is
        # the lol forums don't care if you ask for a page > than one that
        # exists
        currentPageLinks = self.threadSelectors['page'](html)
        if len(currentPageLinks) > 0:
            page = currentPageLinks[0].get('data-pagenum')
        else:
            page = 1

        found = False
        threads = {'data_id': int(data_id), 'page': int(page), 'threads': {}}
        position = 1
        for (idTag, nameTag, authorTag) in zip(self.threadSelectors['id'](html),
                                               self.threadSelectors['name'](html),
                                               self.threadSelectors['author'](html)):
            id = idTag.get('data-topic-id')
            threads['threads'][id] = {
                'id': int(id),
                'name': stripRe.sub('', nameTag.text_content()),
                'author': authorTag.text_content(),
                'position': position,
            }
            position += 1
            found = True

        if not found:
            raise Http404
        else:
            return threads

    def thread(self, data_id, page=1):
        page_content = self.fetch(self.urls['posts'].format(
            data_id=data_id,
            page=int(page or 1)
        ))
        html = lh.fromstring(page_content)

        currentPageLinks = self.postSelectors['page'](html)
        if len(currentPageLinks) > 0:
            page = currentPageLinks[0].get('data-pagenum')
        else:
            page = 1

        found = False
        posts = {'data_id': int(data_id), 'page': int(page), 'posts': {}}
        position = 1
        for (idTag, authorTag, contentTag) in zip(self.postSelectors['id'](html),
                                                  self.postSelectors['author'](html),
                                                  self.postSelectors['content'](html)):
            id = idTag.get('data-post-id')
            posts['posts'][id] = {
                'id': int(id),
                'author': stripRe.sub('', authorTag.text_content()),
                'content': stripRe.sub('',
                                       stringify_children(contentTag)),
                'position': position
            }
            position += 1
            found = True

        if not found:
            raise Http404
        else:
            return posts

    def post(self, data_id):
        pass

class GW2Provider(ProviderBase):

    stripRe = re.compile(r'[\t\r\n]')

    boardSelectors = {
        "id": CSSSelector(".category a:not(.arrow)"),
        "name": CSSSelector(".category .forum h3")
    }

    threadSelectors = {
        'id': CSSSelector('div.topic a.topic'),
        'name': CSSSelector('div.topic a.topic'),
        'author': CSSSelector('div.topic span.creator a.member'),
        'page': CSSSelector('div.pagination .current')
    }

    @staticmethod
    def boardIdTransform(id):
        return '-'.join(id.split('/')[2:4])

    @staticmethod
    def boardIdTransformInv(id):
        return '/'.join(id.split('-'))

    def __init__(self, urls=None):
        if urls is not None:
            self.urls = urls
        else:
            self.urls = {
                "boards": "https://forum-en.guildwars2.com/forum",
                "board": "https://forum-en.guildwars2.com/forum/{url}/?page={page}",
            }

    def boards(self):
        boards = {}
        html = lh.fromstring(self.fetch(self.urls['boards']))

        for (idTag, nameTag) in zip(self.boardSelectors['id'](html),
                                    self.boardSelectors['name'](html)):
            id = self.boardIdTransform(idTag.get('href'))
            name = stripRe.sub('', nameTag.text_content())
            boards[id] = {'id': id, 'name': name}

        if not boards:
            raise Http404
        else:
            return boards

    def board(self, data_id, page=1):
        threads = {}
        html = lh.fromstring(self.fetch(self.urls['board'].format(
            url=self.boardIdTransformInv(data_id),
            page=page
        )))

        threads = {'data_id': data_id, 'page': int(page), 'threads': {}}
        position = 1
        for (idTag, nameTag, authorTag) in zip(self.threadSelectors['id'](html),
                                               self.threadSelectors['name'](html),
                                               self.threadSelectors['author'](html)):
            id = idTag.get('href').split('/')[-1]
            threads['threads'][id] = {
                'id': id,
                'name': stripRe.sub('', nameTag.text_content()),
                'author': stripRe.sub('', authorTag.text_content()),
                'position': position,
            }
            position += 1

        if not threads['threads']:
            raise Http404
        else:
            return threads

    def thread(self, data_id, page=1):
        pass

    def post(self, data_id):
        pass

test_urls = {
    'boards': 'http://localhost/~emallson/forumite/test/data/lol/boards.html',
    'threads': 'http://localhost/~emallson/forumite/test/data/lol/threads.html',
    'posts': 'http://localhost/~emallson/forumite/test/data/lol/posts.html',
    'post': 'http://localhost/~emallson/forumite/test/data/lol/posts.html'
}
