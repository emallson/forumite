from django.conf.urls import patterns

urlpatterns = patterns(
    'data_provider.views',
#    (r'lol_test/boards/', 'lol_test_boards'),
#    (r'lol_test/board/(?P<board_id>[0-9]+)(?:/(?P<page_num>[0-9]+))?/$',
#     'lol_test_threads'),
#    (r'lol_test/thread/(?P<thread_id>[0-9]+)(?:/(?P<page_num>[0-9]+))?/$',
#     'lol_test_posts'),
    (r'^forumite/post/(?P<post_id>[0-9]+)/$', 'test_forumite_post'),

    (r'(?P<forum_slug>[a-z0-9-_]+)/boards/$', 'provider', {'data_type':
                                                           'boards'}),
    (r'(?P<forum_slug>[a-z0-9-_]+)/(?P<data_type>board|thread|post)/'
     r'(?P<data_id>[\w-]+)/$', 'provider'),
    (r'(?P<forum_slug>[a-z0-9-_]+)/(?P<data_type>board|thread|post)/'
     r'(?P<data_id>[\w-]+)/(?P<page>\d+)/$', 'provider'),
)
