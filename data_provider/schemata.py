BoardSchema = {
    "title": "Board Schema",
    "type": "object",
    "properties": {
        "name": {
            "type": "string"
        },
        "id": {
            "type": "string"
        }
    },
    "required": ["name", "id"]
}

BoardListSchema = {
    "type": "object",
    "patternProperties": {
        "^([0-9]+)$": BoardSchema
    },
    "minProperties": 1
}

ThreadSchema = {
    "title": "Thread Schema",
    "type": "object",
    "properties": {
        "name": {
            "type": "string"
        },
        "id": {
            "type": "string"
        },
        "author": {
            "type": "string"
        },
        "position": {
            "type": "integer"
        },
        "length": {
            "type": "integer",
        }
    },
    "required": ["name", "id", "author", "length"]
}

ThreadListSchema = {
    "type": "object",
    "properties": {
        "threads": {
            "type": "object",
            "patternProperties": {
                "^[0-9]+$": ThreadSchema,
            },
        },
        "board_id": {
            "type": "string",
        },
        "page": {
            "type": "integer",
        },
    },
    'required': ['board_id', 'threads']
}

PostSchema = {
    "type": "object",
    "properties": {
        "content": {
            "type": "string"
        },
        "id": {
            "type": "string"
        },
        "author": {
            "type": "string"
        },
    },
    "additionalProperties": {
        "page": {
            "type": "integer"
        },
        "position": {
            "type": "integer"
        }
    },
    "required": ["content", "id", "author"]
}

PostListSchema = {
    "type": "object",
    "properties": {
        "posts": {
            "type": "object",
            "patternProperties": {
                "^[0-9]+$": PostSchema,
            },
        },
        "thread_id": {
            "type": "string",
        },
        "page": {
            "type": "integer",
        }
    },
    "required": ["board_id", "thread_id"],
}
