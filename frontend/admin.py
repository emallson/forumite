from django.contrib import admin
from frontend.models import Forum


class ForumAdmin(admin.ModelAdmin):
    prepopulated_fields = {'slug': ('name',), 'css': ('slug',)}

admin.site.register(Forum, ForumAdmin)
