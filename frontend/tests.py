from django.test import TestCase
from django.test.client import Client
from django.core.urlresolvers import reverse

from frontend.models import Forum


class ForumModel(TestCase):
    def setUp(self):
        self.name = "Forumite Test"
        self.root = "http://localhost/~emallson/forumite/"
        self.default_avatar = "http://localhost/forumite_media/default.jpg"

    def test_initialization(self):
        f = Forum(name=self.name, root=self.root,
                  default_avatar=self.default_avatar)
        self.assertEqual(f.name, self.name)
        self.assertEqual(f.root, self.root)
        self.assertEqual(f.default_avatar, self.default_avatar)


class ReaderView(TestCase):
    def setUp(self):
        self.name = "Forumite Test"
        self.root = "http://localhost/~emallson/forumite/"
        self.forum = Forum(name=self.name, root=self.root)
        self.forum.save()

    def test_reader(self):
        c = Client()
        response = c.get('/read/forumite-test/')
        self.assertEqual(response.status_code, 200)
        self.assertEqual(response.context['forum'].name, self.name)

        response = c.get('/read/terrible/')
        self.assertEqual(response.status_code, 404)

        response = c.get('/read/')
        self.assertEqual(response.status_code, 404)

    def test_url_loading(self):
        c = Client()
        response = c.get('/read/forumite-test/1/100/1/')
        self.assertEqual(response.status_code, 200)
        self.assertEqual(response.context['board_id'], '1')
        self.assertEqual(response.context['thread_id'], '100')
        self.assertEqual(response.context['page_num'], '1')

        response = c.get('/read/forumite-test/')
        self.assertEqual(response.status_code, 200)  # can't hurt to check
        self.assertEqual(response.context['board_id'], 'undefined')
        self.assertEqual(response.context['thread_id'], 'undefined')
        self.assertEqual(response.context['page_num'], 'undefined')


class IndexView(TestCase):

    def setUp(self):
        """ for this test case we want all three enabled """
        forums = Forum.objects.all()
        for forum in forums:
            forum.display_on_index = True
            forum.save()

    def test_loads(self):
        c = Client()
        response = c.get(reverse('frontend.views.index'))
        self.assertEqual(response.status_code, 200)
        self.assertEqual(len(response.context['forums']), 3)
