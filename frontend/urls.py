from django.conf.urls import patterns

urlpatterns = patterns(
    'frontend.views',
    (r'read/(?P<slug>[a-z0-9-]{1,30})/$', 'reader'),
    (r'read/(?P<slug>[a-z0-9-]{1,30})/(?P<board_id>[0-9]+)/(?P<thread_id>[0-9]+)/(?P<page_num>[0-9]+)/$', 'reader'),
    (r'^$', 'index'),
)
