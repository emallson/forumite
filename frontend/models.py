from django.db import models
from django.utils.text import slugify


class Forum(models.Model):
    name = models.CharField(max_length=30, unique=True)
    root = models.CharField(max_length=120)
    slug = models.SlugField(max_length=30, unique=True)
    css = models.CharField(max_length=30)
    default_avatar = models.CharField(max_length=120)
    logo = models.ImageField(upload_to='logos/', default='logos/default.jpg')
    display_on_index = models.BooleanField(default=False)
    provider_name = models.CharField(max_length=30)

    def __str__(self):
        return self.name

    def __repr__(self):
        return self.__str__()

    def save(self, *args, **kwargs):
        if self.slug is None or self.slug == '':
            self.slug = slugify(self.name)
        super(Forum, self).save(*args, **kwargs)
