from django.http import Http404
from django.shortcuts import render_to_response
from frontend.models import Forum
from user_profiles.utils import get_alias, get_global_profile
from django.conf import settings


def reader(request, slug=None,
           board_id='undefined', thread_id='undefined', page_num='undefined'):
    if slug is None:
        raise Http404

    try:
        forum = Forum.objects.get(slug=slug)

        if request.user.is_authenticated():
            alias = get_alias(request.user, forum)
            if alias is None:
                profile = get_global_profile(request.user)
            else:
                profile = alias.profile
        else:
            profile = None

        return render_to_response('frontend/reader.html', {
            'user': request.user,
            'profile': profile,
            'forum': forum,
            'board_id': board_id,
            'thread_id': thread_id,
            'page_num': page_num,
            'path': request.path,
            'forumite_root': settings.FORUMITE_ROOT
        })
    except Forum.DoesNotExist:
        raise Http404


def index(request):
    return render_to_response('frontend/index.html', {
        'forums': Forum.objects.filter(display_on_index=True),
    })
