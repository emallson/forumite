(function(Forumite, $, undefined) {

    /**
     * @name Board
     * @constructor
     * @classdesc Represents an individual Board (eg General Discussion) on a
     * forum.
     *
     * @param {Number} id
     * @param {String} name
     * @param {Array.<Thread>} threads
     */
    Forumite.Board = function(id, name, threads) {
        this.id = Number(id);
        this.name = String(name);
        this.threads = threads;
    };

    Forumite.Post = function(id, author, content) {
        this.id = id;
        this.content = content;
        this.author = author;
    };

    /**
     * @name User
     * @constructor
     * @classdesc Represents a forum user.
     *
     * @param {Number} id
     * @param {String} name
     * @param {String} email
     */
    Forumite.User = function(id, name, email) {
        this.id = Number(id);
        this.name = String(name);
        this.email = String(email);
    };

    /**
     * @name Thread
     * @constructor
     * @classdesc Represents an individual discussion thread.
     *
     * @param {Number} id
     * @param {String} name
     * @param {User} author
     * @param {Array.<Post>} posts
     */
    Forumite.Thread = function(id, name, author, posts) {

        this.id = Number(id);
        this.name = String(name);
        this.author = String(author);
        this.posts = posts;
    };

}(window.forumite = window.forumite || {}, jQuery));