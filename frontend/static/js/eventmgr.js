(function(Forumite, $, undefined) {

    /**
     * The EventMgr class is a very simple event management system based on
     * $.Callbacks(). It is effectively a mapping of string event names to
     * callback objects.
     *
     * @constructor
     */
    Forumite.EventMgr = function() {
        var events = {};

        function makeCallbacks() {
            return $.Callbacks("unique stopOnFalse");
        }
        /**
         * Registers an event callback.
         *
         * @param {String} event
         * @param {Function} callback
         */
        this.on = function(event, callback) {
            if(events.hasOwnProperty(event)) {
                events[event].add(callback);
            } else {
                events[event] = makeCallbacks();
                events[event].add(callback);
            }
        };

        var triggerOnce = function(t, event, callback) {
            var func = function(obj) {
                var f = obj.f;
                var args = Array.prototype.slice.call(arguments);
                // binding overrides application
                // slice is used to remove `f' from the list
                var rval = callback.apply(null, args.slice(1));
                t.off(event, f);
                return rval;
            };

            var obj = {};
            obj.f = func.bind(t, obj);
            return obj.f;
        };

        /**
         * Registers an event callback that will be run only once.
         *
         * @param {String} event
         * @param {Function} callback
         */
        this.once = function(event, callback) {
            if(events.hasOwnProperty(event)) {
                events[event].add(triggerOnce(this, event, callback));
            } else {
                events[event] = makeCallbacks();
                events[event].add(triggerOnce(this, event, callback));
            }
        };

        /**
         * Triggers an event.
         *
         * @param {String} event The event to fire.
         * @param {Anything} [data] The data to be passed to the callback.
         * @param {Boolean} [apply=false] Whether to use .apply(...) to pass the
         * data param.
         */
        this.trigger = function(event, data, apply) {
            if(apply === undefined) {
                apply = false; // not strictly necessary, but nice to clarify
            }

            if(event === undefined) {
                for(var e in events) {
                    if(apply) {
                        events[e].fire.apply(events[e], data);
                    } else {
                        events[e].fire(data);
                    }
                }
            } else if(events.hasOwnProperty(event)) {
                if(apply) {
                    events[event].fire.apply(events[event], data);
                } else {
                    events[event].fire(data);
                }
            }
        };

        /**
         * Removes an event callback.
         *
         * @param {String} event
         * @param {Function} callback
         */
        this.off = function(event, callback) {
            if(event === undefined) {
                for(var key in events) {
                    delete events[key];
                }
            }
            if(events.hasOwnProperty(event)) {
                if(callback === undefined) {
                    delete events[event];
                } else {
                    events[event].remove(callback);
                }
            }
        };
    };

}(window.forumite = window.forumite || {}, jQuery));
