/* exported: slugify */

// First, checks if it isn't implemented yet.
if (!String.prototype.format) {
    String.prototype.format = function() {
        var args = arguments;
        return this.replace(/{(\d+)}/g, function(match, number) {
                    return typeof args[number] !== "undefined" ?
                       args[number] : match;
                });
    };
}

function slugify(str) {
    return str.toLowerCase().replace(/ /g, '-')
              .replace(/[^\w-]+/g,'');
}

if(!Array.prototype.distinct) {
    Array.prototype.distinct = function(){
        var u = {}, a = [];
        for(var i = 0, l = this.length; i < l; ++i){
            if(u.hasOwnProperty(this[i])) {
                continue;
            }
            a.push(this[i]);
            u[this[i]] = 1;
        }
        return a;
    };
}

(function($) {
    $.fn.hasScrollBar = function() {
        return this.get(0).scrollHeight > this.height();
    };
})(jQuery);