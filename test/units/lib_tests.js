module("Lib");

test("Format", function() {
    equal("asdf{0}{1}".format(1, 2), "asdf12");
    equal("asdf{1}{0}".format(1, 2), "asdf21");
    equal("asdf{1}{0}".format(1, 2, 3), "asdf21");
    equal("asdf{1}{0}{2}".format(1, 2), "asdf21{2}");
});

test("Slugify", function() {
    equal(slugify("User#9001"), "user9001");
    equal(slugify("Some text"), "some-text");
});