/* global module */
/* global test */
/* global strictEqual */
/* global deepEqual */

(function(Forumite) {
    module("Data");

    test("Post", function() {
        var post = new Forumite.Post(1, "emallson", "You're terrible");
        strictEqual(post.id, 1, "ID matches");
        strictEqual(post.author, "emallson", "Name matches");
        strictEqual(post.content, "You're terrible", "Content matches");
    });

    test("Thread", function() {
        var posts =  [
            new Forumite.Post(1, "emallson", "You're terrible")
        ];

        var thread = new Forumite.Thread(1, "Terrible thread", "emallson", posts);

            strictEqual(thread.id, 1, "id matches");
        strictEqual(thread.name, "Terrible thread", "name matches");
        strictEqual(thread.author, "emallson", "author matches");
        deepEqual(thread.posts, posts, "posts match");
    });

    test("Board", function() {
        var posts =  [
            new Forumite.Post(1, "emallson", "You're terrible")
        ];
        var threads = new Forumite.Thread(1, "Terrible thread", "emallson", posts);
        var board = new Forumite.Board(1, "Empty Board", threads);

        strictEqual(board.id, 1, "id matches");
        strictEqual(board.name, "Empty Board", "name matches");
        deepEqual(board.threads, threads,"threads match");
    });

    test("User", function() {
        var user = new Forumite.User(1, "emallson", "emallson@archlinux.us");

        strictEqual(user.id, 1, "id matches");
        strictEqual(user.name, "emallson", "name matches");
        strictEqual(user.email, "emallson@archlinux.us", "email matches");
    });

}(window.forumite));