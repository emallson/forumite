/* global module */
/* global asyncTest */
/* global start */
/* global test */
/* global ok */
/* global deepEqual */
/* global forumite */


(function(Forumite) {
    var resolver;
    module("Resolver", {
        'setup': function() {
            resolver = new Forumite.Resolver(
                "http://localhost:8080/providers/lol_test/",
                "http://localhost:8080/",
                "lol"
            );
        }
    });

    asyncTest("Board Loader", function() {
        resolver.once("load-boards", function(data) {
            ok(data, "Boards loaded successfully");
            start();
        });
        resolver.fetchBoards();
    });

    asyncTest("Thread Loader", function() {
        resolver.once("load-threads", function(board_id, pageNum, data) {
            ok(data, "Threads loaded successfully");
            start();
        });
        resolver.fetchThreads(1,1); // args here don't actually matter
    });

    asyncTest("Post Loader", function() {
        resolver.once("load-posts", function(thread_id, pageNum, data) {
            ok(data, "Posts loaded successfully");
            start();
        });
        resolver.fetchPosts(1,1); // and again, these don't matter
    });

    asyncTest("Proper Thread 404 Handling", function() {
        resolver.once("load-threads", function(board_id, pageNum, data) {
            deepEqual(data, null, "Null Thread Dataset returned");
            start();
        });
        resolver.fetchThreads(1,100);
    });

    asyncTest("Proper Post 404 Handling", function() {
        resolver.once("load-posts", function(thread_id, pageNum, data) {
            deepEqual(data, null, "Null Post Dataset returned");
            start();
        });
        resolver.fetchPosts(1,100);
    });

    asyncTest("Profile Loader", function() {
        resolver.once("load-profiles", function(names, data) {
            deepEqual(data, expectedProfileResponse,
                      "Profile data matches expected");
            start();
        });
        resolver.fetchProfiles(profileRequest);
    });

    var profileRequest = [
        'Atlanis', 'User'
    ];
    var expectedProfileResponse = {
        "Atlanis": {
            "avatar": "http://localhost/~emallson/forumite_media/avatars/2013/10/08/buildings_have_eyes.png",
            "name": "Atlanis",
            "signature": "Trolololol"
        },
        "User": {
            "avatar": "http://localhost/~emallson/forumite_media/avatars/2013/10/08/buildings_have_eyes_1.png",
            "name": "User",
            "signature": "Trolololol"
        }
    };

}(window.forumite));