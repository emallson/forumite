/* global module */
/* global asyncTest */
/* global start */
/* global test */
/* global deepEqual */
/* global equal */
/* global ok */

(function($, Forumite) {
    var iface, pushState, replaceState;
    module("Interface", {
        'setup': function() {
            $("#qunit-fixture").append($("<select>", {'id': "boards"}));
            $("#qunit-fixture").append($("<div>", {'id': "threads"}));
            $("#qunit-fixture").append($("<div>", {'id': "posts"}));
            iface = new Forumite.Interface(
                $("#boards"), $("#threads"), $("#posts"),
                'http://media_server/default_avatar.png',
                'http://localhost:8080/', 'lol',
                'http://localhost:8080/providers/lol_test/'
            );
            pushState = window.history.pushState;
            replaceState = window.history.replaceState;
            window.history.pushState =
                window.history.replaceState = function(){};
        },
        'teardown': function() {
            window.history.pushState = pushState;
            window.history.replaceState = replaceState;
        }
    });

    asyncTest("Boards", 7, function() {
        deepEqual(iface.updateBoards(null), false, "Null data returns false");
        iface.updateBoards({
            '1': {
                'id': 1,
                'name': 'Test 1'
            },
            '2': {
                'id': 2,
                'name': 'Test 2'
            }
        });
        equal($("#boards option").length, 3,
              "Loading data creates proper number of rows");
        equal($("#boards option")[1].innerHTML, "Test 1", "Board 1 Content Good");
        equal($("#boards option")[2].innerHTML, "Test 2", "Board 2 Content Good");

        // tests the on-click handler
        iface.resolver.once("load-threads", function(bid, page, data) {
            equal(bid, 1, "Proper Board ID");
            equal(page, 1, "Proper Page Number");
            ok(data, "Got some data");
            start();
        });
        var event = $.Event("change");
        $('#boards').val(1);
        $('#boards').trigger(event);
    });

    test("updateThreads", function() {
        ok(!iface.updateThreads(1, 1, null), "Invalid threads results in false");
        ok(iface.updateThreads(1, 1, thread_data),
           "Valid threads results in true");

        ok($("#threads td.thread-title")[0].innerHTML === "Test 1" &&
           $("#threads td.thread-title")[1].innerHTML === "Test 2" &&
           $("#threads td.thread-title")[2].innerHTML === "Test 3",
           "Correct sorting of threads");
        ok(iface.updateThreads(1,3, thread_data) &&
           iface.updateThreads(1, 2, thread_data),
           "Adding subsequent pages works");
        ok($("#threads table").filter(":first").data("page") === 1 &&
           $("#threads table").filter(":nth-child(2)").data("page") === 2 &&
           $("#threads table").filter(":nth-child(3)").data("page") === 3,
           "Pages are correctly ordered");
    });

    asyncTest("Thread Click", function() {
        iface.updateThreads(1, 1, thread_data);
        // not testing scrolling yet
        iface.resolver.once("load-posts", function(tid, page, data) {
            equal(tid, 1002, "Correct thread id");
            equal(page, 1, "Correct page number");
            ok(data, "Data exists");
            start();
            return false;
        });
        var event = $.Event("click");
        $("#threads td").filter(":first").trigger(event);
    });

    asyncTest("Thread Scroll Down", function() {
        iface.updateThreads(1, 1, thread_data);
        iface.resolver.once("load-threads", function(bid, page, data) {
            equal(bid, 1, "Correct board id");
            equal(page, 2, "Correct page number");
            equal(data, null, "Appropriate lack of data");
            start();
        });
        $("#threads").scrollTop(100000);
        $("#threads").scroll();
    });

    test("updatePosts", function() {
        ok(!iface.updatePosts(100, 1, null),
           "Invalid posts results in false");
        ok(iface.updatePosts(100, 1, post_data),
           "Valid posts results in true");

        equal($("#posts .author strong")[0].innerHTML, "Test 1",
              "Correct sorting of posts - 1");
        equal($("#posts .author strong")[1].innerHTML, "Test 2",
           "Correct sorting of posts - 2");
        ok(iface.updatePosts(100, 2, post_data) &&
           iface.updatePosts(100, 3, post_data),
           "Adding subsequent pages works");
        equal($("#posts table").filter(":first").data("page"), 1,
              "Correct sorting of pages - 1");
        equal($("#posts table").filter(":nth-child(2)").data("page"), 2,
              "Correct sorting of pages - 2");
        equal($("#posts table").filter(":nth-child(3)").data("page"), 3,
              "Correct sorting of pages - 3");
    });

    test("Post URL Updating", function() {
        // we need to have a real function here but can't use pushState
        // this is to prevent creating a bunch of duplicate history entries
        var pushed = false;
        window.history.pushState = function() {
            pushed = true;
            replaceState.apply(window.history, arguments);
        };
        window.history.replaceState = replaceState;
        var url = window.location.href;

        iface.updateThreads(1, 1, thread_data);
        iface.updatePosts(100, 1, {'1': post_data['1']});
        ok(pushed, "pushState was used");
        equal(window.history.state.id, 100, "Page 1 State set properly - 1");
        equal(window.history.state.pageNum, 1, "Page 1 State set properly - 2");

        var hs = window.location.href.split('/');
        deepEqual(hs.splice(hs.length - 4, hs.length), ["1", "100", "1", ""],
                  "Page 1 URL set properly");

        pushed = false;

        iface.updatePosts(100, 2, post_data);
        ok(!pushed, "pushState was not used");

        equal(window.history.state.id, 100, "Page 2 State set properly - 1");
        equal(window.history.state.pageNum, 2, "Page 2 State set properly - 2");

        hs = window.location.href.split('/');
        deepEqual(hs.splice(hs.length - 4, hs.length), ["1", "100", "2", ""],
                  "Page 2 URL set properly");

        window.history.replaceState(null, document.title, url);
    });

    asyncTest("Post Scroll Down", function() {
        iface.updatePosts(100, 1, post_data);
        iface.resolver.once("load-posts", function(tid, page, data) {
            equal(tid, 100, "Correct thread id");
            equal(page, 2, "Correct page number");
            equal(data, null, "Appropriate lack of data");
            start();
        });
        $("#posts").scrollTop(100000);
        $("#posts").scroll();
    });

    asyncTest("Post Scroll Up", function() {
        iface.updatePosts(100, 2, post_data);
        iface.resolver.once("load-posts", function(tid, page, data) {
            equal(tid, 100, "Correct thread id");
            equal(page, 1, "Correct page number");
            equal(data, null, "Appropriate lack of data");
            start();
        });
        $("#posts").scrollTop(100);
        $("#posts").scroll();
        $("#posts").scrollTop(0);
        $("#posts").scroll();
    });

    asyncTest("Loading from URL", 3, function() {
        var loading_iface = new Forumite.Interface($("#boards"), $("#threads"),
                                                   $("#posts"), '', 'http://localhost:8080/', 'lol',
                                                   'http://localhost:8080/providers/lol_test/',
                                                   1,1,1);
        loading_iface.once("load-posts-finished", function() {
            equal($("#threads").data('id'), 1, "Correct board loaded");
            equal($("#posts").data('id'), 1, "Correct thread loaded");
            equal($("#posts").data('page'), 1, "Correct page loaded");
            start();
        });
        loading_iface.refresh('board');
    });

    test("updateProfiles", function() {
        iface.updatePosts(100, 1, post_data);
        iface.updateProfiles(profile_names, profile_data);

        equal($("#posts .avatar")[0].src, "http://media_server/a_real_avatar.png", "Correct avatar src");
        equal($("#posts .avatar")[1].src, "http://media_server/default_avatar.png",
             "Correct default src");

        equal($("#posts .signature:first").text(), "A signature", "Sig is set if exists");
        equal($("#posts .signature:nth(2)").text(), "", "Sig is not set if non-existant");
    });

    var thread_data = {
        "1": {
            "threads": {
                "1001": {
                    "id": 1001,
                    "position": 3,
                    "name": "Test 3",
                    "author": "Test"
                },
                "100": {
                    "id": 100,
                    "position": 2,
                    "name": "Test 2",
                    "author": "Test"
                },
                "1002": {
                    "id": 1002,
                    "position": 1,
                    "name": "Test 1",
                    "author": "Test"
                }
            },
            "page": 1,
            "board_id": 1
        },
        "2": {
            "threads": {},
            "page": 2,
            "board_id": 1
        },
        "3": {
            "threads": {},
            "page": 3,
            "board_id": 1
        }
    };

    var post_data = {
        "1": {
            "posts": {
                "100": {
                    "id": 100,
                    "author": "Test 2",
                    "content": "Bar",
                    "position": 2
                },
                "101": {
                    "id": 101,
                    "author": "Test 1",
                    "content": "Foo",
                    "position": 1
                }
            },
            "page": 1,
            "thread_id": 100
        },
        "2": {
            "posts": {
                "102": {
                    "id": 102,
                    "author": "Test",
                    "content": "Foobar",
                    "position": 1
                }
            },
            "page": 2,
            "thread_id": 100
        },
        "3": {
            "posts": {},
            "page": 3,
            "thread_id": 100
        }
    };


    var profile_names = ["Test 2", "Test 1"];
    var profile_data = {
        "Test 1": {
            "name": "Test 1",
            "avatar": "http://media_server/a_real_avatar.png",
            "signature": "A signature"
        }
    };
}(jQuery, window.forumite));
