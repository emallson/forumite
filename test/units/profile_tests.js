/* global module */
/* global asyncTest */
/* global test */
/* global ok */
/* global equal */
/* global notEqual */

(function($, Forumite) {

    module("Profile", {
        'setup': function() {
            $("#qunit-fixture").html(
                '<li class="row alias-row" id="alias-3"><form method="POST" action="../alias/put/" class="alias-form"><input type="hidden" name="csrfmiddlewaretoken" value="iv752GpLuKaSDwynJp9UOklYm82UHsN3" /><span class="col-2 forum-display"><strong>League of Legends</strong></span><span class="col-2 alias-display avatar-display"><img class="avatar" src="http://localhost/~emallson/forumite_media/avatars/2013/10/08/buildings_have_eyes_1.png"/></span><span class="col-2 alias-edit avatar-edit">Currently: <a href="http://localhost/~emallson/forumite_media/avatars/2013/10/08/buildings_have_eyes_1.png">avatars/2013/10/08/buildings_have_eyes_1.png</a><input id="avatar-clear_id" name="avatar-clear" type="checkbox" /><label for="avatar-clear_id">Clear</label><br />Change: <input id="id_avatar" name="avatar" type="file" /></span><span class="col-3 name-display">User 42 (user-42)</span><span class="col-4 alias-display signature-display">qwertyasdf</span><span class="col-4 alias-edit signature-edit"><textarea cols="40" id="id_signature" name="signature" rows="10">qwertyasdf</textarea></span><span class="col-1 alias-display"><button type="button" class="btn btn-default edit">Edit</button><button type="button" class="btn btn-default authenticate">Authenticate</button></span><span class="col-1 alias-edit"><button type="button" class="btn btn-cancel cancel">Cancel</button><button type="submit" class="btn btn-primary save">Save</button></span></form></li><li class="row alias-row" id="alias-4"><form method="POST" action="../alias/put/" class="alias-form"><input type="hidden" name="csrfmiddlewaretoken" value="iv752GpLuKaSDwynJp9UOklYm82UHsN3" /><span class="col-2 forum-display"><strong>League of Legends</strong></span><span class="col-2 alias-display avatar-display"><img class="avatar" src="http://localhost/~emallson/forumite_media/avatars/2013/10/08/buildings_have_eyes_1.png"/></span><span class="col-2 alias-edit avatar-edit">Currently: <a href="http://localhost/~emallson/forumite_media/avatars/2013/10/08/buildings_have_eyes_1.png">avatars/2013/10/08/buildings_have_eyes_1.png</a><input id="avatar-clear_id" name="avatar-clear" type="checkbox" /><label for="avatar-clear_id">Clear</label><br />Change: <input id="id_avatar" name="avatar" type="file" /></span><span class="col-3 name-display">User 42 (user-42)</span><span class="col-4 alias-display signature-display">qwertyasdf</span><span class="col-4 alias-edit signature-edit"><textarea cols="40" id="id_signature" name="signature" rows="10">qwertyasdf</textarea></span><span class="col-1 alias-display"><button type="button" class="btn btn-default edit">Edit</button><button type="button" class="btn btn-default authenticate">Authenticate</button></span><span class="col-1 alias-edit"><button type="button" class="btn btn-cancel cancel">Cancel</button><button type="submit" class="btn btn-primary save">Save</button></span></form></li><div id="authModal"><input id="authHash"><input id="aliasID"><input type="text" id="authPostId"/><button type="button" id="checkAuth"/></div><div id="deleteModal"><form action="" id="delete-form"></form><p id="forum-name"></p></div>'
            );
        },
        'teardown': function() {
        }
    });

    test("Display State Changes", function() {
        var visible = true;
        $('.alias-display').each(function() {
            if(!$(this).is(":visible")) {
                visible = false;
            }
        });
        ok(visible, "alias-display starts visible");

        visible = true;
        $('.alias-edit').each(function() {
            if($(this).is(":visible")) {
                visible = false;
            }
        });
        ok(visible, "alias-edit starts hidden");

        Forumite.Profile.displayEdit($('.alias-row'));

        visible = true;
        $('.alias-display').each(function() {
            if($(this).is(":visible")) {
                visible = false;
            }
        });
        ok(visible, "alias-display hidden after displayEdit");

        visible = true;
        $('.alias-edit').each(function() {
            if(!$(this).is(":visible")) {
                visible = false;
            }
        });
        ok(visible, "alias-edit visible after displayEdit");

        Forumite.Profile.displayDisplay($('.alias-row'));

        visible = true;
        $('.alias-display').each(function() {
            if(!$(this).is(":visible")) {
                visible = false;
            }
        });
        ok(visible, "alias-display visible after displayDisplay");

        visible = true;
        $('.alias-edit').each(function() {
            if($(this).is(":visible")) {
                visible = false;
            }
        });
        ok(visible, "alias-edit hidden after displayDisplay");
    });

    test("Update Display", function() {
        Forumite.Profile.updateDisplay($(".alias-row:first"), test_data);

        equal($(".alias-row:first .forum-display strong").text(),
              "League of Legends", "Forum unchanged");
        equal($(".alias-row:first .name-display").text(),
              "User 42 (user-42)", "Username unchanged");
        equal($(".alias-row:first .signature-display").text(), "Test Signature");
        equal($(".alias-row:first .avatar-display img").prop('src'),
              "http://localhost/~emallson/forumite_media/avatars/2013/10/08/Avatar_of_Grenth_concept_art.jpg");

        var test_data_2 = $.extend(true, {}, test_data);
        delete test_data_2.avatar;
        Forumite.Profile.updateDisplay($(".alias-row:first"), test_data_2);
        equal($(".alias-row:first .avatar-display img").prop('src'), "");
    });

    test("Update Edit", function() {
        $(".alias-row:first .avatar-edit input[type='checkbox']").prop('checked',
                                                                       true);
        Forumite.Profile.updateEdit($(".alias-row:first"), test_data);

        equal($(".alias-row:first .signature-edit textarea").val(), test_data.signature,
              "Correct signature");
        equal($(".alias-row:first .avatar-edit a").prop('href'), test_data.avatar,
              "Correct avatar link");
        equal($(".alias-row:first .avatar-edit input[type='file']").val(), '',
              "File Upload input still exists");
        equal($(".alias-row:first .avatar-edit input[type='checkbox']").is(":checked"), false,
              "Avatar clear box unchecked");

        var test_data_2 = $.extend(true, {}, test_data);
        delete test_data_2.avatar;
        Forumite.Profile.updateEdit($(".alias-row:first"), test_data_2);
        equal($(".alias-row:first .avatar-edit a").length, 0, "Avatar link removed");
        equal($(".alias-row:first .avatar-edit input[type='checkbox']").length, 0,
              "Clear box removed");

        Forumite.Profile.updateEdit($(".alias-row:first"), test_data);
        equal($(".alias-row:first .avatar-edit a").length, 1, "Avatar link added");
        equal($(".alias-row:first .avatar-edit input[type='checkbox']").length, 1,
              "Clear box added");
    });

    test("Setup Authentication Modal", function() {
        Forumite.Profile.setupModal($("#authModal"), 1, "a hash");
        equal($("#authHash").val(), "[auth]a hash[/auth]", "Correct Hash");
        equal($("#aliasID").val(), 1, "Correct Alias ID");
        equal($("#authPostId").val(), "", "Empty Post ID");

        $("#authPostId").val("blah");
        Forumite.Profile.setupModal($("#authModal"), 2, "another hash");
        equal($("#authHash").val(), "[auth]another hash[/auth]", "Hash changed");
        equal($("#aliasID").val(), 2, "Alias ID changed");
        equal($("#authPostId").val(), "", "Empty Post Id");
    });

    test("Setup Delete Modal", function() {
        Forumite.Profile.setupDeleteModal($("#deleteModal"), "alias-1",
                                          "/user/alias/delete/1/", "Some forum");
        equal($("#deleteModal").data('alias-row-id'), "alias-1", "Correct row ID");
        equal(
            RegExp('^(([^:/?#]+):)?(//([^/?#]*))?([^?#]*)(\\?([^#]*))?(#(.*))?').
                exec($("#delete-form").prop('action'))[5],
            "/user/alias/delete/1/", "Correct form action");
        equal($("#deleteModal #forum-name").text(), "Some forum",
              "Forum name matches");
    });

    test("Remove Alias", function() {
        Forumite.Profile.removeAliasRow($("#alias-3"));
        equal($("#alias-3").length, 0, "Row removed");
        equal($("#alias-4").length, 1, "Other row remains");
    });

    var test_data = {
        'avatar': "http://localhost/~emallson/forumite_media/avatars/2013/10/08/Avatar_of_Grenth_concept_art.jpg",
        'signature': "Test Signature"
    };

}(jQuery, window.forumite));