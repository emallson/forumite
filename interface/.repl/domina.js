// Compiled by ClojureScript 0.0-2030
goog.provide('domina');
goog.require('cljs.core');
goog.require('goog.dom.classes');
goog.require('domina.support');
goog.require('goog.dom.classes');
goog.require('cljs.core');
goog.require('goog.events');
goog.require('goog.string');
goog.require('goog.dom.xml');
goog.require('goog.dom');
goog.require('goog.dom.forms');
goog.require('goog.dom');
goog.require('goog.string');
goog.require('clojure.string');
goog.require('goog.style');
goog.require('clojure.string');
goog.require('goog.dom.xml');
goog.require('goog.style');
goog.require('goog.dom.forms');
goog.require('domina.support');
goog.require('goog.events');
goog.require('cljs.core');
domina.re_html = /<|&#?\w+;/;
domina.re_leading_whitespace = /^\s+/;
domina.re_xhtml_tag = /<(?!area|br|col|embed|hr|img|input|link|meta|param)(([\w:]+)[^>]*)\/>/i;
domina.re_tag_name = /<([\w:]+)/;
domina.re_no_inner_html = /<(?:script|style)/i;
domina.re_tbody = /<tbody/i;
var opt_wrapper_6467 = cljs.core.PersistentVector.fromArray([1,"<select multiple='multiple'>","</select>"], true);var table_section_wrapper_6468 = cljs.core.PersistentVector.fromArray([1,"<table>","</table>"], true);var cell_wrapper_6469 = cljs.core.PersistentVector.fromArray([3,"<table><tbody><tr>","</tr></tbody></table>"], true);domina.wrap_map = cljs.core.PersistentHashMap.fromArrays(["col",new cljs.core.Keyword(null,"default","default",2558708147),"tfoot","caption","optgroup","legend","area","td","thead","th","option","tbody","tr","colgroup"],[cljs.core.PersistentVector.fromArray([2,"<table><tbody></tbody><colgroup>","</colgroup></table>"], true),cljs.core.PersistentVector.fromArray([0,"",""], true),table_section_wrapper_6468,table_section_wrapper_6468,opt_wrapper_6467,cljs.core.PersistentVector.fromArray([1,"<fieldset>","</fieldset>"], true),cljs.core.PersistentVector.fromArray([1,"<map>","</map>"], true),cell_wrapper_6469,table_section_wrapper_6468,cell_wrapper_6469,opt_wrapper_6467,table_section_wrapper_6468,cljs.core.PersistentVector.fromArray([2,"<table><tbody>","</tbody></table>"], true),table_section_wrapper_6468]);
domina.remove_extraneous_tbody_BANG_ = (function remove_extraneous_tbody_BANG_(div,html,tag_name,start_wrap){var no_tbody_QMARK_ = cljs.core.not.call(null,cljs.core.re_find.call(null,domina.re_tbody,html));var tbody = (((cljs.core._EQ_.call(null,tag_name,"table")) && (no_tbody_QMARK_))?(function (){var and__4667__auto__ = div.firstChild;if(cljs.core.truth_(and__4667__auto__))
{return div.firstChild.childNodes;
} else
{return and__4667__auto__;
}
})():(((cljs.core._EQ_.call(null,start_wrap,"<table>")) && (no_tbody_QMARK_))?divchildNodes:cljs.core.PersistentVector.EMPTY));var seq__6474 = cljs.core.seq.call(null,tbody);var chunk__6475 = null;var count__6476 = 0;var i__6477 = 0;while(true){
if((i__6477 < count__6476))
{var child = cljs.core._nth.call(null,chunk__6475,i__6477);if((cljs.core._EQ_.call(null,child.nodeName,"tbody")) && (cljs.core._EQ_.call(null,child.childNodes.length,0)))
{child.parentNode.removeChild(child);
} else
{}
{
var G__6478 = seq__6474;
var G__6479 = chunk__6475;
var G__6480 = count__6476;
var G__6481 = (i__6477 + 1);
seq__6474 = G__6478;
chunk__6475 = G__6479;
count__6476 = G__6480;
i__6477 = G__6481;
continue;
}
} else
{var temp__4092__auto__ = cljs.core.seq.call(null,seq__6474);if(temp__4092__auto__)
{var seq__6474__$1 = temp__4092__auto__;if(cljs.core.chunked_seq_QMARK_.call(null,seq__6474__$1))
{var c__5393__auto__ = cljs.core.chunk_first.call(null,seq__6474__$1);{
var G__6482 = cljs.core.chunk_rest.call(null,seq__6474__$1);
var G__6483 = c__5393__auto__;
var G__6484 = cljs.core.count.call(null,c__5393__auto__);
var G__6485 = 0;
seq__6474 = G__6482;
chunk__6475 = G__6483;
count__6476 = G__6484;
i__6477 = G__6485;
continue;
}
} else
{var child = cljs.core.first.call(null,seq__6474__$1);if((cljs.core._EQ_.call(null,child.nodeName,"tbody")) && (cljs.core._EQ_.call(null,child.childNodes.length,0)))
{child.parentNode.removeChild(child);
} else
{}
{
var G__6486 = cljs.core.next.call(null,seq__6474__$1);
var G__6487 = null;
var G__6488 = 0;
var G__6489 = 0;
seq__6474 = G__6486;
chunk__6475 = G__6487;
count__6476 = G__6488;
i__6477 = G__6489;
continue;
}
}
} else
{return null;
}
}
break;
}
});
domina.restore_leading_whitespace_BANG_ = (function restore_leading_whitespace_BANG_(div,html){return div.insertBefore(document.createTextNode(cljs.core.first.call(null,cljs.core.re_find.call(null,domina.re_leading_whitespace,html))),div.firstChild);
});
/**
* takes an string of html and returns a NodeList of dom fragments
*/
domina.html_to_dom = (function html_to_dom(html){var html__$1 = clojure.string.replace.call(null,html,domina.re_xhtml_tag,"<$1></$2>");var tag_name = [cljs.core.str(cljs.core.second.call(null,cljs.core.re_find.call(null,domina.re_tag_name,html__$1)))].join('').toLowerCase();var vec__6491 = cljs.core.get.call(null,domina.wrap_map,tag_name,new cljs.core.Keyword(null,"default","default",2558708147).cljs$core$IFn$_invoke$arity$1(domina.wrap_map));var depth = cljs.core.nth.call(null,vec__6491,0,null);var start_wrap = cljs.core.nth.call(null,vec__6491,1,null);var end_wrap = cljs.core.nth.call(null,vec__6491,2,null);var div = (function (){var wrapper = (function (){var div = document.createElement("div");div.innerHTML = [cljs.core.str(start_wrap),cljs.core.str(html__$1),cljs.core.str(end_wrap)].join('');
return div;
})();var level = depth;while(true){
if((level > 0))
{{
var G__6492 = wrapper.lastChild;
var G__6493 = (level - 1);
wrapper = G__6492;
level = G__6493;
continue;
}
} else
{return wrapper;
}
break;
}
})();if(cljs.core.truth_(domina.support.extraneous_tbody_QMARK_))
{domina.remove_extraneous_tbody_BANG_.call(null,div,html__$1,tag_name,start_wrap);
} else
{}
if(cljs.core.truth_((function (){var and__4667__auto__ = cljs.core.not.call(null,domina.support.leading_whitespace_QMARK_);if(and__4667__auto__)
{return cljs.core.re_find.call(null,domina.re_leading_whitespace,html__$1);
} else
{return and__4667__auto__;
}
})()))
{domina.restore_leading_whitespace_BANG_.call(null,div,html__$1);
} else
{}
return div.childNodes;
});
domina.string_to_dom = (function string_to_dom(s){if(cljs.core.truth_(cljs.core.re_find.call(null,domina.re_html,s)))
{return domina.html_to_dom.call(null,s);
} else
{return document.createTextNode(s);
}
});
domina.DomContent = {};
domina.nodes = (function nodes(content){if((function (){var and__4667__auto__ = content;if(and__4667__auto__)
{return content.domina$DomContent$nodes$arity$1;
} else
{return and__4667__auto__;
}
})())
{return content.domina$DomContent$nodes$arity$1(content);
} else
{var x__5272__auto__ = (((content == null))?null:content);return (function (){var or__4676__auto__ = (domina.nodes[goog.typeOf(x__5272__auto__)]);if(or__4676__auto__)
{return or__4676__auto__;
} else
{var or__4676__auto____$1 = (domina.nodes["_"]);if(or__4676__auto____$1)
{return or__4676__auto____$1;
} else
{throw cljs.core.missing_protocol.call(null,"DomContent.nodes",content);
}
}
})().call(null,content);
}
});
domina.single_node = (function single_node(nodeseq){if((function (){var and__4667__auto__ = nodeseq;if(and__4667__auto__)
{return nodeseq.domina$DomContent$single_node$arity$1;
} else
{return and__4667__auto__;
}
})())
{return nodeseq.domina$DomContent$single_node$arity$1(nodeseq);
} else
{var x__5272__auto__ = (((nodeseq == null))?null:nodeseq);return (function (){var or__4676__auto__ = (domina.single_node[goog.typeOf(x__5272__auto__)]);if(or__4676__auto__)
{return or__4676__auto__;
} else
{var or__4676__auto____$1 = (domina.single_node["_"]);if(or__4676__auto____$1)
{return or__4676__auto____$1;
} else
{throw cljs.core.missing_protocol.call(null,"DomContent.single-node",nodeseq);
}
}
})().call(null,nodeseq);
}
});
domina._STAR_debug_STAR_ = true;
/**
* @param {...*} var_args
*/
domina.log_debug = (function() { 
var log_debug__delegate = function (mesg){if(cljs.core.truth_((function (){var and__4667__auto__ = domina._STAR_debug_STAR_;if(cljs.core.truth_(and__4667__auto__))
{return !(cljs.core._EQ_.call(null,window.console,undefined));
} else
{return and__4667__auto__;
}
})()))
{return console.log(cljs.core.apply.call(null,cljs.core.str,mesg));
} else
{return null;
}
};
var log_debug = function (var_args){
var mesg = null;if (arguments.length > 0) {
  mesg = cljs.core.array_seq(Array.prototype.slice.call(arguments, 0),0);} 
return log_debug__delegate.call(this,mesg);};
log_debug.cljs$lang$maxFixedArity = 0;
log_debug.cljs$lang$applyTo = (function (arglist__6494){
var mesg = cljs.core.seq(arglist__6494);
return log_debug__delegate(mesg);
});
log_debug.cljs$core$IFn$_invoke$arity$variadic = log_debug__delegate;
return log_debug;
})()
;
/**
* @param {...*} var_args
*/
domina.log = (function() { 
var log__delegate = function (mesg){if(cljs.core.truth_(window.console))
{return console.log(cljs.core.apply.call(null,cljs.core.str,mesg));
} else
{return null;
}
};
var log = function (var_args){
var mesg = null;if (arguments.length > 0) {
  mesg = cljs.core.array_seq(Array.prototype.slice.call(arguments, 0),0);} 
return log__delegate.call(this,mesg);};
log.cljs$lang$maxFixedArity = 0;
log.cljs$lang$applyTo = (function (arglist__6495){
var mesg = cljs.core.seq(arglist__6495);
return log__delegate(mesg);
});
log.cljs$core$IFn$_invoke$arity$variadic = log__delegate;
return log;
})()
;
/**
* Returns content containing a single node by looking up the given ID
*/
domina.by_id = (function by_id(id){return goog.dom.getElement(cljs.core.name.call(null,id));
});
/**
* Returns content containing nodes which have the specified CSS class.
*/
domina.by_class = (function by_class(class_name){return domina.normalize_seq.call(null,goog.dom.getElementsByClass(cljs.core.name.call(null,class_name)));
});
/**
* Gets all the child nodes of the elements in a content. Same as (xpath content '*') but more efficient.
*/
domina.children = (function children(content){return cljs.core.doall.call(null,cljs.core.mapcat.call(null,goog.dom.getChildren,domina.nodes.call(null,content)));
});
/**
* Returns the deepest common ancestor of the argument contents (which are presumed to be single nodes), or nil if they are from different documents.
* @param {...*} var_args
*/
domina.common_ancestor = (function() { 
var common_ancestor__delegate = function (contents){return cljs.core.apply.call(null,goog.dom.findCommonAncestor,cljs.core.map.call(null,domina.single_node,contents));
};
var common_ancestor = function (var_args){
var contents = null;if (arguments.length > 0) {
  contents = cljs.core.array_seq(Array.prototype.slice.call(arguments, 0),0);} 
return common_ancestor__delegate.call(this,contents);};
common_ancestor.cljs$lang$maxFixedArity = 0;
common_ancestor.cljs$lang$applyTo = (function (arglist__6496){
var contents = cljs.core.seq(arglist__6496);
return common_ancestor__delegate(contents);
});
common_ancestor.cljs$core$IFn$_invoke$arity$variadic = common_ancestor__delegate;
return common_ancestor;
})()
;
/**
* Returns true if the first argument is an ancestor of the second argument. Presumes both arguments are single-node contents.
*/
domina.ancestor_QMARK_ = (function ancestor_QMARK_(ancestor_content,descendant_content){return cljs.core._EQ_.call(null,domina.common_ancestor.call(null,ancestor_content,descendant_content),domina.single_node.call(null,ancestor_content));
});
/**
* Returns a deep clone of content.
*/
domina.clone = (function clone(content){return cljs.core.map.call(null,(function (p1__6497_SHARP_){return p1__6497_SHARP_.cloneNode(true);
}),domina.nodes.call(null,content));
});
/**
* Given a parent and child contents, appends each of the children to all of the parents. If there is more than one node in the parent content, clones the children for the additional parents. Returns the parent content.
*/
domina.append_BANG_ = (function append_BANG_(parent_content,child_content){domina.apply_with_cloning.call(null,goog.dom.appendChild,parent_content,child_content);
return parent_content;
});
/**
* Given a parent and child contents, appends each of the children to all of the parents at the specified index. If there is more than one node in the parent content, clones the children for the additional parents. Returns the parent content.
*/
domina.insert_BANG_ = (function insert_BANG_(parent_content,child_content,idx){domina.apply_with_cloning.call(null,(function (p1__6498_SHARP_,p2__6499_SHARP_){return goog.dom.insertChildAt(p1__6498_SHARP_,p2__6499_SHARP_,idx);
}),parent_content,child_content);
return parent_content;
});
/**
* Given a parent and child contents, prepends each of the children to all of the parents. If there is more than one node in the parent content, clones the children for the additional parents. Returns the parent content.
*/
domina.prepend_BANG_ = (function prepend_BANG_(parent_content,child_content){domina.insert_BANG_.call(null,parent_content,child_content,0);
return parent_content;
});
/**
* Given a content and some new content, inserts the new content immediately before the reference content. If there is more than one node in the reference content, clones the new content for each one.
*/
domina.insert_before_BANG_ = (function insert_before_BANG_(content,new_content){domina.apply_with_cloning.call(null,(function (p1__6501_SHARP_,p2__6500_SHARP_){return goog.dom.insertSiblingBefore(p2__6500_SHARP_,p1__6501_SHARP_);
}),content,new_content);
return content;
});
/**
* Given a content and some new content, inserts the new content immediately after the reference content. If there is more than one node in the reference content, clones the new content for each one.
*/
domina.insert_after_BANG_ = (function insert_after_BANG_(content,new_content){domina.apply_with_cloning.call(null,(function (p1__6503_SHARP_,p2__6502_SHARP_){return goog.dom.insertSiblingAfter(p2__6502_SHARP_,p1__6503_SHARP_);
}),content,new_content);
return content;
});
/**
* Given some old content and some new content, replaces the old content with new content. If there are multiple nodes in the old content, replaces each of them and clones the new content as necessary.
*/
domina.swap_content_BANG_ = (function swap_content_BANG_(old_content,new_content){domina.apply_with_cloning.call(null,(function (p1__6505_SHARP_,p2__6504_SHARP_){return goog.dom.replaceNode(p2__6504_SHARP_,p1__6505_SHARP_);
}),old_content,new_content);
return old_content;
});
/**
* Removes all the nodes in a content from the DOM and returns them.
*/
domina.detach_BANG_ = (function detach_BANG_(content){return cljs.core.doall.call(null,cljs.core.map.call(null,goog.dom.removeNode,domina.nodes.call(null,content)));
});
/**
* Removes all the nodes in a content from the DOM. Returns nil.
*/
domina.destroy_BANG_ = (function destroy_BANG_(content){return cljs.core.dorun.call(null,cljs.core.map.call(null,goog.dom.removeNode,domina.nodes.call(null,content)));
});
/**
* Removes all the child nodes in a content from the DOM. Returns the original content.
*/
domina.destroy_children_BANG_ = (function destroy_children_BANG_(content){cljs.core.dorun.call(null,cljs.core.map.call(null,goog.dom.removeChildren,domina.nodes.call(null,content)));
return content;
});
/**
* Gets the value of a CSS property. Assumes content will be a single node. Name may be a string or keyword. Returns nil if there is no value set for the style.
*/
domina.style = (function style(content,name){var s = goog.style.getStyle(domina.single_node.call(null,content),cljs.core.name.call(null,name));if(cljs.core.truth_(clojure.string.blank_QMARK_.call(null,s)))
{return null;
} else
{return s;
}
});
/**
* Gets the value of an HTML attribute. Assumes content will be a single node. Name may be a stirng or keyword. Returns nil if there is no value set for the style.
*/
domina.attr = (function attr(content,name){return domina.single_node.call(null,content).getAttribute(cljs.core.name.call(null,name));
});
/**
* Sets the value of a CSS property for each node in the content. Name may be a string or keyword. Value will be cast to a string, multiple values wil be concatenated.
* @param {...*} var_args
*/
domina.set_style_BANG_ = (function() { 
var set_style_BANG___delegate = function (content,name,value){var seq__6510_6514 = cljs.core.seq.call(null,domina.nodes.call(null,content));var chunk__6511_6515 = null;var count__6512_6516 = 0;var i__6513_6517 = 0;while(true){
if((i__6513_6517 < count__6512_6516))
{var n_6518 = cljs.core._nth.call(null,chunk__6511_6515,i__6513_6517);goog.style.setStyle(n_6518,cljs.core.name.call(null,name),cljs.core.apply.call(null,cljs.core.str,value));
{
var G__6519 = seq__6510_6514;
var G__6520 = chunk__6511_6515;
var G__6521 = count__6512_6516;
var G__6522 = (i__6513_6517 + 1);
seq__6510_6514 = G__6519;
chunk__6511_6515 = G__6520;
count__6512_6516 = G__6521;
i__6513_6517 = G__6522;
continue;
}
} else
{var temp__4092__auto___6523 = cljs.core.seq.call(null,seq__6510_6514);if(temp__4092__auto___6523)
{var seq__6510_6524__$1 = temp__4092__auto___6523;if(cljs.core.chunked_seq_QMARK_.call(null,seq__6510_6524__$1))
{var c__5393__auto___6525 = cljs.core.chunk_first.call(null,seq__6510_6524__$1);{
var G__6526 = cljs.core.chunk_rest.call(null,seq__6510_6524__$1);
var G__6527 = c__5393__auto___6525;
var G__6528 = cljs.core.count.call(null,c__5393__auto___6525);
var G__6529 = 0;
seq__6510_6514 = G__6526;
chunk__6511_6515 = G__6527;
count__6512_6516 = G__6528;
i__6513_6517 = G__6529;
continue;
}
} else
{var n_6530 = cljs.core.first.call(null,seq__6510_6524__$1);goog.style.setStyle(n_6530,cljs.core.name.call(null,name),cljs.core.apply.call(null,cljs.core.str,value));
{
var G__6531 = cljs.core.next.call(null,seq__6510_6524__$1);
var G__6532 = null;
var G__6533 = 0;
var G__6534 = 0;
seq__6510_6514 = G__6531;
chunk__6511_6515 = G__6532;
count__6512_6516 = G__6533;
i__6513_6517 = G__6534;
continue;
}
}
} else
{}
}
break;
}
return content;
};
var set_style_BANG_ = function (content,name,var_args){
var value = null;if (arguments.length > 2) {
  value = cljs.core.array_seq(Array.prototype.slice.call(arguments, 2),0);} 
return set_style_BANG___delegate.call(this,content,name,value);};
set_style_BANG_.cljs$lang$maxFixedArity = 2;
set_style_BANG_.cljs$lang$applyTo = (function (arglist__6535){
var content = cljs.core.first(arglist__6535);
arglist__6535 = cljs.core.next(arglist__6535);
var name = cljs.core.first(arglist__6535);
var value = cljs.core.rest(arglist__6535);
return set_style_BANG___delegate(content,name,value);
});
set_style_BANG_.cljs$core$IFn$_invoke$arity$variadic = set_style_BANG___delegate;
return set_style_BANG_;
})()
;
/**
* Sets the value of an HTML property for each node in the content. Name may be a string or keyword. Value will be cast to a string, multiple values wil be concatenated.
* @param {...*} var_args
*/
domina.set_attr_BANG_ = (function() { 
var set_attr_BANG___delegate = function (content,name,value){var seq__6540_6544 = cljs.core.seq.call(null,domina.nodes.call(null,content));var chunk__6541_6545 = null;var count__6542_6546 = 0;var i__6543_6547 = 0;while(true){
if((i__6543_6547 < count__6542_6546))
{var n_6548 = cljs.core._nth.call(null,chunk__6541_6545,i__6543_6547);n_6548.setAttribute(cljs.core.name.call(null,name),cljs.core.apply.call(null,cljs.core.str,value));
{
var G__6549 = seq__6540_6544;
var G__6550 = chunk__6541_6545;
var G__6551 = count__6542_6546;
var G__6552 = (i__6543_6547 + 1);
seq__6540_6544 = G__6549;
chunk__6541_6545 = G__6550;
count__6542_6546 = G__6551;
i__6543_6547 = G__6552;
continue;
}
} else
{var temp__4092__auto___6553 = cljs.core.seq.call(null,seq__6540_6544);if(temp__4092__auto___6553)
{var seq__6540_6554__$1 = temp__4092__auto___6553;if(cljs.core.chunked_seq_QMARK_.call(null,seq__6540_6554__$1))
{var c__5393__auto___6555 = cljs.core.chunk_first.call(null,seq__6540_6554__$1);{
var G__6556 = cljs.core.chunk_rest.call(null,seq__6540_6554__$1);
var G__6557 = c__5393__auto___6555;
var G__6558 = cljs.core.count.call(null,c__5393__auto___6555);
var G__6559 = 0;
seq__6540_6544 = G__6556;
chunk__6541_6545 = G__6557;
count__6542_6546 = G__6558;
i__6543_6547 = G__6559;
continue;
}
} else
{var n_6560 = cljs.core.first.call(null,seq__6540_6554__$1);n_6560.setAttribute(cljs.core.name.call(null,name),cljs.core.apply.call(null,cljs.core.str,value));
{
var G__6561 = cljs.core.next.call(null,seq__6540_6554__$1);
var G__6562 = null;
var G__6563 = 0;
var G__6564 = 0;
seq__6540_6544 = G__6561;
chunk__6541_6545 = G__6562;
count__6542_6546 = G__6563;
i__6543_6547 = G__6564;
continue;
}
}
} else
{}
}
break;
}
return content;
};
var set_attr_BANG_ = function (content,name,var_args){
var value = null;if (arguments.length > 2) {
  value = cljs.core.array_seq(Array.prototype.slice.call(arguments, 2),0);} 
return set_attr_BANG___delegate.call(this,content,name,value);};
set_attr_BANG_.cljs$lang$maxFixedArity = 2;
set_attr_BANG_.cljs$lang$applyTo = (function (arglist__6565){
var content = cljs.core.first(arglist__6565);
arglist__6565 = cljs.core.next(arglist__6565);
var name = cljs.core.first(arglist__6565);
var value = cljs.core.rest(arglist__6565);
return set_attr_BANG___delegate(content,name,value);
});
set_attr_BANG_.cljs$core$IFn$_invoke$arity$variadic = set_attr_BANG___delegate;
return set_attr_BANG_;
})()
;
/**
* Removes the specified HTML property for each node in the content. Name may be a string or keyword.
*/
domina.remove_attr_BANG_ = (function remove_attr_BANG_(content,name){var seq__6570_6574 = cljs.core.seq.call(null,domina.nodes.call(null,content));var chunk__6571_6575 = null;var count__6572_6576 = 0;var i__6573_6577 = 0;while(true){
if((i__6573_6577 < count__6572_6576))
{var n_6578 = cljs.core._nth.call(null,chunk__6571_6575,i__6573_6577);n_6578.removeAttribute(cljs.core.name.call(null,name));
{
var G__6579 = seq__6570_6574;
var G__6580 = chunk__6571_6575;
var G__6581 = count__6572_6576;
var G__6582 = (i__6573_6577 + 1);
seq__6570_6574 = G__6579;
chunk__6571_6575 = G__6580;
count__6572_6576 = G__6581;
i__6573_6577 = G__6582;
continue;
}
} else
{var temp__4092__auto___6583 = cljs.core.seq.call(null,seq__6570_6574);if(temp__4092__auto___6583)
{var seq__6570_6584__$1 = temp__4092__auto___6583;if(cljs.core.chunked_seq_QMARK_.call(null,seq__6570_6584__$1))
{var c__5393__auto___6585 = cljs.core.chunk_first.call(null,seq__6570_6584__$1);{
var G__6586 = cljs.core.chunk_rest.call(null,seq__6570_6584__$1);
var G__6587 = c__5393__auto___6585;
var G__6588 = cljs.core.count.call(null,c__5393__auto___6585);
var G__6589 = 0;
seq__6570_6574 = G__6586;
chunk__6571_6575 = G__6587;
count__6572_6576 = G__6588;
i__6573_6577 = G__6589;
continue;
}
} else
{var n_6590 = cljs.core.first.call(null,seq__6570_6584__$1);n_6590.removeAttribute(cljs.core.name.call(null,name));
{
var G__6591 = cljs.core.next.call(null,seq__6570_6584__$1);
var G__6592 = null;
var G__6593 = 0;
var G__6594 = 0;
seq__6570_6574 = G__6591;
chunk__6571_6575 = G__6592;
count__6572_6576 = G__6593;
i__6573_6577 = G__6594;
continue;
}
}
} else
{}
}
break;
}
return content;
});
/**
* Parses a CSS style string and returns the properties as a map.
*/
domina.parse_style_attributes = (function parse_style_attributes(style){return cljs.core.reduce.call(null,(function (acc,pair){var vec__6596 = pair.split(/\s*:\s*/);var k = cljs.core.nth.call(null,vec__6596,0,null);var v = cljs.core.nth.call(null,vec__6596,1,null);if(cljs.core.truth_((function (){var and__4667__auto__ = k;if(cljs.core.truth_(and__4667__auto__))
{return v;
} else
{return and__4667__auto__;
}
})()))
{return cljs.core.assoc.call(null,acc,cljs.core.keyword.call(null,k.toLowerCase()),v);
} else
{return acc;
}
}),cljs.core.PersistentArrayMap.EMPTY,style.split(/\s*;\s*/));
});
/**
* Returns a map of the CSS styles/values. Assumes content will be a single node. Style names are returned as keywords.
*/
domina.styles = (function styles(content){var style = domina.attr.call(null,content,"style");if(typeof style === 'string')
{return domina.parse_style_attributes.call(null,style);
} else
{if((style == null))
{return cljs.core.PersistentArrayMap.EMPTY;
} else
{if(cljs.core.truth_(style.cssText))
{return domina.parse_style_attributes.call(null,style.cssText);
} else
{if(new cljs.core.Keyword(null,"else","else",1017020587))
{return cljs.core.PersistentArrayMap.EMPTY;
} else
{return null;
}
}
}
}
});
/**
* Returns a map of the HTML attributes/values. Assumes content will be a single node. Attribute names are returned as keywords.
*/
domina.attrs = (function attrs(content){var node = domina.single_node.call(null,content);var attrs__$1 = node.attributes;return cljs.core.reduce.call(null,cljs.core.conj,cljs.core.filter.call(null,cljs.core.complement.call(null,cljs.core.nil_QMARK_),cljs.core.map.call(null,(function (p1__6597_SHARP_){var attr = attrs__$1.item(p1__6597_SHARP_);var value = attr.nodeValue;if((cljs.core.not_EQ_.call(null,null,value)) && (cljs.core.not_EQ_.call(null,"",value)))
{return cljs.core.PersistentArrayMap.fromArray([cljs.core.keyword.call(null,attr.nodeName.toLowerCase()),attr.nodeValue], true);
} else
{return null;
}
}),cljs.core.range.call(null,attrs__$1.length))));
});
/**
* Sets the specified CSS styles for each node in the content, given a map of names and values. Style names may be keywords or strings.
*/
domina.set_styles_BANG_ = (function set_styles_BANG_(content,styles){var seq__6604_6610 = cljs.core.seq.call(null,styles);var chunk__6605_6611 = null;var count__6606_6612 = 0;var i__6607_6613 = 0;while(true){
if((i__6607_6613 < count__6606_6612))
{var vec__6608_6614 = cljs.core._nth.call(null,chunk__6605_6611,i__6607_6613);var name_6615 = cljs.core.nth.call(null,vec__6608_6614,0,null);var value_6616 = cljs.core.nth.call(null,vec__6608_6614,1,null);domina.set_style_BANG_.call(null,content,name_6615,value_6616);
{
var G__6617 = seq__6604_6610;
var G__6618 = chunk__6605_6611;
var G__6619 = count__6606_6612;
var G__6620 = (i__6607_6613 + 1);
seq__6604_6610 = G__6617;
chunk__6605_6611 = G__6618;
count__6606_6612 = G__6619;
i__6607_6613 = G__6620;
continue;
}
} else
{var temp__4092__auto___6621 = cljs.core.seq.call(null,seq__6604_6610);if(temp__4092__auto___6621)
{var seq__6604_6622__$1 = temp__4092__auto___6621;if(cljs.core.chunked_seq_QMARK_.call(null,seq__6604_6622__$1))
{var c__5393__auto___6623 = cljs.core.chunk_first.call(null,seq__6604_6622__$1);{
var G__6624 = cljs.core.chunk_rest.call(null,seq__6604_6622__$1);
var G__6625 = c__5393__auto___6623;
var G__6626 = cljs.core.count.call(null,c__5393__auto___6623);
var G__6627 = 0;
seq__6604_6610 = G__6624;
chunk__6605_6611 = G__6625;
count__6606_6612 = G__6626;
i__6607_6613 = G__6627;
continue;
}
} else
{var vec__6609_6628 = cljs.core.first.call(null,seq__6604_6622__$1);var name_6629 = cljs.core.nth.call(null,vec__6609_6628,0,null);var value_6630 = cljs.core.nth.call(null,vec__6609_6628,1,null);domina.set_style_BANG_.call(null,content,name_6629,value_6630);
{
var G__6631 = cljs.core.next.call(null,seq__6604_6622__$1);
var G__6632 = null;
var G__6633 = 0;
var G__6634 = 0;
seq__6604_6610 = G__6631;
chunk__6605_6611 = G__6632;
count__6606_6612 = G__6633;
i__6607_6613 = G__6634;
continue;
}
}
} else
{}
}
break;
}
return content;
});
/**
* Sets the specified attributes for each node in the content, given a map of names and values. Names may be a string or keyword. Values will be cast to a string, multiple values wil be concatenated.
*/
domina.set_attrs_BANG_ = (function set_attrs_BANG_(content,attrs){var seq__6641_6647 = cljs.core.seq.call(null,attrs);var chunk__6642_6648 = null;var count__6643_6649 = 0;var i__6644_6650 = 0;while(true){
if((i__6644_6650 < count__6643_6649))
{var vec__6645_6651 = cljs.core._nth.call(null,chunk__6642_6648,i__6644_6650);var name_6652 = cljs.core.nth.call(null,vec__6645_6651,0,null);var value_6653 = cljs.core.nth.call(null,vec__6645_6651,1,null);domina.set_attr_BANG_.call(null,content,name_6652,value_6653);
{
var G__6654 = seq__6641_6647;
var G__6655 = chunk__6642_6648;
var G__6656 = count__6643_6649;
var G__6657 = (i__6644_6650 + 1);
seq__6641_6647 = G__6654;
chunk__6642_6648 = G__6655;
count__6643_6649 = G__6656;
i__6644_6650 = G__6657;
continue;
}
} else
{var temp__4092__auto___6658 = cljs.core.seq.call(null,seq__6641_6647);if(temp__4092__auto___6658)
{var seq__6641_6659__$1 = temp__4092__auto___6658;if(cljs.core.chunked_seq_QMARK_.call(null,seq__6641_6659__$1))
{var c__5393__auto___6660 = cljs.core.chunk_first.call(null,seq__6641_6659__$1);{
var G__6661 = cljs.core.chunk_rest.call(null,seq__6641_6659__$1);
var G__6662 = c__5393__auto___6660;
var G__6663 = cljs.core.count.call(null,c__5393__auto___6660);
var G__6664 = 0;
seq__6641_6647 = G__6661;
chunk__6642_6648 = G__6662;
count__6643_6649 = G__6663;
i__6644_6650 = G__6664;
continue;
}
} else
{var vec__6646_6665 = cljs.core.first.call(null,seq__6641_6659__$1);var name_6666 = cljs.core.nth.call(null,vec__6646_6665,0,null);var value_6667 = cljs.core.nth.call(null,vec__6646_6665,1,null);domina.set_attr_BANG_.call(null,content,name_6666,value_6667);
{
var G__6668 = cljs.core.next.call(null,seq__6641_6659__$1);
var G__6669 = null;
var G__6670 = 0;
var G__6671 = 0;
seq__6641_6647 = G__6668;
chunk__6642_6648 = G__6669;
count__6643_6649 = G__6670;
i__6644_6650 = G__6671;
continue;
}
}
} else
{}
}
break;
}
return content;
});
/**
* Returns true if the node has the specified CSS class. Assumes content is a single node.
*/
domina.has_class_QMARK_ = (function has_class_QMARK_(content,class$){return goog.dom.classes.has(domina.single_node.call(null,content),class$);
});
/**
* Adds the specified CSS class to each node in the content.
*/
domina.add_class_BANG_ = (function add_class_BANG_(content,class$){var seq__6676_6680 = cljs.core.seq.call(null,domina.nodes.call(null,content));var chunk__6677_6681 = null;var count__6678_6682 = 0;var i__6679_6683 = 0;while(true){
if((i__6679_6683 < count__6678_6682))
{var node_6684 = cljs.core._nth.call(null,chunk__6677_6681,i__6679_6683);goog.dom.classes.add(node_6684,class$);
{
var G__6685 = seq__6676_6680;
var G__6686 = chunk__6677_6681;
var G__6687 = count__6678_6682;
var G__6688 = (i__6679_6683 + 1);
seq__6676_6680 = G__6685;
chunk__6677_6681 = G__6686;
count__6678_6682 = G__6687;
i__6679_6683 = G__6688;
continue;
}
} else
{var temp__4092__auto___6689 = cljs.core.seq.call(null,seq__6676_6680);if(temp__4092__auto___6689)
{var seq__6676_6690__$1 = temp__4092__auto___6689;if(cljs.core.chunked_seq_QMARK_.call(null,seq__6676_6690__$1))
{var c__5393__auto___6691 = cljs.core.chunk_first.call(null,seq__6676_6690__$1);{
var G__6692 = cljs.core.chunk_rest.call(null,seq__6676_6690__$1);
var G__6693 = c__5393__auto___6691;
var G__6694 = cljs.core.count.call(null,c__5393__auto___6691);
var G__6695 = 0;
seq__6676_6680 = G__6692;
chunk__6677_6681 = G__6693;
count__6678_6682 = G__6694;
i__6679_6683 = G__6695;
continue;
}
} else
{var node_6696 = cljs.core.first.call(null,seq__6676_6690__$1);goog.dom.classes.add(node_6696,class$);
{
var G__6697 = cljs.core.next.call(null,seq__6676_6690__$1);
var G__6698 = null;
var G__6699 = 0;
var G__6700 = 0;
seq__6676_6680 = G__6697;
chunk__6677_6681 = G__6698;
count__6678_6682 = G__6699;
i__6679_6683 = G__6700;
continue;
}
}
} else
{}
}
break;
}
return content;
});
/**
* Removes the specified CSS class from each node in the content.
*/
domina.remove_class_BANG_ = (function remove_class_BANG_(content,class$){var seq__6705_6709 = cljs.core.seq.call(null,domina.nodes.call(null,content));var chunk__6706_6710 = null;var count__6707_6711 = 0;var i__6708_6712 = 0;while(true){
if((i__6708_6712 < count__6707_6711))
{var node_6713 = cljs.core._nth.call(null,chunk__6706_6710,i__6708_6712);goog.dom.classes.remove(node_6713,class$);
{
var G__6714 = seq__6705_6709;
var G__6715 = chunk__6706_6710;
var G__6716 = count__6707_6711;
var G__6717 = (i__6708_6712 + 1);
seq__6705_6709 = G__6714;
chunk__6706_6710 = G__6715;
count__6707_6711 = G__6716;
i__6708_6712 = G__6717;
continue;
}
} else
{var temp__4092__auto___6718 = cljs.core.seq.call(null,seq__6705_6709);if(temp__4092__auto___6718)
{var seq__6705_6719__$1 = temp__4092__auto___6718;if(cljs.core.chunked_seq_QMARK_.call(null,seq__6705_6719__$1))
{var c__5393__auto___6720 = cljs.core.chunk_first.call(null,seq__6705_6719__$1);{
var G__6721 = cljs.core.chunk_rest.call(null,seq__6705_6719__$1);
var G__6722 = c__5393__auto___6720;
var G__6723 = cljs.core.count.call(null,c__5393__auto___6720);
var G__6724 = 0;
seq__6705_6709 = G__6721;
chunk__6706_6710 = G__6722;
count__6707_6711 = G__6723;
i__6708_6712 = G__6724;
continue;
}
} else
{var node_6725 = cljs.core.first.call(null,seq__6705_6719__$1);goog.dom.classes.remove(node_6725,class$);
{
var G__6726 = cljs.core.next.call(null,seq__6705_6719__$1);
var G__6727 = null;
var G__6728 = 0;
var G__6729 = 0;
seq__6705_6709 = G__6726;
chunk__6706_6710 = G__6727;
count__6707_6711 = G__6728;
i__6708_6712 = G__6729;
continue;
}
}
} else
{}
}
break;
}
return content;
});
/**
* Toggles the specified CSS class from each node in the content.
*/
domina.toggle_class_BANG_ = (function toggle_class_BANG_(content,class$){var seq__6734_6738 = cljs.core.seq.call(null,domina.nodes.call(null,content));var chunk__6735_6739 = null;var count__6736_6740 = 0;var i__6737_6741 = 0;while(true){
if((i__6737_6741 < count__6736_6740))
{var node_6742 = cljs.core._nth.call(null,chunk__6735_6739,i__6737_6741);goog.dom.classes.toggle(node_6742,class$);
{
var G__6743 = seq__6734_6738;
var G__6744 = chunk__6735_6739;
var G__6745 = count__6736_6740;
var G__6746 = (i__6737_6741 + 1);
seq__6734_6738 = G__6743;
chunk__6735_6739 = G__6744;
count__6736_6740 = G__6745;
i__6737_6741 = G__6746;
continue;
}
} else
{var temp__4092__auto___6747 = cljs.core.seq.call(null,seq__6734_6738);if(temp__4092__auto___6747)
{var seq__6734_6748__$1 = temp__4092__auto___6747;if(cljs.core.chunked_seq_QMARK_.call(null,seq__6734_6748__$1))
{var c__5393__auto___6749 = cljs.core.chunk_first.call(null,seq__6734_6748__$1);{
var G__6750 = cljs.core.chunk_rest.call(null,seq__6734_6748__$1);
var G__6751 = c__5393__auto___6749;
var G__6752 = cljs.core.count.call(null,c__5393__auto___6749);
var G__6753 = 0;
seq__6734_6738 = G__6750;
chunk__6735_6739 = G__6751;
count__6736_6740 = G__6752;
i__6737_6741 = G__6753;
continue;
}
} else
{var node_6754 = cljs.core.first.call(null,seq__6734_6748__$1);goog.dom.classes.toggle(node_6754,class$);
{
var G__6755 = cljs.core.next.call(null,seq__6734_6748__$1);
var G__6756 = null;
var G__6757 = 0;
var G__6758 = 0;
seq__6734_6738 = G__6755;
chunk__6735_6739 = G__6756;
count__6736_6740 = G__6757;
i__6737_6741 = G__6758;
continue;
}
}
} else
{}
}
break;
}
return content;
});
/**
* Returns a seq of all the CSS classes currently applied to a node. Assumes content is a single node.
*/
domina.classes = (function classes(content){return cljs.core.seq.call(null,goog.dom.classes.get(domina.single_node.call(null,content)));
});
/**
* Sets the class attribute of the content nodes to classes, which can
* be either a class attribute string or a seq of classname strings.
*/
domina.set_classes_BANG_ = (function set_classes_BANG_(content,classes){var classes_6767__$1 = ((cljs.core.coll_QMARK_.call(null,classes))?clojure.string.join.call(null," ",classes):classes);var seq__6763_6768 = cljs.core.seq.call(null,domina.nodes.call(null,content));var chunk__6764_6769 = null;var count__6765_6770 = 0;var i__6766_6771 = 0;while(true){
if((i__6766_6771 < count__6765_6770))
{var node_6772 = cljs.core._nth.call(null,chunk__6764_6769,i__6766_6771);goog.dom.classes.set(node_6772,classes_6767__$1);
{
var G__6773 = seq__6763_6768;
var G__6774 = chunk__6764_6769;
var G__6775 = count__6765_6770;
var G__6776 = (i__6766_6771 + 1);
seq__6763_6768 = G__6773;
chunk__6764_6769 = G__6774;
count__6765_6770 = G__6775;
i__6766_6771 = G__6776;
continue;
}
} else
{var temp__4092__auto___6777 = cljs.core.seq.call(null,seq__6763_6768);if(temp__4092__auto___6777)
{var seq__6763_6778__$1 = temp__4092__auto___6777;if(cljs.core.chunked_seq_QMARK_.call(null,seq__6763_6778__$1))
{var c__5393__auto___6779 = cljs.core.chunk_first.call(null,seq__6763_6778__$1);{
var G__6780 = cljs.core.chunk_rest.call(null,seq__6763_6778__$1);
var G__6781 = c__5393__auto___6779;
var G__6782 = cljs.core.count.call(null,c__5393__auto___6779);
var G__6783 = 0;
seq__6763_6768 = G__6780;
chunk__6764_6769 = G__6781;
count__6765_6770 = G__6782;
i__6766_6771 = G__6783;
continue;
}
} else
{var node_6784 = cljs.core.first.call(null,seq__6763_6778__$1);goog.dom.classes.set(node_6784,classes_6767__$1);
{
var G__6785 = cljs.core.next.call(null,seq__6763_6778__$1);
var G__6786 = null;
var G__6787 = 0;
var G__6788 = 0;
seq__6763_6768 = G__6785;
chunk__6764_6769 = G__6786;
count__6765_6770 = G__6787;
i__6766_6771 = G__6788;
continue;
}
}
} else
{}
}
break;
}
return content;
});
/**
* Returns the text of a node. Assumes content is a single node. For consistency across browsers, will always trim whitespace of the beginning and end of the returned text.
*/
domina.text = (function text(content){return goog.string.trim(goog.dom.getTextContent(domina.single_node.call(null,content)));
});
/**
* Sets the text value of all the nodes in the given content.
*/
domina.set_text_BANG_ = (function set_text_BANG_(content,value){var seq__6793_6797 = cljs.core.seq.call(null,domina.nodes.call(null,content));var chunk__6794_6798 = null;var count__6795_6799 = 0;var i__6796_6800 = 0;while(true){
if((i__6796_6800 < count__6795_6799))
{var node_6801 = cljs.core._nth.call(null,chunk__6794_6798,i__6796_6800);goog.dom.setTextContent(node_6801,value);
{
var G__6802 = seq__6793_6797;
var G__6803 = chunk__6794_6798;
var G__6804 = count__6795_6799;
var G__6805 = (i__6796_6800 + 1);
seq__6793_6797 = G__6802;
chunk__6794_6798 = G__6803;
count__6795_6799 = G__6804;
i__6796_6800 = G__6805;
continue;
}
} else
{var temp__4092__auto___6806 = cljs.core.seq.call(null,seq__6793_6797);if(temp__4092__auto___6806)
{var seq__6793_6807__$1 = temp__4092__auto___6806;if(cljs.core.chunked_seq_QMARK_.call(null,seq__6793_6807__$1))
{var c__5393__auto___6808 = cljs.core.chunk_first.call(null,seq__6793_6807__$1);{
var G__6809 = cljs.core.chunk_rest.call(null,seq__6793_6807__$1);
var G__6810 = c__5393__auto___6808;
var G__6811 = cljs.core.count.call(null,c__5393__auto___6808);
var G__6812 = 0;
seq__6793_6797 = G__6809;
chunk__6794_6798 = G__6810;
count__6795_6799 = G__6811;
i__6796_6800 = G__6812;
continue;
}
} else
{var node_6813 = cljs.core.first.call(null,seq__6793_6807__$1);goog.dom.setTextContent(node_6813,value);
{
var G__6814 = cljs.core.next.call(null,seq__6793_6807__$1);
var G__6815 = null;
var G__6816 = 0;
var G__6817 = 0;
seq__6793_6797 = G__6814;
chunk__6794_6798 = G__6815;
count__6795_6799 = G__6816;
i__6796_6800 = G__6817;
continue;
}
}
} else
{}
}
break;
}
return content;
});
/**
* Returns the value of a node (presumably a form field). Assumes content is a single node.
*/
domina.value = (function value(content){return goog.dom.forms.getValue(domina.single_node.call(null,content));
});
/**
* Sets the value of all the nodes (presumably form fields) in the given content.
*/
domina.set_value_BANG_ = (function set_value_BANG_(content,value){var seq__6822_6826 = cljs.core.seq.call(null,domina.nodes.call(null,content));var chunk__6823_6827 = null;var count__6824_6828 = 0;var i__6825_6829 = 0;while(true){
if((i__6825_6829 < count__6824_6828))
{var node_6830 = cljs.core._nth.call(null,chunk__6823_6827,i__6825_6829);goog.dom.forms.setValue(node_6830,value);
{
var G__6831 = seq__6822_6826;
var G__6832 = chunk__6823_6827;
var G__6833 = count__6824_6828;
var G__6834 = (i__6825_6829 + 1);
seq__6822_6826 = G__6831;
chunk__6823_6827 = G__6832;
count__6824_6828 = G__6833;
i__6825_6829 = G__6834;
continue;
}
} else
{var temp__4092__auto___6835 = cljs.core.seq.call(null,seq__6822_6826);if(temp__4092__auto___6835)
{var seq__6822_6836__$1 = temp__4092__auto___6835;if(cljs.core.chunked_seq_QMARK_.call(null,seq__6822_6836__$1))
{var c__5393__auto___6837 = cljs.core.chunk_first.call(null,seq__6822_6836__$1);{
var G__6838 = cljs.core.chunk_rest.call(null,seq__6822_6836__$1);
var G__6839 = c__5393__auto___6837;
var G__6840 = cljs.core.count.call(null,c__5393__auto___6837);
var G__6841 = 0;
seq__6822_6826 = G__6838;
chunk__6823_6827 = G__6839;
count__6824_6828 = G__6840;
i__6825_6829 = G__6841;
continue;
}
} else
{var node_6842 = cljs.core.first.call(null,seq__6822_6836__$1);goog.dom.forms.setValue(node_6842,value);
{
var G__6843 = cljs.core.next.call(null,seq__6822_6836__$1);
var G__6844 = null;
var G__6845 = 0;
var G__6846 = 0;
seq__6822_6826 = G__6843;
chunk__6823_6827 = G__6844;
count__6824_6828 = G__6845;
i__6825_6829 = G__6846;
continue;
}
}
} else
{}
}
break;
}
return content;
});
/**
* Returns the innerHTML of a node. Assumes content is a single node.
*/
domina.html = (function html(content){return domina.single_node.call(null,content).innerHTML;
});
domina.replace_children_BANG_ = (function replace_children_BANG_(content,inner_content){return domina.append_BANG_.call(null,domina.destroy_children_BANG_.call(null,content),inner_content);
});
domina.set_inner_html_BANG_ = (function set_inner_html_BANG_(content,html_string){var allows_inner_html_QMARK_ = cljs.core.not.call(null,cljs.core.re_find.call(null,domina.re_no_inner_html,html_string));var leading_whitespace_QMARK_ = cljs.core.re_find.call(null,domina.re_leading_whitespace,html_string);var tag_name = [cljs.core.str(cljs.core.second.call(null,cljs.core.re_find.call(null,domina.re_tag_name,html_string)))].join('').toLowerCase();var special_tag_QMARK_ = cljs.core.contains_QMARK_.call(null,domina.wrap_map,tag_name);if(cljs.core.truth_((function (){var and__4667__auto__ = allows_inner_html_QMARK_;if(and__4667__auto__)
{var and__4667__auto____$1 = (function (){var or__4676__auto__ = domina.support.leading_whitespace_QMARK_;if(cljs.core.truth_(or__4676__auto__))
{return or__4676__auto__;
} else
{return cljs.core.not.call(null,leading_whitespace_QMARK_);
}
})();if(cljs.core.truth_(and__4667__auto____$1))
{return !(special_tag_QMARK_);
} else
{return and__4667__auto____$1;
}
} else
{return and__4667__auto__;
}
})()))
{var value_6857 = clojure.string.replace.call(null,html_string,domina.re_xhtml_tag,"<$1></$2>");try{var seq__6853_6858 = cljs.core.seq.call(null,domina.nodes.call(null,content));var chunk__6854_6859 = null;var count__6855_6860 = 0;var i__6856_6861 = 0;while(true){
if((i__6856_6861 < count__6855_6860))
{var node_6862 = cljs.core._nth.call(null,chunk__6854_6859,i__6856_6861);node_6862.innerHTML = value_6857;
{
var G__6863 = seq__6853_6858;
var G__6864 = chunk__6854_6859;
var G__6865 = count__6855_6860;
var G__6866 = (i__6856_6861 + 1);
seq__6853_6858 = G__6863;
chunk__6854_6859 = G__6864;
count__6855_6860 = G__6865;
i__6856_6861 = G__6866;
continue;
}
} else
{var temp__4092__auto___6867 = cljs.core.seq.call(null,seq__6853_6858);if(temp__4092__auto___6867)
{var seq__6853_6868__$1 = temp__4092__auto___6867;if(cljs.core.chunked_seq_QMARK_.call(null,seq__6853_6868__$1))
{var c__5393__auto___6869 = cljs.core.chunk_first.call(null,seq__6853_6868__$1);{
var G__6870 = cljs.core.chunk_rest.call(null,seq__6853_6868__$1);
var G__6871 = c__5393__auto___6869;
var G__6872 = cljs.core.count.call(null,c__5393__auto___6869);
var G__6873 = 0;
seq__6853_6858 = G__6870;
chunk__6854_6859 = G__6871;
count__6855_6860 = G__6872;
i__6856_6861 = G__6873;
continue;
}
} else
{var node_6874 = cljs.core.first.call(null,seq__6853_6868__$1);node_6874.innerHTML = value_6857;
{
var G__6875 = cljs.core.next.call(null,seq__6853_6868__$1);
var G__6876 = null;
var G__6877 = 0;
var G__6878 = 0;
seq__6853_6858 = G__6875;
chunk__6854_6859 = G__6876;
count__6855_6860 = G__6877;
i__6856_6861 = G__6878;
continue;
}
}
} else
{}
}
break;
}
}catch (e6852){if((e6852 instanceof Error))
{var e_6879 = e6852;domina.replace_children_BANG_.call(null,content,value_6857);
} else
{if(new cljs.core.Keyword(null,"else","else",1017020587))
{throw e6852;
} else
{}
}
}} else
{domina.replace_children_BANG_.call(null,content,html_string);
}
return content;
});
/**
* Sets the innerHTML value for all the nodes in the given content.
*/
domina.set_html_BANG_ = (function set_html_BANG_(content,inner_content){if(typeof inner_content === 'string')
{return domina.set_inner_html_BANG_.call(null,content,inner_content);
} else
{return domina.replace_children_BANG_.call(null,content,inner_content);
}
});
/**
* Returns data associated with a node for a given key. Assumes
* content is a single node. If the bubble parameter is set to true,
* will search parent nodes if the key is not found.
*/
domina.get_data = (function() {
var get_data = null;
var get_data__2 = (function (node,key){return get_data.call(null,node,key,false);
});
var get_data__3 = (function (node,key,bubble){var m = domina.single_node.call(null,node).__domina_data;var value = (cljs.core.truth_(m)?cljs.core.get.call(null,m,key):null);if(cljs.core.truth_((function (){var and__4667__auto__ = bubble;if(cljs.core.truth_(and__4667__auto__))
{return (value == null);
} else
{return and__4667__auto__;
}
})()))
{var temp__4092__auto__ = domina.single_node.call(null,node).parentNode;if(cljs.core.truth_(temp__4092__auto__))
{var parent = temp__4092__auto__;return get_data.call(null,parent,key,true);
} else
{return null;
}
} else
{return value;
}
});
get_data = function(node,key,bubble){
switch(arguments.length){
case 2:
return get_data__2.call(this,node,key);
case 3:
return get_data__3.call(this,node,key,bubble);
}
throw(new Error('Invalid arity: ' + arguments.length));
};
get_data.cljs$core$IFn$_invoke$arity$2 = get_data__2;
get_data.cljs$core$IFn$_invoke$arity$3 = get_data__3;
return get_data;
})()
;
/**
* Sets a data on the node for a given key. Assumes content is a
* single node. Data should be ClojureScript values and data structures
* only; using other objects as data may result in memory leaks on some
* browsers.
*/
domina.set_data_BANG_ = (function set_data_BANG_(node,key,value){var m = (function (){var or__4676__auto__ = domina.single_node.call(null,node).__domina_data;if(cljs.core.truth_(or__4676__auto__))
{return or__4676__auto__;
} else
{return cljs.core.PersistentArrayMap.EMPTY;
}
})();return domina.single_node.call(null,node).__domina_data = cljs.core.assoc.call(null,m,key,value);
});
/**
* Takes a two-arg function, a reference DomContent and new
* DomContent. Applies the function for each reference / content
* combination. Uses clones of the new content for each additional
* parent after the first.
*/
domina.apply_with_cloning = (function apply_with_cloning(f,parent_content,child_content){var parents = domina.nodes.call(null,parent_content);var children = domina.nodes.call(null,child_content);var first_child = (function (){var frag = document.createDocumentFragment();var seq__6886_6890 = cljs.core.seq.call(null,children);var chunk__6887_6891 = null;var count__6888_6892 = 0;var i__6889_6893 = 0;while(true){
if((i__6889_6893 < count__6888_6892))
{var child_6894 = cljs.core._nth.call(null,chunk__6887_6891,i__6889_6893);frag.appendChild(child_6894);
{
var G__6895 = seq__6886_6890;
var G__6896 = chunk__6887_6891;
var G__6897 = count__6888_6892;
var G__6898 = (i__6889_6893 + 1);
seq__6886_6890 = G__6895;
chunk__6887_6891 = G__6896;
count__6888_6892 = G__6897;
i__6889_6893 = G__6898;
continue;
}
} else
{var temp__4092__auto___6899 = cljs.core.seq.call(null,seq__6886_6890);if(temp__4092__auto___6899)
{var seq__6886_6900__$1 = temp__4092__auto___6899;if(cljs.core.chunked_seq_QMARK_.call(null,seq__6886_6900__$1))
{var c__5393__auto___6901 = cljs.core.chunk_first.call(null,seq__6886_6900__$1);{
var G__6902 = cljs.core.chunk_rest.call(null,seq__6886_6900__$1);
var G__6903 = c__5393__auto___6901;
var G__6904 = cljs.core.count.call(null,c__5393__auto___6901);
var G__6905 = 0;
seq__6886_6890 = G__6902;
chunk__6887_6891 = G__6903;
count__6888_6892 = G__6904;
i__6889_6893 = G__6905;
continue;
}
} else
{var child_6906 = cljs.core.first.call(null,seq__6886_6900__$1);frag.appendChild(child_6906);
{
var G__6907 = cljs.core.next.call(null,seq__6886_6900__$1);
var G__6908 = null;
var G__6909 = 0;
var G__6910 = 0;
seq__6886_6890 = G__6907;
chunk__6887_6891 = G__6908;
count__6888_6892 = G__6909;
i__6889_6893 = G__6910;
continue;
}
}
} else
{}
}
break;
}
return frag;
})();var other_children = cljs.core.doall.call(null,cljs.core.repeatedly.call(null,(cljs.core.count.call(null,parents) - 1),((function (parents,children,first_child){
return (function (){return first_child.cloneNode(true);
});})(parents,children,first_child))
));if(cljs.core.seq.call(null,parents))
{f.call(null,cljs.core.first.call(null,parents),first_child);
return cljs.core.doall.call(null,cljs.core.map.call(null,(function (p1__6880_SHARP_,p2__6881_SHARP_){return f.call(null,p1__6880_SHARP_,p2__6881_SHARP_);
}),cljs.core.rest.call(null,parents),other_children));
} else
{return null;
}
});
domina.lazy_nl_via_item = (function() {
var lazy_nl_via_item = null;
var lazy_nl_via_item__1 = (function (nl){return lazy_nl_via_item.call(null,nl,0);
});
var lazy_nl_via_item__2 = (function (nl,n){if((n < nl.length))
{return (new cljs.core.LazySeq(null,(function (){return cljs.core.cons.call(null,nl.item(n),lazy_nl_via_item.call(null,nl,(n + 1)));
}),null,null));
} else
{return null;
}
});
lazy_nl_via_item = function(nl,n){
switch(arguments.length){
case 1:
return lazy_nl_via_item__1.call(this,nl);
case 2:
return lazy_nl_via_item__2.call(this,nl,n);
}
throw(new Error('Invalid arity: ' + arguments.length));
};
lazy_nl_via_item.cljs$core$IFn$_invoke$arity$1 = lazy_nl_via_item__1;
lazy_nl_via_item.cljs$core$IFn$_invoke$arity$2 = lazy_nl_via_item__2;
return lazy_nl_via_item;
})()
;
domina.lazy_nl_via_array_ref = (function() {
var lazy_nl_via_array_ref = null;
var lazy_nl_via_array_ref__1 = (function (nl){return lazy_nl_via_array_ref.call(null,nl,0);
});
var lazy_nl_via_array_ref__2 = (function (nl,n){if((n < nl.length))
{return (new cljs.core.LazySeq(null,(function (){return cljs.core.cons.call(null,(nl[n]),lazy_nl_via_array_ref.call(null,nl,(n + 1)));
}),null,null));
} else
{return null;
}
});
lazy_nl_via_array_ref = function(nl,n){
switch(arguments.length){
case 1:
return lazy_nl_via_array_ref__1.call(this,nl);
case 2:
return lazy_nl_via_array_ref__2.call(this,nl,n);
}
throw(new Error('Invalid arity: ' + arguments.length));
};
lazy_nl_via_array_ref.cljs$core$IFn$_invoke$arity$1 = lazy_nl_via_array_ref__1;
lazy_nl_via_array_ref.cljs$core$IFn$_invoke$arity$2 = lazy_nl_via_array_ref__2;
return lazy_nl_via_array_ref;
})()
;
/**
* A lazy seq view of a js/NodeList, or other array-like javascript things
*/
domina.lazy_nodelist = (function lazy_nodelist(nl){if(cljs.core.truth_(nl.item))
{return domina.lazy_nl_via_item.call(null,nl);
} else
{return domina.lazy_nl_via_array_ref.call(null,nl);
}
});
domina.array_like_QMARK_ = (function array_like_QMARK_(obj){var and__4667__auto__ = obj;if(cljs.core.truth_(and__4667__auto__))
{var and__4667__auto____$1 = cljs.core.not.call(null,obj.nodeName);if(and__4667__auto____$1)
{return obj.length;
} else
{return and__4667__auto____$1;
}
} else
{return and__4667__auto__;
}
});
/**
* Some versions of IE have things that are like arrays in that they
* respond to .length, but are not arrays nor NodeSets. This returns a
* real sequence view of such objects. If passed an object that is not
* a logical sequence at all, returns a single-item seq containing the
* object.
*/
domina.normalize_seq = (function normalize_seq(list_thing){if((list_thing == null))
{return cljs.core.List.EMPTY;
} else
{if((function (){var G__6912 = list_thing;if(G__6912)
{var bit__5295__auto__ = (G__6912.cljs$lang$protocol_mask$partition0$ & 8388608);if((bit__5295__auto__) || (G__6912.cljs$core$ISeqable$))
{return true;
} else
{if((!G__6912.cljs$lang$protocol_mask$partition0$))
{return cljs.core.native_satisfies_QMARK_.call(null,cljs.core.ISeqable,G__6912);
} else
{return false;
}
}
} else
{return cljs.core.native_satisfies_QMARK_.call(null,cljs.core.ISeqable,G__6912);
}
})())
{return cljs.core.seq.call(null,list_thing);
} else
{if(cljs.core.truth_(domina.array_like_QMARK_.call(null,list_thing)))
{return domina.lazy_nodelist.call(null,list_thing);
} else
{if(new cljs.core.Keyword(null,"default","default",2558708147))
{return cljs.core.seq.call(null,cljs.core.PersistentVector.fromArray([list_thing], true));
} else
{return null;
}
}
}
}
});
(domina.DomContent["_"] = true);
(domina.nodes["_"] = (function (content){if((content == null))
{return cljs.core.List.EMPTY;
} else
{if((function (){var G__6913 = content;if(G__6913)
{var bit__5295__auto__ = (G__6913.cljs$lang$protocol_mask$partition0$ & 8388608);if((bit__5295__auto__) || (G__6913.cljs$core$ISeqable$))
{return true;
} else
{if((!G__6913.cljs$lang$protocol_mask$partition0$))
{return cljs.core.native_satisfies_QMARK_.call(null,cljs.core.ISeqable,G__6913);
} else
{return false;
}
}
} else
{return cljs.core.native_satisfies_QMARK_.call(null,cljs.core.ISeqable,G__6913);
}
})())
{return cljs.core.seq.call(null,content);
} else
{if(cljs.core.truth_(domina.array_like_QMARK_.call(null,content)))
{return domina.lazy_nodelist.call(null,content);
} else
{if(new cljs.core.Keyword(null,"default","default",2558708147))
{return cljs.core.seq.call(null,cljs.core.PersistentVector.fromArray([content], true));
} else
{return null;
}
}
}
}
}));
(domina.single_node["_"] = (function (content){if((content == null))
{return null;
} else
{if((function (){var G__6914 = content;if(G__6914)
{var bit__5295__auto__ = (G__6914.cljs$lang$protocol_mask$partition0$ & 8388608);if((bit__5295__auto__) || (G__6914.cljs$core$ISeqable$))
{return true;
} else
{if((!G__6914.cljs$lang$protocol_mask$partition0$))
{return cljs.core.native_satisfies_QMARK_.call(null,cljs.core.ISeqable,G__6914);
} else
{return false;
}
}
} else
{return cljs.core.native_satisfies_QMARK_.call(null,cljs.core.ISeqable,G__6914);
}
})())
{return cljs.core.first.call(null,content);
} else
{if(cljs.core.truth_(domina.array_like_QMARK_.call(null,content)))
{return content.item(0);
} else
{if(new cljs.core.Keyword(null,"default","default",2558708147))
{return content;
} else
{return null;
}
}
}
}
}));
(domina.DomContent["string"] = true);
(domina.nodes["string"] = (function (s){return cljs.core.doall.call(null,domina.nodes.call(null,domina.string_to_dom.call(null,s)));
}));
(domina.single_node["string"] = (function (s){return domina.single_node.call(null,domina.string_to_dom.call(null,s));
}));
if(cljs.core.truth_((typeof NodeList != 'undefined')))
{NodeList.prototype.cljs$core$ISeqable$ = true;
NodeList.prototype.cljs$core$ISeqable$_seq$arity$1 = (function (nodelist){var nodelist__$1 = this;return domina.lazy_nodelist.call(null,nodelist__$1);
});
NodeList.prototype.cljs$core$IIndexed$ = true;
NodeList.prototype.cljs$core$IIndexed$_nth$arity$2 = (function (nodelist,n){var nodelist__$1 = this;return nodelist__$1.item(n);
});
NodeList.prototype.cljs$core$IIndexed$_nth$arity$3 = (function (nodelist,n,not_found){var nodelist__$1 = this;if((nodelist__$1.length <= n))
{return not_found;
} else
{return cljs.core.nth.call(null,nodelist__$1,n);
}
});
NodeList.prototype.cljs$core$ICounted$ = true;
NodeList.prototype.cljs$core$ICounted$_count$arity$1 = (function (nodelist){var nodelist__$1 = this;return nodelist__$1.length;
});
} else
{}
if(cljs.core.truth_((typeof StaticNodeList != 'undefined')))
{StaticNodeList.prototype.cljs$core$ISeqable$ = true;
StaticNodeList.prototype.cljs$core$ISeqable$_seq$arity$1 = (function (nodelist){var nodelist__$1 = this;return domina.lazy_nodelist.call(null,nodelist__$1);
});
StaticNodeList.prototype.cljs$core$IIndexed$ = true;
StaticNodeList.prototype.cljs$core$IIndexed$_nth$arity$2 = (function (nodelist,n){var nodelist__$1 = this;return nodelist__$1.item(n);
});
StaticNodeList.prototype.cljs$core$IIndexed$_nth$arity$3 = (function (nodelist,n,not_found){var nodelist__$1 = this;if((nodelist__$1.length <= n))
{return not_found;
} else
{return cljs.core.nth.call(null,nodelist__$1,n);
}
});
StaticNodeList.prototype.cljs$core$ICounted$ = true;
StaticNodeList.prototype.cljs$core$ICounted$_count$arity$1 = (function (nodelist){var nodelist__$1 = this;return nodelist__$1.length;
});
} else
{}
if(cljs.core.truth_((typeof HTMLCollection != 'undefined')))
{HTMLCollection.prototype.cljs$core$ISeqable$ = true;
HTMLCollection.prototype.cljs$core$ISeqable$_seq$arity$1 = (function (coll){var coll__$1 = this;return domina.lazy_nodelist.call(null,coll__$1);
});
HTMLCollection.prototype.cljs$core$IIndexed$ = true;
HTMLCollection.prototype.cljs$core$IIndexed$_nth$arity$2 = (function (coll,n){var coll__$1 = this;return coll__$1.item(n);
});
HTMLCollection.prototype.cljs$core$IIndexed$_nth$arity$3 = (function (coll,n,not_found){var coll__$1 = this;if((coll__$1.length <= n))
{return not_found;
} else
{return cljs.core.nth.call(null,coll__$1,n);
}
});
HTMLCollection.prototype.cljs$core$ICounted$ = true;
HTMLCollection.prototype.cljs$core$ICounted$_count$arity$1 = (function (coll){var coll__$1 = this;return coll__$1.length;
});
} else
{}
