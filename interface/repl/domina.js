// Compiled by ClojureScript 0.0-2030
goog.provide('domina');
goog.require('cljs.core');
goog.require('goog.dom.classes');
goog.require('domina.support');
goog.require('goog.dom.classes');
goog.require('cljs.core');
goog.require('goog.events');
goog.require('goog.string');
goog.require('goog.dom.xml');
goog.require('goog.dom');
goog.require('goog.dom.forms');
goog.require('goog.dom');
goog.require('goog.string');
goog.require('clojure.string');
goog.require('goog.style');
goog.require('clojure.string');
goog.require('goog.dom.xml');
goog.require('goog.style');
goog.require('goog.dom.forms');
goog.require('domina.support');
goog.require('goog.events');
goog.require('cljs.core');
domina.re_html = /<|&#?\w+;/;
domina.re_leading_whitespace = /^\s+/;
domina.re_xhtml_tag = /<(?!area|br|col|embed|hr|img|input|link|meta|param)(([\w:]+)[^>]*)\/>/i;
domina.re_tag_name = /<([\w:]+)/;
domina.re_no_inner_html = /<(?:script|style)/i;
domina.re_tbody = /<tbody/i;
var opt_wrapper_5067 = cljs.core.PersistentVector.fromArray([1,"<select multiple='multiple'>","</select>"], true);var table_section_wrapper_5068 = cljs.core.PersistentVector.fromArray([1,"<table>","</table>"], true);var cell_wrapper_5069 = cljs.core.PersistentVector.fromArray([3,"<table><tbody><tr>","</tr></tbody></table>"], true);domina.wrap_map = cljs.core.PersistentHashMap.fromArrays(["col",new cljs.core.Keyword(null,"default","default",2558708147),"tfoot","caption","optgroup","legend","area","td","thead","th","option","tbody","tr","colgroup"],[cljs.core.PersistentVector.fromArray([2,"<table><tbody></tbody><colgroup>","</colgroup></table>"], true),cljs.core.PersistentVector.fromArray([0,"",""], true),table_section_wrapper_5068,table_section_wrapper_5068,opt_wrapper_5067,cljs.core.PersistentVector.fromArray([1,"<fieldset>","</fieldset>"], true),cljs.core.PersistentVector.fromArray([1,"<map>","</map>"], true),cell_wrapper_5069,table_section_wrapper_5068,cell_wrapper_5069,opt_wrapper_5067,table_section_wrapper_5068,cljs.core.PersistentVector.fromArray([2,"<table><tbody>","</tbody></table>"], true),table_section_wrapper_5068]);
domina.remove_extraneous_tbody_BANG_ = (function remove_extraneous_tbody_BANG_(div,html,tag_name,start_wrap){var no_tbody_QMARK_ = cljs.core.not.call(null,cljs.core.re_find.call(null,domina.re_tbody,html));var tbody = (((cljs.core._EQ_.call(null,tag_name,"table")) && (no_tbody_QMARK_))?(function (){var and__3213__auto__ = div.firstChild;if(cljs.core.truth_(and__3213__auto__))
{return div.firstChild.childNodes;
} else
{return and__3213__auto__;
}
})():(((cljs.core._EQ_.call(null,start_wrap,"<table>")) && (no_tbody_QMARK_))?divchildNodes:cljs.core.PersistentVector.EMPTY));var seq__5074 = cljs.core.seq.call(null,tbody);var chunk__5075 = null;var count__5076 = 0;var i__5077 = 0;while(true){
if((i__5077 < count__5076))
{var child = cljs.core._nth.call(null,chunk__5075,i__5077);if((cljs.core._EQ_.call(null,child.nodeName,"tbody")) && (cljs.core._EQ_.call(null,child.childNodes.length,0)))
{child.parentNode.removeChild(child);
} else
{}
{
var G__5078 = seq__5074;
var G__5079 = chunk__5075;
var G__5080 = count__5076;
var G__5081 = (i__5077 + 1);
seq__5074 = G__5078;
chunk__5075 = G__5079;
count__5076 = G__5080;
i__5077 = G__5081;
continue;
}
} else
{var temp__4092__auto__ = cljs.core.seq.call(null,seq__5074);if(temp__4092__auto__)
{var seq__5074__$1 = temp__4092__auto__;if(cljs.core.chunked_seq_QMARK_.call(null,seq__5074__$1))
{var c__3939__auto__ = cljs.core.chunk_first.call(null,seq__5074__$1);{
var G__5082 = cljs.core.chunk_rest.call(null,seq__5074__$1);
var G__5083 = c__3939__auto__;
var G__5084 = cljs.core.count.call(null,c__3939__auto__);
var G__5085 = 0;
seq__5074 = G__5082;
chunk__5075 = G__5083;
count__5076 = G__5084;
i__5077 = G__5085;
continue;
}
} else
{var child = cljs.core.first.call(null,seq__5074__$1);if((cljs.core._EQ_.call(null,child.nodeName,"tbody")) && (cljs.core._EQ_.call(null,child.childNodes.length,0)))
{child.parentNode.removeChild(child);
} else
{}
{
var G__5086 = cljs.core.next.call(null,seq__5074__$1);
var G__5087 = null;
var G__5088 = 0;
var G__5089 = 0;
seq__5074 = G__5086;
chunk__5075 = G__5087;
count__5076 = G__5088;
i__5077 = G__5089;
continue;
}
}
} else
{return null;
}
}
break;
}
});
domina.restore_leading_whitespace_BANG_ = (function restore_leading_whitespace_BANG_(div,html){return div.insertBefore(document.createTextNode(cljs.core.first.call(null,cljs.core.re_find.call(null,domina.re_leading_whitespace,html))),div.firstChild);
});
/**
* takes an string of html and returns a NodeList of dom fragments
*/
domina.html_to_dom = (function html_to_dom(html){var html__$1 = clojure.string.replace.call(null,html,domina.re_xhtml_tag,"<$1></$2>");var tag_name = [cljs.core.str(cljs.core.second.call(null,cljs.core.re_find.call(null,domina.re_tag_name,html__$1)))].join('').toLowerCase();var vec__5091 = cljs.core.get.call(null,domina.wrap_map,tag_name,new cljs.core.Keyword(null,"default","default",2558708147).cljs$core$IFn$_invoke$arity$1(domina.wrap_map));var depth = cljs.core.nth.call(null,vec__5091,0,null);var start_wrap = cljs.core.nth.call(null,vec__5091,1,null);var end_wrap = cljs.core.nth.call(null,vec__5091,2,null);var div = (function (){var wrapper = (function (){var div = document.createElement("div");div.innerHTML = [cljs.core.str(start_wrap),cljs.core.str(html__$1),cljs.core.str(end_wrap)].join('');
return div;
})();var level = depth;while(true){
if((level > 0))
{{
var G__5092 = wrapper.lastChild;
var G__5093 = (level - 1);
wrapper = G__5092;
level = G__5093;
continue;
}
} else
{return wrapper;
}
break;
}
})();if(cljs.core.truth_(domina.support.extraneous_tbody_QMARK_))
{domina.remove_extraneous_tbody_BANG_.call(null,div,html__$1,tag_name,start_wrap);
} else
{}
if(cljs.core.truth_((function (){var and__3213__auto__ = cljs.core.not.call(null,domina.support.leading_whitespace_QMARK_);if(and__3213__auto__)
{return cljs.core.re_find.call(null,domina.re_leading_whitespace,html__$1);
} else
{return and__3213__auto__;
}
})()))
{domina.restore_leading_whitespace_BANG_.call(null,div,html__$1);
} else
{}
return div.childNodes;
});
domina.string_to_dom = (function string_to_dom(s){if(cljs.core.truth_(cljs.core.re_find.call(null,domina.re_html,s)))
{return domina.html_to_dom.call(null,s);
} else
{return document.createTextNode(s);
}
});
domina.DomContent = {};
domina.nodes = (function nodes(content){if((function (){var and__3213__auto__ = content;if(and__3213__auto__)
{return content.domina$DomContent$nodes$arity$1;
} else
{return and__3213__auto__;
}
})())
{return content.domina$DomContent$nodes$arity$1(content);
} else
{var x__3818__auto__ = (((content == null))?null:content);return (function (){var or__3222__auto__ = (domina.nodes[goog.typeOf(x__3818__auto__)]);if(or__3222__auto__)
{return or__3222__auto__;
} else
{var or__3222__auto____$1 = (domina.nodes["_"]);if(or__3222__auto____$1)
{return or__3222__auto____$1;
} else
{throw cljs.core.missing_protocol.call(null,"DomContent.nodes",content);
}
}
})().call(null,content);
}
});
domina.single_node = (function single_node(nodeseq){if((function (){var and__3213__auto__ = nodeseq;if(and__3213__auto__)
{return nodeseq.domina$DomContent$single_node$arity$1;
} else
{return and__3213__auto__;
}
})())
{return nodeseq.domina$DomContent$single_node$arity$1(nodeseq);
} else
{var x__3818__auto__ = (((nodeseq == null))?null:nodeseq);return (function (){var or__3222__auto__ = (domina.single_node[goog.typeOf(x__3818__auto__)]);if(or__3222__auto__)
{return or__3222__auto__;
} else
{var or__3222__auto____$1 = (domina.single_node["_"]);if(or__3222__auto____$1)
{return or__3222__auto____$1;
} else
{throw cljs.core.missing_protocol.call(null,"DomContent.single-node",nodeseq);
}
}
})().call(null,nodeseq);
}
});
domina._STAR_debug_STAR_ = true;
/**
* @param {...*} var_args
*/
domina.log_debug = (function() { 
var log_debug__delegate = function (mesg){if(cljs.core.truth_((function (){var and__3213__auto__ = domina._STAR_debug_STAR_;if(cljs.core.truth_(and__3213__auto__))
{return !(cljs.core._EQ_.call(null,window.console,undefined));
} else
{return and__3213__auto__;
}
})()))
{return console.log(cljs.core.apply.call(null,cljs.core.str,mesg));
} else
{return null;
}
};
var log_debug = function (var_args){
var mesg = null;if (arguments.length > 0) {
  mesg = cljs.core.array_seq(Array.prototype.slice.call(arguments, 0),0);} 
return log_debug__delegate.call(this,mesg);};
log_debug.cljs$lang$maxFixedArity = 0;
log_debug.cljs$lang$applyTo = (function (arglist__5094){
var mesg = cljs.core.seq(arglist__5094);
return log_debug__delegate(mesg);
});
log_debug.cljs$core$IFn$_invoke$arity$variadic = log_debug__delegate;
return log_debug;
})()
;
/**
* @param {...*} var_args
*/
domina.log = (function() { 
var log__delegate = function (mesg){if(cljs.core.truth_(window.console))
{return console.log(cljs.core.apply.call(null,cljs.core.str,mesg));
} else
{return null;
}
};
var log = function (var_args){
var mesg = null;if (arguments.length > 0) {
  mesg = cljs.core.array_seq(Array.prototype.slice.call(arguments, 0),0);} 
return log__delegate.call(this,mesg);};
log.cljs$lang$maxFixedArity = 0;
log.cljs$lang$applyTo = (function (arglist__5095){
var mesg = cljs.core.seq(arglist__5095);
return log__delegate(mesg);
});
log.cljs$core$IFn$_invoke$arity$variadic = log__delegate;
return log;
})()
;
/**
* Returns content containing a single node by looking up the given ID
*/
domina.by_id = (function by_id(id){return goog.dom.getElement(cljs.core.name.call(null,id));
});
/**
* Returns content containing nodes which have the specified CSS class.
*/
domina.by_class = (function by_class(class_name){return domina.normalize_seq.call(null,goog.dom.getElementsByClass(cljs.core.name.call(null,class_name)));
});
/**
* Gets all the child nodes of the elements in a content. Same as (xpath content '*') but more efficient.
*/
domina.children = (function children(content){return cljs.core.doall.call(null,cljs.core.mapcat.call(null,goog.dom.getChildren,domina.nodes.call(null,content)));
});
/**
* Returns the deepest common ancestor of the argument contents (which are presumed to be single nodes), or nil if they are from different documents.
* @param {...*} var_args
*/
domina.common_ancestor = (function() { 
var common_ancestor__delegate = function (contents){return cljs.core.apply.call(null,goog.dom.findCommonAncestor,cljs.core.map.call(null,domina.single_node,contents));
};
var common_ancestor = function (var_args){
var contents = null;if (arguments.length > 0) {
  contents = cljs.core.array_seq(Array.prototype.slice.call(arguments, 0),0);} 
return common_ancestor__delegate.call(this,contents);};
common_ancestor.cljs$lang$maxFixedArity = 0;
common_ancestor.cljs$lang$applyTo = (function (arglist__5096){
var contents = cljs.core.seq(arglist__5096);
return common_ancestor__delegate(contents);
});
common_ancestor.cljs$core$IFn$_invoke$arity$variadic = common_ancestor__delegate;
return common_ancestor;
})()
;
/**
* Returns true if the first argument is an ancestor of the second argument. Presumes both arguments are single-node contents.
*/
domina.ancestor_QMARK_ = (function ancestor_QMARK_(ancestor_content,descendant_content){return cljs.core._EQ_.call(null,domina.common_ancestor.call(null,ancestor_content,descendant_content),domina.single_node.call(null,ancestor_content));
});
/**
* Returns a deep clone of content.
*/
domina.clone = (function clone(content){return cljs.core.map.call(null,(function (p1__5097_SHARP_){return p1__5097_SHARP_.cloneNode(true);
}),domina.nodes.call(null,content));
});
/**
* Given a parent and child contents, appends each of the children to all of the parents. If there is more than one node in the parent content, clones the children for the additional parents. Returns the parent content.
*/
domina.append_BANG_ = (function append_BANG_(parent_content,child_content){domina.apply_with_cloning.call(null,goog.dom.appendChild,parent_content,child_content);
return parent_content;
});
/**
* Given a parent and child contents, appends each of the children to all of the parents at the specified index. If there is more than one node in the parent content, clones the children for the additional parents. Returns the parent content.
*/
domina.insert_BANG_ = (function insert_BANG_(parent_content,child_content,idx){domina.apply_with_cloning.call(null,(function (p1__5098_SHARP_,p2__5099_SHARP_){return goog.dom.insertChildAt(p1__5098_SHARP_,p2__5099_SHARP_,idx);
}),parent_content,child_content);
return parent_content;
});
/**
* Given a parent and child contents, prepends each of the children to all of the parents. If there is more than one node in the parent content, clones the children for the additional parents. Returns the parent content.
*/
domina.prepend_BANG_ = (function prepend_BANG_(parent_content,child_content){domina.insert_BANG_.call(null,parent_content,child_content,0);
return parent_content;
});
/**
* Given a content and some new content, inserts the new content immediately before the reference content. If there is more than one node in the reference content, clones the new content for each one.
*/
domina.insert_before_BANG_ = (function insert_before_BANG_(content,new_content){domina.apply_with_cloning.call(null,(function (p1__5101_SHARP_,p2__5100_SHARP_){return goog.dom.insertSiblingBefore(p2__5100_SHARP_,p1__5101_SHARP_);
}),content,new_content);
return content;
});
/**
* Given a content and some new content, inserts the new content immediately after the reference content. If there is more than one node in the reference content, clones the new content for each one.
*/
domina.insert_after_BANG_ = (function insert_after_BANG_(content,new_content){domina.apply_with_cloning.call(null,(function (p1__5103_SHARP_,p2__5102_SHARP_){return goog.dom.insertSiblingAfter(p2__5102_SHARP_,p1__5103_SHARP_);
}),content,new_content);
return content;
});
/**
* Given some old content and some new content, replaces the old content with new content. If there are multiple nodes in the old content, replaces each of them and clones the new content as necessary.
*/
domina.swap_content_BANG_ = (function swap_content_BANG_(old_content,new_content){domina.apply_with_cloning.call(null,(function (p1__5105_SHARP_,p2__5104_SHARP_){return goog.dom.replaceNode(p2__5104_SHARP_,p1__5105_SHARP_);
}),old_content,new_content);
return old_content;
});
/**
* Removes all the nodes in a content from the DOM and returns them.
*/
domina.detach_BANG_ = (function detach_BANG_(content){return cljs.core.doall.call(null,cljs.core.map.call(null,goog.dom.removeNode,domina.nodes.call(null,content)));
});
/**
* Removes all the nodes in a content from the DOM. Returns nil.
*/
domina.destroy_BANG_ = (function destroy_BANG_(content){return cljs.core.dorun.call(null,cljs.core.map.call(null,goog.dom.removeNode,domina.nodes.call(null,content)));
});
/**
* Removes all the child nodes in a content from the DOM. Returns the original content.
*/
domina.destroy_children_BANG_ = (function destroy_children_BANG_(content){cljs.core.dorun.call(null,cljs.core.map.call(null,goog.dom.removeChildren,domina.nodes.call(null,content)));
return content;
});
/**
* Gets the value of a CSS property. Assumes content will be a single node. Name may be a string or keyword. Returns nil if there is no value set for the style.
*/
domina.style = (function style(content,name){var s = goog.style.getStyle(domina.single_node.call(null,content),cljs.core.name.call(null,name));if(cljs.core.truth_(clojure.string.blank_QMARK_.call(null,s)))
{return null;
} else
{return s;
}
});
/**
* Gets the value of an HTML attribute. Assumes content will be a single node. Name may be a stirng or keyword. Returns nil if there is no value set for the style.
*/
domina.attr = (function attr(content,name){return domina.single_node.call(null,content).getAttribute(cljs.core.name.call(null,name));
});
/**
* Sets the value of a CSS property for each node in the content. Name may be a string or keyword. Value will be cast to a string, multiple values wil be concatenated.
* @param {...*} var_args
*/
domina.set_style_BANG_ = (function() { 
var set_style_BANG___delegate = function (content,name,value){var seq__5110_5114 = cljs.core.seq.call(null,domina.nodes.call(null,content));var chunk__5111_5115 = null;var count__5112_5116 = 0;var i__5113_5117 = 0;while(true){
if((i__5113_5117 < count__5112_5116))
{var n_5118 = cljs.core._nth.call(null,chunk__5111_5115,i__5113_5117);goog.style.setStyle(n_5118,cljs.core.name.call(null,name),cljs.core.apply.call(null,cljs.core.str,value));
{
var G__5119 = seq__5110_5114;
var G__5120 = chunk__5111_5115;
var G__5121 = count__5112_5116;
var G__5122 = (i__5113_5117 + 1);
seq__5110_5114 = G__5119;
chunk__5111_5115 = G__5120;
count__5112_5116 = G__5121;
i__5113_5117 = G__5122;
continue;
}
} else
{var temp__4092__auto___5123 = cljs.core.seq.call(null,seq__5110_5114);if(temp__4092__auto___5123)
{var seq__5110_5124__$1 = temp__4092__auto___5123;if(cljs.core.chunked_seq_QMARK_.call(null,seq__5110_5124__$1))
{var c__3939__auto___5125 = cljs.core.chunk_first.call(null,seq__5110_5124__$1);{
var G__5126 = cljs.core.chunk_rest.call(null,seq__5110_5124__$1);
var G__5127 = c__3939__auto___5125;
var G__5128 = cljs.core.count.call(null,c__3939__auto___5125);
var G__5129 = 0;
seq__5110_5114 = G__5126;
chunk__5111_5115 = G__5127;
count__5112_5116 = G__5128;
i__5113_5117 = G__5129;
continue;
}
} else
{var n_5130 = cljs.core.first.call(null,seq__5110_5124__$1);goog.style.setStyle(n_5130,cljs.core.name.call(null,name),cljs.core.apply.call(null,cljs.core.str,value));
{
var G__5131 = cljs.core.next.call(null,seq__5110_5124__$1);
var G__5132 = null;
var G__5133 = 0;
var G__5134 = 0;
seq__5110_5114 = G__5131;
chunk__5111_5115 = G__5132;
count__5112_5116 = G__5133;
i__5113_5117 = G__5134;
continue;
}
}
} else
{}
}
break;
}
return content;
};
var set_style_BANG_ = function (content,name,var_args){
var value = null;if (arguments.length > 2) {
  value = cljs.core.array_seq(Array.prototype.slice.call(arguments, 2),0);} 
return set_style_BANG___delegate.call(this,content,name,value);};
set_style_BANG_.cljs$lang$maxFixedArity = 2;
set_style_BANG_.cljs$lang$applyTo = (function (arglist__5135){
var content = cljs.core.first(arglist__5135);
arglist__5135 = cljs.core.next(arglist__5135);
var name = cljs.core.first(arglist__5135);
var value = cljs.core.rest(arglist__5135);
return set_style_BANG___delegate(content,name,value);
});
set_style_BANG_.cljs$core$IFn$_invoke$arity$variadic = set_style_BANG___delegate;
return set_style_BANG_;
})()
;
/**
* Sets the value of an HTML property for each node in the content. Name may be a string or keyword. Value will be cast to a string, multiple values wil be concatenated.
* @param {...*} var_args
*/
domina.set_attr_BANG_ = (function() { 
var set_attr_BANG___delegate = function (content,name,value){var seq__5140_5144 = cljs.core.seq.call(null,domina.nodes.call(null,content));var chunk__5141_5145 = null;var count__5142_5146 = 0;var i__5143_5147 = 0;while(true){
if((i__5143_5147 < count__5142_5146))
{var n_5148 = cljs.core._nth.call(null,chunk__5141_5145,i__5143_5147);n_5148.setAttribute(cljs.core.name.call(null,name),cljs.core.apply.call(null,cljs.core.str,value));
{
var G__5149 = seq__5140_5144;
var G__5150 = chunk__5141_5145;
var G__5151 = count__5142_5146;
var G__5152 = (i__5143_5147 + 1);
seq__5140_5144 = G__5149;
chunk__5141_5145 = G__5150;
count__5142_5146 = G__5151;
i__5143_5147 = G__5152;
continue;
}
} else
{var temp__4092__auto___5153 = cljs.core.seq.call(null,seq__5140_5144);if(temp__4092__auto___5153)
{var seq__5140_5154__$1 = temp__4092__auto___5153;if(cljs.core.chunked_seq_QMARK_.call(null,seq__5140_5154__$1))
{var c__3939__auto___5155 = cljs.core.chunk_first.call(null,seq__5140_5154__$1);{
var G__5156 = cljs.core.chunk_rest.call(null,seq__5140_5154__$1);
var G__5157 = c__3939__auto___5155;
var G__5158 = cljs.core.count.call(null,c__3939__auto___5155);
var G__5159 = 0;
seq__5140_5144 = G__5156;
chunk__5141_5145 = G__5157;
count__5142_5146 = G__5158;
i__5143_5147 = G__5159;
continue;
}
} else
{var n_5160 = cljs.core.first.call(null,seq__5140_5154__$1);n_5160.setAttribute(cljs.core.name.call(null,name),cljs.core.apply.call(null,cljs.core.str,value));
{
var G__5161 = cljs.core.next.call(null,seq__5140_5154__$1);
var G__5162 = null;
var G__5163 = 0;
var G__5164 = 0;
seq__5140_5144 = G__5161;
chunk__5141_5145 = G__5162;
count__5142_5146 = G__5163;
i__5143_5147 = G__5164;
continue;
}
}
} else
{}
}
break;
}
return content;
};
var set_attr_BANG_ = function (content,name,var_args){
var value = null;if (arguments.length > 2) {
  value = cljs.core.array_seq(Array.prototype.slice.call(arguments, 2),0);} 
return set_attr_BANG___delegate.call(this,content,name,value);};
set_attr_BANG_.cljs$lang$maxFixedArity = 2;
set_attr_BANG_.cljs$lang$applyTo = (function (arglist__5165){
var content = cljs.core.first(arglist__5165);
arglist__5165 = cljs.core.next(arglist__5165);
var name = cljs.core.first(arglist__5165);
var value = cljs.core.rest(arglist__5165);
return set_attr_BANG___delegate(content,name,value);
});
set_attr_BANG_.cljs$core$IFn$_invoke$arity$variadic = set_attr_BANG___delegate;
return set_attr_BANG_;
})()
;
/**
* Removes the specified HTML property for each node in the content. Name may be a string or keyword.
*/
domina.remove_attr_BANG_ = (function remove_attr_BANG_(content,name){var seq__5170_5174 = cljs.core.seq.call(null,domina.nodes.call(null,content));var chunk__5171_5175 = null;var count__5172_5176 = 0;var i__5173_5177 = 0;while(true){
if((i__5173_5177 < count__5172_5176))
{var n_5178 = cljs.core._nth.call(null,chunk__5171_5175,i__5173_5177);n_5178.removeAttribute(cljs.core.name.call(null,name));
{
var G__5179 = seq__5170_5174;
var G__5180 = chunk__5171_5175;
var G__5181 = count__5172_5176;
var G__5182 = (i__5173_5177 + 1);
seq__5170_5174 = G__5179;
chunk__5171_5175 = G__5180;
count__5172_5176 = G__5181;
i__5173_5177 = G__5182;
continue;
}
} else
{var temp__4092__auto___5183 = cljs.core.seq.call(null,seq__5170_5174);if(temp__4092__auto___5183)
{var seq__5170_5184__$1 = temp__4092__auto___5183;if(cljs.core.chunked_seq_QMARK_.call(null,seq__5170_5184__$1))
{var c__3939__auto___5185 = cljs.core.chunk_first.call(null,seq__5170_5184__$1);{
var G__5186 = cljs.core.chunk_rest.call(null,seq__5170_5184__$1);
var G__5187 = c__3939__auto___5185;
var G__5188 = cljs.core.count.call(null,c__3939__auto___5185);
var G__5189 = 0;
seq__5170_5174 = G__5186;
chunk__5171_5175 = G__5187;
count__5172_5176 = G__5188;
i__5173_5177 = G__5189;
continue;
}
} else
{var n_5190 = cljs.core.first.call(null,seq__5170_5184__$1);n_5190.removeAttribute(cljs.core.name.call(null,name));
{
var G__5191 = cljs.core.next.call(null,seq__5170_5184__$1);
var G__5192 = null;
var G__5193 = 0;
var G__5194 = 0;
seq__5170_5174 = G__5191;
chunk__5171_5175 = G__5192;
count__5172_5176 = G__5193;
i__5173_5177 = G__5194;
continue;
}
}
} else
{}
}
break;
}
return content;
});
/**
* Parses a CSS style string and returns the properties as a map.
*/
domina.parse_style_attributes = (function parse_style_attributes(style){return cljs.core.reduce.call(null,(function (acc,pair){var vec__5196 = pair.split(/\s*:\s*/);var k = cljs.core.nth.call(null,vec__5196,0,null);var v = cljs.core.nth.call(null,vec__5196,1,null);if(cljs.core.truth_((function (){var and__3213__auto__ = k;if(cljs.core.truth_(and__3213__auto__))
{return v;
} else
{return and__3213__auto__;
}
})()))
{return cljs.core.assoc.call(null,acc,cljs.core.keyword.call(null,k.toLowerCase()),v);
} else
{return acc;
}
}),cljs.core.PersistentArrayMap.EMPTY,style.split(/\s*;\s*/));
});
/**
* Returns a map of the CSS styles/values. Assumes content will be a single node. Style names are returned as keywords.
*/
domina.styles = (function styles(content){var style = domina.attr.call(null,content,"style");if(typeof style === 'string')
{return domina.parse_style_attributes.call(null,style);
} else
{if((style == null))
{return cljs.core.PersistentArrayMap.EMPTY;
} else
{if(cljs.core.truth_(style.cssText))
{return domina.parse_style_attributes.call(null,style.cssText);
} else
{if(new cljs.core.Keyword(null,"else","else",1017020587))
{return cljs.core.PersistentArrayMap.EMPTY;
} else
{return null;
}
}
}
}
});
/**
* Returns a map of the HTML attributes/values. Assumes content will be a single node. Attribute names are returned as keywords.
*/
domina.attrs = (function attrs(content){var node = domina.single_node.call(null,content);var attrs__$1 = node.attributes;return cljs.core.reduce.call(null,cljs.core.conj,cljs.core.filter.call(null,cljs.core.complement.call(null,cljs.core.nil_QMARK_),cljs.core.map.call(null,(function (p1__5197_SHARP_){var attr = attrs__$1.item(p1__5197_SHARP_);var value = attr.nodeValue;if((cljs.core.not_EQ_.call(null,null,value)) && (cljs.core.not_EQ_.call(null,"",value)))
{return cljs.core.PersistentArrayMap.fromArray([cljs.core.keyword.call(null,attr.nodeName.toLowerCase()),attr.nodeValue], true);
} else
{return null;
}
}),cljs.core.range.call(null,attrs__$1.length))));
});
/**
* Sets the specified CSS styles for each node in the content, given a map of names and values. Style names may be keywords or strings.
*/
domina.set_styles_BANG_ = (function set_styles_BANG_(content,styles){var seq__5204_5210 = cljs.core.seq.call(null,styles);var chunk__5205_5211 = null;var count__5206_5212 = 0;var i__5207_5213 = 0;while(true){
if((i__5207_5213 < count__5206_5212))
{var vec__5208_5214 = cljs.core._nth.call(null,chunk__5205_5211,i__5207_5213);var name_5215 = cljs.core.nth.call(null,vec__5208_5214,0,null);var value_5216 = cljs.core.nth.call(null,vec__5208_5214,1,null);domina.set_style_BANG_.call(null,content,name_5215,value_5216);
{
var G__5217 = seq__5204_5210;
var G__5218 = chunk__5205_5211;
var G__5219 = count__5206_5212;
var G__5220 = (i__5207_5213 + 1);
seq__5204_5210 = G__5217;
chunk__5205_5211 = G__5218;
count__5206_5212 = G__5219;
i__5207_5213 = G__5220;
continue;
}
} else
{var temp__4092__auto___5221 = cljs.core.seq.call(null,seq__5204_5210);if(temp__4092__auto___5221)
{var seq__5204_5222__$1 = temp__4092__auto___5221;if(cljs.core.chunked_seq_QMARK_.call(null,seq__5204_5222__$1))
{var c__3939__auto___5223 = cljs.core.chunk_first.call(null,seq__5204_5222__$1);{
var G__5224 = cljs.core.chunk_rest.call(null,seq__5204_5222__$1);
var G__5225 = c__3939__auto___5223;
var G__5226 = cljs.core.count.call(null,c__3939__auto___5223);
var G__5227 = 0;
seq__5204_5210 = G__5224;
chunk__5205_5211 = G__5225;
count__5206_5212 = G__5226;
i__5207_5213 = G__5227;
continue;
}
} else
{var vec__5209_5228 = cljs.core.first.call(null,seq__5204_5222__$1);var name_5229 = cljs.core.nth.call(null,vec__5209_5228,0,null);var value_5230 = cljs.core.nth.call(null,vec__5209_5228,1,null);domina.set_style_BANG_.call(null,content,name_5229,value_5230);
{
var G__5231 = cljs.core.next.call(null,seq__5204_5222__$1);
var G__5232 = null;
var G__5233 = 0;
var G__5234 = 0;
seq__5204_5210 = G__5231;
chunk__5205_5211 = G__5232;
count__5206_5212 = G__5233;
i__5207_5213 = G__5234;
continue;
}
}
} else
{}
}
break;
}
return content;
});
/**
* Sets the specified attributes for each node in the content, given a map of names and values. Names may be a string or keyword. Values will be cast to a string, multiple values wil be concatenated.
*/
domina.set_attrs_BANG_ = (function set_attrs_BANG_(content,attrs){var seq__5241_5247 = cljs.core.seq.call(null,attrs);var chunk__5242_5248 = null;var count__5243_5249 = 0;var i__5244_5250 = 0;while(true){
if((i__5244_5250 < count__5243_5249))
{var vec__5245_5251 = cljs.core._nth.call(null,chunk__5242_5248,i__5244_5250);var name_5252 = cljs.core.nth.call(null,vec__5245_5251,0,null);var value_5253 = cljs.core.nth.call(null,vec__5245_5251,1,null);domina.set_attr_BANG_.call(null,content,name_5252,value_5253);
{
var G__5254 = seq__5241_5247;
var G__5255 = chunk__5242_5248;
var G__5256 = count__5243_5249;
var G__5257 = (i__5244_5250 + 1);
seq__5241_5247 = G__5254;
chunk__5242_5248 = G__5255;
count__5243_5249 = G__5256;
i__5244_5250 = G__5257;
continue;
}
} else
{var temp__4092__auto___5258 = cljs.core.seq.call(null,seq__5241_5247);if(temp__4092__auto___5258)
{var seq__5241_5259__$1 = temp__4092__auto___5258;if(cljs.core.chunked_seq_QMARK_.call(null,seq__5241_5259__$1))
{var c__3939__auto___5260 = cljs.core.chunk_first.call(null,seq__5241_5259__$1);{
var G__5261 = cljs.core.chunk_rest.call(null,seq__5241_5259__$1);
var G__5262 = c__3939__auto___5260;
var G__5263 = cljs.core.count.call(null,c__3939__auto___5260);
var G__5264 = 0;
seq__5241_5247 = G__5261;
chunk__5242_5248 = G__5262;
count__5243_5249 = G__5263;
i__5244_5250 = G__5264;
continue;
}
} else
{var vec__5246_5265 = cljs.core.first.call(null,seq__5241_5259__$1);var name_5266 = cljs.core.nth.call(null,vec__5246_5265,0,null);var value_5267 = cljs.core.nth.call(null,vec__5246_5265,1,null);domina.set_attr_BANG_.call(null,content,name_5266,value_5267);
{
var G__5268 = cljs.core.next.call(null,seq__5241_5259__$1);
var G__5269 = null;
var G__5270 = 0;
var G__5271 = 0;
seq__5241_5247 = G__5268;
chunk__5242_5248 = G__5269;
count__5243_5249 = G__5270;
i__5244_5250 = G__5271;
continue;
}
}
} else
{}
}
break;
}
return content;
});
/**
* Returns true if the node has the specified CSS class. Assumes content is a single node.
*/
domina.has_class_QMARK_ = (function has_class_QMARK_(content,class$){return goog.dom.classes.has(domina.single_node.call(null,content),class$);
});
/**
* Adds the specified CSS class to each node in the content.
*/
domina.add_class_BANG_ = (function add_class_BANG_(content,class$){var seq__5276_5280 = cljs.core.seq.call(null,domina.nodes.call(null,content));var chunk__5277_5281 = null;var count__5278_5282 = 0;var i__5279_5283 = 0;while(true){
if((i__5279_5283 < count__5278_5282))
{var node_5284 = cljs.core._nth.call(null,chunk__5277_5281,i__5279_5283);goog.dom.classes.add(node_5284,class$);
{
var G__5285 = seq__5276_5280;
var G__5286 = chunk__5277_5281;
var G__5287 = count__5278_5282;
var G__5288 = (i__5279_5283 + 1);
seq__5276_5280 = G__5285;
chunk__5277_5281 = G__5286;
count__5278_5282 = G__5287;
i__5279_5283 = G__5288;
continue;
}
} else
{var temp__4092__auto___5289 = cljs.core.seq.call(null,seq__5276_5280);if(temp__4092__auto___5289)
{var seq__5276_5290__$1 = temp__4092__auto___5289;if(cljs.core.chunked_seq_QMARK_.call(null,seq__5276_5290__$1))
{var c__3939__auto___5291 = cljs.core.chunk_first.call(null,seq__5276_5290__$1);{
var G__5292 = cljs.core.chunk_rest.call(null,seq__5276_5290__$1);
var G__5293 = c__3939__auto___5291;
var G__5294 = cljs.core.count.call(null,c__3939__auto___5291);
var G__5295 = 0;
seq__5276_5280 = G__5292;
chunk__5277_5281 = G__5293;
count__5278_5282 = G__5294;
i__5279_5283 = G__5295;
continue;
}
} else
{var node_5296 = cljs.core.first.call(null,seq__5276_5290__$1);goog.dom.classes.add(node_5296,class$);
{
var G__5297 = cljs.core.next.call(null,seq__5276_5290__$1);
var G__5298 = null;
var G__5299 = 0;
var G__5300 = 0;
seq__5276_5280 = G__5297;
chunk__5277_5281 = G__5298;
count__5278_5282 = G__5299;
i__5279_5283 = G__5300;
continue;
}
}
} else
{}
}
break;
}
return content;
});
/**
* Removes the specified CSS class from each node in the content.
*/
domina.remove_class_BANG_ = (function remove_class_BANG_(content,class$){var seq__5305_5309 = cljs.core.seq.call(null,domina.nodes.call(null,content));var chunk__5306_5310 = null;var count__5307_5311 = 0;var i__5308_5312 = 0;while(true){
if((i__5308_5312 < count__5307_5311))
{var node_5313 = cljs.core._nth.call(null,chunk__5306_5310,i__5308_5312);goog.dom.classes.remove(node_5313,class$);
{
var G__5314 = seq__5305_5309;
var G__5315 = chunk__5306_5310;
var G__5316 = count__5307_5311;
var G__5317 = (i__5308_5312 + 1);
seq__5305_5309 = G__5314;
chunk__5306_5310 = G__5315;
count__5307_5311 = G__5316;
i__5308_5312 = G__5317;
continue;
}
} else
{var temp__4092__auto___5318 = cljs.core.seq.call(null,seq__5305_5309);if(temp__4092__auto___5318)
{var seq__5305_5319__$1 = temp__4092__auto___5318;if(cljs.core.chunked_seq_QMARK_.call(null,seq__5305_5319__$1))
{var c__3939__auto___5320 = cljs.core.chunk_first.call(null,seq__5305_5319__$1);{
var G__5321 = cljs.core.chunk_rest.call(null,seq__5305_5319__$1);
var G__5322 = c__3939__auto___5320;
var G__5323 = cljs.core.count.call(null,c__3939__auto___5320);
var G__5324 = 0;
seq__5305_5309 = G__5321;
chunk__5306_5310 = G__5322;
count__5307_5311 = G__5323;
i__5308_5312 = G__5324;
continue;
}
} else
{var node_5325 = cljs.core.first.call(null,seq__5305_5319__$1);goog.dom.classes.remove(node_5325,class$);
{
var G__5326 = cljs.core.next.call(null,seq__5305_5319__$1);
var G__5327 = null;
var G__5328 = 0;
var G__5329 = 0;
seq__5305_5309 = G__5326;
chunk__5306_5310 = G__5327;
count__5307_5311 = G__5328;
i__5308_5312 = G__5329;
continue;
}
}
} else
{}
}
break;
}
return content;
});
/**
* Toggles the specified CSS class from each node in the content.
*/
domina.toggle_class_BANG_ = (function toggle_class_BANG_(content,class$){var seq__5334_5338 = cljs.core.seq.call(null,domina.nodes.call(null,content));var chunk__5335_5339 = null;var count__5336_5340 = 0;var i__5337_5341 = 0;while(true){
if((i__5337_5341 < count__5336_5340))
{var node_5342 = cljs.core._nth.call(null,chunk__5335_5339,i__5337_5341);goog.dom.classes.toggle(node_5342,class$);
{
var G__5343 = seq__5334_5338;
var G__5344 = chunk__5335_5339;
var G__5345 = count__5336_5340;
var G__5346 = (i__5337_5341 + 1);
seq__5334_5338 = G__5343;
chunk__5335_5339 = G__5344;
count__5336_5340 = G__5345;
i__5337_5341 = G__5346;
continue;
}
} else
{var temp__4092__auto___5347 = cljs.core.seq.call(null,seq__5334_5338);if(temp__4092__auto___5347)
{var seq__5334_5348__$1 = temp__4092__auto___5347;if(cljs.core.chunked_seq_QMARK_.call(null,seq__5334_5348__$1))
{var c__3939__auto___5349 = cljs.core.chunk_first.call(null,seq__5334_5348__$1);{
var G__5350 = cljs.core.chunk_rest.call(null,seq__5334_5348__$1);
var G__5351 = c__3939__auto___5349;
var G__5352 = cljs.core.count.call(null,c__3939__auto___5349);
var G__5353 = 0;
seq__5334_5338 = G__5350;
chunk__5335_5339 = G__5351;
count__5336_5340 = G__5352;
i__5337_5341 = G__5353;
continue;
}
} else
{var node_5354 = cljs.core.first.call(null,seq__5334_5348__$1);goog.dom.classes.toggle(node_5354,class$);
{
var G__5355 = cljs.core.next.call(null,seq__5334_5348__$1);
var G__5356 = null;
var G__5357 = 0;
var G__5358 = 0;
seq__5334_5338 = G__5355;
chunk__5335_5339 = G__5356;
count__5336_5340 = G__5357;
i__5337_5341 = G__5358;
continue;
}
}
} else
{}
}
break;
}
return content;
});
/**
* Returns a seq of all the CSS classes currently applied to a node. Assumes content is a single node.
*/
domina.classes = (function classes(content){return cljs.core.seq.call(null,goog.dom.classes.get(domina.single_node.call(null,content)));
});
/**
* Sets the class attribute of the content nodes to classes, which can
* be either a class attribute string or a seq of classname strings.
*/
domina.set_classes_BANG_ = (function set_classes_BANG_(content,classes){var classes_5367__$1 = ((cljs.core.coll_QMARK_.call(null,classes))?clojure.string.join.call(null," ",classes):classes);var seq__5363_5368 = cljs.core.seq.call(null,domina.nodes.call(null,content));var chunk__5364_5369 = null;var count__5365_5370 = 0;var i__5366_5371 = 0;while(true){
if((i__5366_5371 < count__5365_5370))
{var node_5372 = cljs.core._nth.call(null,chunk__5364_5369,i__5366_5371);goog.dom.classes.set(node_5372,classes_5367__$1);
{
var G__5373 = seq__5363_5368;
var G__5374 = chunk__5364_5369;
var G__5375 = count__5365_5370;
var G__5376 = (i__5366_5371 + 1);
seq__5363_5368 = G__5373;
chunk__5364_5369 = G__5374;
count__5365_5370 = G__5375;
i__5366_5371 = G__5376;
continue;
}
} else
{var temp__4092__auto___5377 = cljs.core.seq.call(null,seq__5363_5368);if(temp__4092__auto___5377)
{var seq__5363_5378__$1 = temp__4092__auto___5377;if(cljs.core.chunked_seq_QMARK_.call(null,seq__5363_5378__$1))
{var c__3939__auto___5379 = cljs.core.chunk_first.call(null,seq__5363_5378__$1);{
var G__5380 = cljs.core.chunk_rest.call(null,seq__5363_5378__$1);
var G__5381 = c__3939__auto___5379;
var G__5382 = cljs.core.count.call(null,c__3939__auto___5379);
var G__5383 = 0;
seq__5363_5368 = G__5380;
chunk__5364_5369 = G__5381;
count__5365_5370 = G__5382;
i__5366_5371 = G__5383;
continue;
}
} else
{var node_5384 = cljs.core.first.call(null,seq__5363_5378__$1);goog.dom.classes.set(node_5384,classes_5367__$1);
{
var G__5385 = cljs.core.next.call(null,seq__5363_5378__$1);
var G__5386 = null;
var G__5387 = 0;
var G__5388 = 0;
seq__5363_5368 = G__5385;
chunk__5364_5369 = G__5386;
count__5365_5370 = G__5387;
i__5366_5371 = G__5388;
continue;
}
}
} else
{}
}
break;
}
return content;
});
/**
* Returns the text of a node. Assumes content is a single node. For consistency across browsers, will always trim whitespace of the beginning and end of the returned text.
*/
domina.text = (function text(content){return goog.string.trim(goog.dom.getTextContent(domina.single_node.call(null,content)));
});
/**
* Sets the text value of all the nodes in the given content.
*/
domina.set_text_BANG_ = (function set_text_BANG_(content,value){var seq__5393_5397 = cljs.core.seq.call(null,domina.nodes.call(null,content));var chunk__5394_5398 = null;var count__5395_5399 = 0;var i__5396_5400 = 0;while(true){
if((i__5396_5400 < count__5395_5399))
{var node_5401 = cljs.core._nth.call(null,chunk__5394_5398,i__5396_5400);goog.dom.setTextContent(node_5401,value);
{
var G__5402 = seq__5393_5397;
var G__5403 = chunk__5394_5398;
var G__5404 = count__5395_5399;
var G__5405 = (i__5396_5400 + 1);
seq__5393_5397 = G__5402;
chunk__5394_5398 = G__5403;
count__5395_5399 = G__5404;
i__5396_5400 = G__5405;
continue;
}
} else
{var temp__4092__auto___5406 = cljs.core.seq.call(null,seq__5393_5397);if(temp__4092__auto___5406)
{var seq__5393_5407__$1 = temp__4092__auto___5406;if(cljs.core.chunked_seq_QMARK_.call(null,seq__5393_5407__$1))
{var c__3939__auto___5408 = cljs.core.chunk_first.call(null,seq__5393_5407__$1);{
var G__5409 = cljs.core.chunk_rest.call(null,seq__5393_5407__$1);
var G__5410 = c__3939__auto___5408;
var G__5411 = cljs.core.count.call(null,c__3939__auto___5408);
var G__5412 = 0;
seq__5393_5397 = G__5409;
chunk__5394_5398 = G__5410;
count__5395_5399 = G__5411;
i__5396_5400 = G__5412;
continue;
}
} else
{var node_5413 = cljs.core.first.call(null,seq__5393_5407__$1);goog.dom.setTextContent(node_5413,value);
{
var G__5414 = cljs.core.next.call(null,seq__5393_5407__$1);
var G__5415 = null;
var G__5416 = 0;
var G__5417 = 0;
seq__5393_5397 = G__5414;
chunk__5394_5398 = G__5415;
count__5395_5399 = G__5416;
i__5396_5400 = G__5417;
continue;
}
}
} else
{}
}
break;
}
return content;
});
/**
* Returns the value of a node (presumably a form field). Assumes content is a single node.
*/
domina.value = (function value(content){return goog.dom.forms.getValue(domina.single_node.call(null,content));
});
/**
* Sets the value of all the nodes (presumably form fields) in the given content.
*/
domina.set_value_BANG_ = (function set_value_BANG_(content,value){var seq__5422_5426 = cljs.core.seq.call(null,domina.nodes.call(null,content));var chunk__5423_5427 = null;var count__5424_5428 = 0;var i__5425_5429 = 0;while(true){
if((i__5425_5429 < count__5424_5428))
{var node_5430 = cljs.core._nth.call(null,chunk__5423_5427,i__5425_5429);goog.dom.forms.setValue(node_5430,value);
{
var G__5431 = seq__5422_5426;
var G__5432 = chunk__5423_5427;
var G__5433 = count__5424_5428;
var G__5434 = (i__5425_5429 + 1);
seq__5422_5426 = G__5431;
chunk__5423_5427 = G__5432;
count__5424_5428 = G__5433;
i__5425_5429 = G__5434;
continue;
}
} else
{var temp__4092__auto___5435 = cljs.core.seq.call(null,seq__5422_5426);if(temp__4092__auto___5435)
{var seq__5422_5436__$1 = temp__4092__auto___5435;if(cljs.core.chunked_seq_QMARK_.call(null,seq__5422_5436__$1))
{var c__3939__auto___5437 = cljs.core.chunk_first.call(null,seq__5422_5436__$1);{
var G__5438 = cljs.core.chunk_rest.call(null,seq__5422_5436__$1);
var G__5439 = c__3939__auto___5437;
var G__5440 = cljs.core.count.call(null,c__3939__auto___5437);
var G__5441 = 0;
seq__5422_5426 = G__5438;
chunk__5423_5427 = G__5439;
count__5424_5428 = G__5440;
i__5425_5429 = G__5441;
continue;
}
} else
{var node_5442 = cljs.core.first.call(null,seq__5422_5436__$1);goog.dom.forms.setValue(node_5442,value);
{
var G__5443 = cljs.core.next.call(null,seq__5422_5436__$1);
var G__5444 = null;
var G__5445 = 0;
var G__5446 = 0;
seq__5422_5426 = G__5443;
chunk__5423_5427 = G__5444;
count__5424_5428 = G__5445;
i__5425_5429 = G__5446;
continue;
}
}
} else
{}
}
break;
}
return content;
});
/**
* Returns the innerHTML of a node. Assumes content is a single node.
*/
domina.html = (function html(content){return domina.single_node.call(null,content).innerHTML;
});
domina.replace_children_BANG_ = (function replace_children_BANG_(content,inner_content){return domina.append_BANG_.call(null,domina.destroy_children_BANG_.call(null,content),inner_content);
});
domina.set_inner_html_BANG_ = (function set_inner_html_BANG_(content,html_string){var allows_inner_html_QMARK_ = cljs.core.not.call(null,cljs.core.re_find.call(null,domina.re_no_inner_html,html_string));var leading_whitespace_QMARK_ = cljs.core.re_find.call(null,domina.re_leading_whitespace,html_string);var tag_name = [cljs.core.str(cljs.core.second.call(null,cljs.core.re_find.call(null,domina.re_tag_name,html_string)))].join('').toLowerCase();var special_tag_QMARK_ = cljs.core.contains_QMARK_.call(null,domina.wrap_map,tag_name);if(cljs.core.truth_((function (){var and__3213__auto__ = allows_inner_html_QMARK_;if(and__3213__auto__)
{var and__3213__auto____$1 = (function (){var or__3222__auto__ = domina.support.leading_whitespace_QMARK_;if(cljs.core.truth_(or__3222__auto__))
{return or__3222__auto__;
} else
{return cljs.core.not.call(null,leading_whitespace_QMARK_);
}
})();if(cljs.core.truth_(and__3213__auto____$1))
{return !(special_tag_QMARK_);
} else
{return and__3213__auto____$1;
}
} else
{return and__3213__auto__;
}
})()))
{var value_5457 = clojure.string.replace.call(null,html_string,domina.re_xhtml_tag,"<$1></$2>");try{var seq__5453_5458 = cljs.core.seq.call(null,domina.nodes.call(null,content));var chunk__5454_5459 = null;var count__5455_5460 = 0;var i__5456_5461 = 0;while(true){
if((i__5456_5461 < count__5455_5460))
{var node_5462 = cljs.core._nth.call(null,chunk__5454_5459,i__5456_5461);node_5462.innerHTML = value_5457;
{
var G__5463 = seq__5453_5458;
var G__5464 = chunk__5454_5459;
var G__5465 = count__5455_5460;
var G__5466 = (i__5456_5461 + 1);
seq__5453_5458 = G__5463;
chunk__5454_5459 = G__5464;
count__5455_5460 = G__5465;
i__5456_5461 = G__5466;
continue;
}
} else
{var temp__4092__auto___5467 = cljs.core.seq.call(null,seq__5453_5458);if(temp__4092__auto___5467)
{var seq__5453_5468__$1 = temp__4092__auto___5467;if(cljs.core.chunked_seq_QMARK_.call(null,seq__5453_5468__$1))
{var c__3939__auto___5469 = cljs.core.chunk_first.call(null,seq__5453_5468__$1);{
var G__5470 = cljs.core.chunk_rest.call(null,seq__5453_5468__$1);
var G__5471 = c__3939__auto___5469;
var G__5472 = cljs.core.count.call(null,c__3939__auto___5469);
var G__5473 = 0;
seq__5453_5458 = G__5470;
chunk__5454_5459 = G__5471;
count__5455_5460 = G__5472;
i__5456_5461 = G__5473;
continue;
}
} else
{var node_5474 = cljs.core.first.call(null,seq__5453_5468__$1);node_5474.innerHTML = value_5457;
{
var G__5475 = cljs.core.next.call(null,seq__5453_5468__$1);
var G__5476 = null;
var G__5477 = 0;
var G__5478 = 0;
seq__5453_5458 = G__5475;
chunk__5454_5459 = G__5476;
count__5455_5460 = G__5477;
i__5456_5461 = G__5478;
continue;
}
}
} else
{}
}
break;
}
}catch (e5452){if((e5452 instanceof Error))
{var e_5479 = e5452;domina.replace_children_BANG_.call(null,content,value_5457);
} else
{if(new cljs.core.Keyword(null,"else","else",1017020587))
{throw e5452;
} else
{}
}
}} else
{domina.replace_children_BANG_.call(null,content,html_string);
}
return content;
});
/**
* Sets the innerHTML value for all the nodes in the given content.
*/
domina.set_html_BANG_ = (function set_html_BANG_(content,inner_content){if(typeof inner_content === 'string')
{return domina.set_inner_html_BANG_.call(null,content,inner_content);
} else
{return domina.replace_children_BANG_.call(null,content,inner_content);
}
});
/**
* Returns data associated with a node for a given key. Assumes
* content is a single node. If the bubble parameter is set to true,
* will search parent nodes if the key is not found.
*/
domina.get_data = (function() {
var get_data = null;
var get_data__2 = (function (node,key){return get_data.call(null,node,key,false);
});
var get_data__3 = (function (node,key,bubble){var m = domina.single_node.call(null,node).__domina_data;var value = (cljs.core.truth_(m)?cljs.core.get.call(null,m,key):null);if(cljs.core.truth_((function (){var and__3213__auto__ = bubble;if(cljs.core.truth_(and__3213__auto__))
{return (value == null);
} else
{return and__3213__auto__;
}
})()))
{var temp__4092__auto__ = domina.single_node.call(null,node).parentNode;if(cljs.core.truth_(temp__4092__auto__))
{var parent = temp__4092__auto__;return get_data.call(null,parent,key,true);
} else
{return null;
}
} else
{return value;
}
});
get_data = function(node,key,bubble){
switch(arguments.length){
case 2:
return get_data__2.call(this,node,key);
case 3:
return get_data__3.call(this,node,key,bubble);
}
throw(new Error('Invalid arity: ' + arguments.length));
};
get_data.cljs$core$IFn$_invoke$arity$2 = get_data__2;
get_data.cljs$core$IFn$_invoke$arity$3 = get_data__3;
return get_data;
})()
;
/**
* Sets a data on the node for a given key. Assumes content is a
* single node. Data should be ClojureScript values and data structures
* only; using other objects as data may result in memory leaks on some
* browsers.
*/
domina.set_data_BANG_ = (function set_data_BANG_(node,key,value){var m = (function (){var or__3222__auto__ = domina.single_node.call(null,node).__domina_data;if(cljs.core.truth_(or__3222__auto__))
{return or__3222__auto__;
} else
{return cljs.core.PersistentArrayMap.EMPTY;
}
})();return domina.single_node.call(null,node).__domina_data = cljs.core.assoc.call(null,m,key,value);
});
/**
* Takes a two-arg function, a reference DomContent and new
* DomContent. Applies the function for each reference / content
* combination. Uses clones of the new content for each additional
* parent after the first.
*/
domina.apply_with_cloning = (function apply_with_cloning(f,parent_content,child_content){var parents = domina.nodes.call(null,parent_content);var children = domina.nodes.call(null,child_content);var first_child = (function (){var frag = document.createDocumentFragment();var seq__5486_5490 = cljs.core.seq.call(null,children);var chunk__5487_5491 = null;var count__5488_5492 = 0;var i__5489_5493 = 0;while(true){
if((i__5489_5493 < count__5488_5492))
{var child_5494 = cljs.core._nth.call(null,chunk__5487_5491,i__5489_5493);frag.appendChild(child_5494);
{
var G__5495 = seq__5486_5490;
var G__5496 = chunk__5487_5491;
var G__5497 = count__5488_5492;
var G__5498 = (i__5489_5493 + 1);
seq__5486_5490 = G__5495;
chunk__5487_5491 = G__5496;
count__5488_5492 = G__5497;
i__5489_5493 = G__5498;
continue;
}
} else
{var temp__4092__auto___5499 = cljs.core.seq.call(null,seq__5486_5490);if(temp__4092__auto___5499)
{var seq__5486_5500__$1 = temp__4092__auto___5499;if(cljs.core.chunked_seq_QMARK_.call(null,seq__5486_5500__$1))
{var c__3939__auto___5501 = cljs.core.chunk_first.call(null,seq__5486_5500__$1);{
var G__5502 = cljs.core.chunk_rest.call(null,seq__5486_5500__$1);
var G__5503 = c__3939__auto___5501;
var G__5504 = cljs.core.count.call(null,c__3939__auto___5501);
var G__5505 = 0;
seq__5486_5490 = G__5502;
chunk__5487_5491 = G__5503;
count__5488_5492 = G__5504;
i__5489_5493 = G__5505;
continue;
}
} else
{var child_5506 = cljs.core.first.call(null,seq__5486_5500__$1);frag.appendChild(child_5506);
{
var G__5507 = cljs.core.next.call(null,seq__5486_5500__$1);
var G__5508 = null;
var G__5509 = 0;
var G__5510 = 0;
seq__5486_5490 = G__5507;
chunk__5487_5491 = G__5508;
count__5488_5492 = G__5509;
i__5489_5493 = G__5510;
continue;
}
}
} else
{}
}
break;
}
return frag;
})();var other_children = cljs.core.doall.call(null,cljs.core.repeatedly.call(null,(cljs.core.count.call(null,parents) - 1),((function (parents,children,first_child){
return (function (){return first_child.cloneNode(true);
});})(parents,children,first_child))
));if(cljs.core.seq.call(null,parents))
{f.call(null,cljs.core.first.call(null,parents),first_child);
return cljs.core.doall.call(null,cljs.core.map.call(null,(function (p1__5480_SHARP_,p2__5481_SHARP_){return f.call(null,p1__5480_SHARP_,p2__5481_SHARP_);
}),cljs.core.rest.call(null,parents),other_children));
} else
{return null;
}
});
domina.lazy_nl_via_item = (function() {
var lazy_nl_via_item = null;
var lazy_nl_via_item__1 = (function (nl){return lazy_nl_via_item.call(null,nl,0);
});
var lazy_nl_via_item__2 = (function (nl,n){if((n < nl.length))
{return (new cljs.core.LazySeq(null,(function (){return cljs.core.cons.call(null,nl.item(n),lazy_nl_via_item.call(null,nl,(n + 1)));
}),null,null));
} else
{return null;
}
});
lazy_nl_via_item = function(nl,n){
switch(arguments.length){
case 1:
return lazy_nl_via_item__1.call(this,nl);
case 2:
return lazy_nl_via_item__2.call(this,nl,n);
}
throw(new Error('Invalid arity: ' + arguments.length));
};
lazy_nl_via_item.cljs$core$IFn$_invoke$arity$1 = lazy_nl_via_item__1;
lazy_nl_via_item.cljs$core$IFn$_invoke$arity$2 = lazy_nl_via_item__2;
return lazy_nl_via_item;
})()
;
domina.lazy_nl_via_array_ref = (function() {
var lazy_nl_via_array_ref = null;
var lazy_nl_via_array_ref__1 = (function (nl){return lazy_nl_via_array_ref.call(null,nl,0);
});
var lazy_nl_via_array_ref__2 = (function (nl,n){if((n < nl.length))
{return (new cljs.core.LazySeq(null,(function (){return cljs.core.cons.call(null,(nl[n]),lazy_nl_via_array_ref.call(null,nl,(n + 1)));
}),null,null));
} else
{return null;
}
});
lazy_nl_via_array_ref = function(nl,n){
switch(arguments.length){
case 1:
return lazy_nl_via_array_ref__1.call(this,nl);
case 2:
return lazy_nl_via_array_ref__2.call(this,nl,n);
}
throw(new Error('Invalid arity: ' + arguments.length));
};
lazy_nl_via_array_ref.cljs$core$IFn$_invoke$arity$1 = lazy_nl_via_array_ref__1;
lazy_nl_via_array_ref.cljs$core$IFn$_invoke$arity$2 = lazy_nl_via_array_ref__2;
return lazy_nl_via_array_ref;
})()
;
/**
* A lazy seq view of a js/NodeList, or other array-like javascript things
*/
domina.lazy_nodelist = (function lazy_nodelist(nl){if(cljs.core.truth_(nl.item))
{return domina.lazy_nl_via_item.call(null,nl);
} else
{return domina.lazy_nl_via_array_ref.call(null,nl);
}
});
domina.array_like_QMARK_ = (function array_like_QMARK_(obj){var and__3213__auto__ = obj;if(cljs.core.truth_(and__3213__auto__))
{var and__3213__auto____$1 = cljs.core.not.call(null,obj.nodeName);if(and__3213__auto____$1)
{return obj.length;
} else
{return and__3213__auto____$1;
}
} else
{return and__3213__auto__;
}
});
/**
* Some versions of IE have things that are like arrays in that they
* respond to .length, but are not arrays nor NodeSets. This returns a
* real sequence view of such objects. If passed an object that is not
* a logical sequence at all, returns a single-item seq containing the
* object.
*/
domina.normalize_seq = (function normalize_seq(list_thing){if((list_thing == null))
{return cljs.core.List.EMPTY;
} else
{if((function (){var G__5512 = list_thing;if(G__5512)
{var bit__3841__auto__ = (G__5512.cljs$lang$protocol_mask$partition0$ & 8388608);if((bit__3841__auto__) || (G__5512.cljs$core$ISeqable$))
{return true;
} else
{if((!G__5512.cljs$lang$protocol_mask$partition0$))
{return cljs.core.native_satisfies_QMARK_.call(null,cljs.core.ISeqable,G__5512);
} else
{return false;
}
}
} else
{return cljs.core.native_satisfies_QMARK_.call(null,cljs.core.ISeqable,G__5512);
}
})())
{return cljs.core.seq.call(null,list_thing);
} else
{if(cljs.core.truth_(domina.array_like_QMARK_.call(null,list_thing)))
{return domina.lazy_nodelist.call(null,list_thing);
} else
{if(new cljs.core.Keyword(null,"default","default",2558708147))
{return cljs.core.seq.call(null,cljs.core.PersistentVector.fromArray([list_thing], true));
} else
{return null;
}
}
}
}
});
(domina.DomContent["_"] = true);
(domina.nodes["_"] = (function (content){if((content == null))
{return cljs.core.List.EMPTY;
} else
{if((function (){var G__5513 = content;if(G__5513)
{var bit__3841__auto__ = (G__5513.cljs$lang$protocol_mask$partition0$ & 8388608);if((bit__3841__auto__) || (G__5513.cljs$core$ISeqable$))
{return true;
} else
{if((!G__5513.cljs$lang$protocol_mask$partition0$))
{return cljs.core.native_satisfies_QMARK_.call(null,cljs.core.ISeqable,G__5513);
} else
{return false;
}
}
} else
{return cljs.core.native_satisfies_QMARK_.call(null,cljs.core.ISeqable,G__5513);
}
})())
{return cljs.core.seq.call(null,content);
} else
{if(cljs.core.truth_(domina.array_like_QMARK_.call(null,content)))
{return domina.lazy_nodelist.call(null,content);
} else
{if(new cljs.core.Keyword(null,"default","default",2558708147))
{return cljs.core.seq.call(null,cljs.core.PersistentVector.fromArray([content], true));
} else
{return null;
}
}
}
}
}));
(domina.single_node["_"] = (function (content){if((content == null))
{return null;
} else
{if((function (){var G__5514 = content;if(G__5514)
{var bit__3841__auto__ = (G__5514.cljs$lang$protocol_mask$partition0$ & 8388608);if((bit__3841__auto__) || (G__5514.cljs$core$ISeqable$))
{return true;
} else
{if((!G__5514.cljs$lang$protocol_mask$partition0$))
{return cljs.core.native_satisfies_QMARK_.call(null,cljs.core.ISeqable,G__5514);
} else
{return false;
}
}
} else
{return cljs.core.native_satisfies_QMARK_.call(null,cljs.core.ISeqable,G__5514);
}
})())
{return cljs.core.first.call(null,content);
} else
{if(cljs.core.truth_(domina.array_like_QMARK_.call(null,content)))
{return content.item(0);
} else
{if(new cljs.core.Keyword(null,"default","default",2558708147))
{return content;
} else
{return null;
}
}
}
}
}));
(domina.DomContent["string"] = true);
(domina.nodes["string"] = (function (s){return cljs.core.doall.call(null,domina.nodes.call(null,domina.string_to_dom.call(null,s)));
}));
(domina.single_node["string"] = (function (s){return domina.single_node.call(null,domina.string_to_dom.call(null,s));
}));
if(cljs.core.truth_((typeof NodeList != 'undefined')))
{NodeList.prototype.cljs$core$ISeqable$ = true;
NodeList.prototype.cljs$core$ISeqable$_seq$arity$1 = (function (nodelist){var nodelist__$1 = this;return domina.lazy_nodelist.call(null,nodelist__$1);
});
NodeList.prototype.cljs$core$IIndexed$ = true;
NodeList.prototype.cljs$core$IIndexed$_nth$arity$2 = (function (nodelist,n){var nodelist__$1 = this;return nodelist__$1.item(n);
});
NodeList.prototype.cljs$core$IIndexed$_nth$arity$3 = (function (nodelist,n,not_found){var nodelist__$1 = this;if((nodelist__$1.length <= n))
{return not_found;
} else
{return cljs.core.nth.call(null,nodelist__$1,n);
}
});
NodeList.prototype.cljs$core$ICounted$ = true;
NodeList.prototype.cljs$core$ICounted$_count$arity$1 = (function (nodelist){var nodelist__$1 = this;return nodelist__$1.length;
});
} else
{}
if(cljs.core.truth_((typeof StaticNodeList != 'undefined')))
{StaticNodeList.prototype.cljs$core$ISeqable$ = true;
StaticNodeList.prototype.cljs$core$ISeqable$_seq$arity$1 = (function (nodelist){var nodelist__$1 = this;return domina.lazy_nodelist.call(null,nodelist__$1);
});
StaticNodeList.prototype.cljs$core$IIndexed$ = true;
StaticNodeList.prototype.cljs$core$IIndexed$_nth$arity$2 = (function (nodelist,n){var nodelist__$1 = this;return nodelist__$1.item(n);
});
StaticNodeList.prototype.cljs$core$IIndexed$_nth$arity$3 = (function (nodelist,n,not_found){var nodelist__$1 = this;if((nodelist__$1.length <= n))
{return not_found;
} else
{return cljs.core.nth.call(null,nodelist__$1,n);
}
});
StaticNodeList.prototype.cljs$core$ICounted$ = true;
StaticNodeList.prototype.cljs$core$ICounted$_count$arity$1 = (function (nodelist){var nodelist__$1 = this;return nodelist__$1.length;
});
} else
{}
if(cljs.core.truth_((typeof HTMLCollection != 'undefined')))
{HTMLCollection.prototype.cljs$core$ISeqable$ = true;
HTMLCollection.prototype.cljs$core$ISeqable$_seq$arity$1 = (function (coll){var coll__$1 = this;return domina.lazy_nodelist.call(null,coll__$1);
});
HTMLCollection.prototype.cljs$core$IIndexed$ = true;
HTMLCollection.prototype.cljs$core$IIndexed$_nth$arity$2 = (function (coll,n){var coll__$1 = this;return coll__$1.item(n);
});
HTMLCollection.prototype.cljs$core$IIndexed$_nth$arity$3 = (function (coll,n,not_found){var coll__$1 = this;if((coll__$1.length <= n))
{return not_found;
} else
{return cljs.core.nth.call(null,coll__$1,n);
}
});
HTMLCollection.prototype.cljs$core$ICounted$ = true;
HTMLCollection.prototype.cljs$core$ICounted$_count$arity$1 = (function (coll){var coll__$1 = this;return coll__$1.length;
});
} else
{}
