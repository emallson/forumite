(ns forumite.test.interface.core-test
  (:require [forumite.interface.core :as core]
            [forumite.interface.board :as board]
            [forumite.interface.thread :as thread]
            [cemerick.cljs.test :as t])
  (:require-macros [cemerick.cljs.test
                    :refer [is deftest with-test run-tests testing test-var]]
                   [dommy.macros :refer [node sel sel1]]))

(deftest test-restore-state-from-path
  (let [path-1 "/blah/blah/read/lol/1/2/3/"
        path-2 "/blah/blah/read/lol/1/"
        path-bad "/blah/blah/read/lol/1/a/"]
    (core/restore-state-from-path path-1)
    (is (= @board/*current-board-id* "1"))
    (is (= @thread/*current-thread-id* "2"))
    (is (= @thread/*current-thread-page-number* 3))

    (core/restore-state-from-path path-2)
    (is (= @board/*current-board-id* "1"))
    (is (= @thread/*current-thread-id* @thread/*current-thread-page-number* nil))

    (core/restore-state-from-path path-bad)
    (is (= @board/*current-board-id* @thread/*current-thread-id* @thread/*current-thread-page-number* nil))))
          
