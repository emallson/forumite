(ns forumite.test.interface.manipulation-test
  (:require [forumite.interface.manipulation :as man]
            [forumite.interface.board :as board]
            [forumite.interface.utils :refer [children?]]
            [dommy.attrs :refer [attr]])
  (:require-macros [cemerick.cljs.test
                    :refer [is deftest with-test run-tests testing test-var]]
                   [dommy.macros :refer [node sel sel1]]))

(deftest test-make-page-html
  (let [page-empty (man/make-page-html 1)
        page-filled (man/make-page-html 2 :body (board/make-thread-html {:name "Test" 
                                  :length 20
                                  :author "Nobody"
                                  :id 9001
                                  :position 1}))]
    (is (not (children? (sel1 page-empty :tbody))))
    (is (= (attr page-empty :data-page) "1"))
    (is (= (attr page-filled :data-page) "2"))
    (is (children? (sel1 page-filled :tbody)))
    (is (> (count (sel page-filled :tr)) 0))))

(deftest test-find-page
  (let [data (node [:div
                    (man/make-page-html 1)
                    (man/make-page-html 3)
                    (man/make-page-html 4)])]
    (is (= (attr (man/find-page data 1) :data-page) "1") "Finding the first page")
    (is (= (attr (man/find-page data 4) :data-page) "4") "Finding the last page")
    (is (= (attr (man/find-page data 3) :data-page) "3") "Finding a middle page")
    (is (nil? (man/find-page data 2)) "Finding a non-existant page")))

(deftest test-remove-page!
  (let [data (node [:div
                    (man/make-page-html 1)
                    (man/make-page-html 3)
                    (man/make-page-html 4)])]
    (is (= (attr (man/remove-page! data 1) :data-page) "1") "Finding the first page")
    (is (nil? (man/find-page data 1)) "First page no longer exists")
    (is (nil? (man/remove-page! data 2)) "Non-existant page doesn't error")))    

(deftest test-find-after
  (let [data (node [:table {:class "page"
                            :data-page 1}
                    [:tr {:data-position 1}]
                    [:tr {:data-position 2}]
                    [:tr {:data-position 4}]
                    [:tr {:data-position 5}]])
        data-paginated (node [:div
                              (man/make-page-html 1)
                              (man/make-page-html 2)
                              (man/make-page-html 4)
                              (man/make-page-html 5)])]
    (is (= (attr (man/find-after data 1) :data-position) "2") "Consecutive positions")
    (is (= (attr (man/find-after data 2) :data-position) "4") "Non-consecutive positions")
    (is (= (attr (man/find-after data 3) :data-position) "4") "Non-consecutive positions - 2")
    (is (= (man/find-after data 6) nil) "Non-existant element")
    (is (= (attr (man/find-after data-paginated 3
                                  :key :data-page 
                                  :selector :table) :data-page) "4") "Different key and selector")))

(deftest test-find-or-add-page
  (let [data (node [:div
                    (man/make-page-html 1)
                    (man/make-page-html 3)])]
    (is (= (attr (man/find-or-add-page data 1) :data-page) "1") "Finding page 1")
    (is (= (count (sel data :table.page)) 2) "Did not add an existing page")
    (is (= (attr (man/find-or-add-page data 2) :data-page) "2") "Adding page 2")
    (is (= (attr (nth (sel data :table.page) 1) :data-page) "2")
        "Page in correct location")))

(deftest test-insert-element
  (let [page-1 (man/make-page-html 1)
        page-3 (man/make-page-html 3)
        container (node [:div page-1 page-3])
        element-1 (board/make-thread-html {:name "Test" 
                                          :length 20
                                          :author "Nobody"
                                          :id 9001
                                          :position 1})
        element-2 (board/make-thread-html {:name "Test" 
                                          :length 20
                                          :author "Nobody"
                                          :id 9002
                                          :position 2})
        element-3 (board/make-thread-html {:name "Test" 
                                          :length 20
                                          :author "Nobody"
                                          :id 9003
                                          :position 3})]
    (is (children? (man/insert-element container element-1 1)) "Insert
    element-1 and check for children")
    (is (= (sel1 container :tr) element-1) "Check that element-1 has been added")
    (is (= (count (sel (man/insert-element container element-3 1) :tr)) 2) 
        "Check that adding a second element works")
    (is (= (nth (sel page-1 :tr) 1) element-3) "Check that the ordering is correct")
    (is (= (count (sel (man/insert-element container element-2 1) :tr)) 3) 
        "Check that adding a third element works")
    (is (= (nth (sel page-1 :tr) 1) element-2) "Check that the ordering is correct")
    (is (= (count (sel (man/insert-element container element-1 3) :tr)) 1) 
        "Check that adding an element to another page works and that each
        element is only added to its own page.")
    (is (= (count (sel (man/insert-element container element-1 2) :tr)) 1) 
        "Check that adding an element to a new page works.")))
