(ns forumite.test.interface.utils-test
  (:require-macros [cemerick.cljs.test
                    :refer [is deftest with-test run-tests testing test-var]]
                   [dommy.macros :refer [node sel sel1]]
                   [atlanis.utils :refer [unless]])
  (:require [cemerick.cljs.test :as t] 
            [forumite.interface.utils :as utils]
            [dommy.attrs :refer [attr]]
            [dommy.core :refer [text]]))

(deftest test-unless
  (let [val (atom false)]
    (unless true
            (reset! val true))
    (is (not @val))
    (unless false
            (reset! val true))
    (is @val)))

(deftest test-child-count
  (let [data-empty (node [:table])
        data-4 (node [:table [:tr] [:tr] [:tr] [:tr]])
        data-nested (node [:table [:tr [:td] [:td]]])]
    (is (= (utils/child-count data-empty) 0))
    (is (= (utils/child-count data-4) 4))
    (is (= (utils/child-count data-nested) 1))))

(deftest test-children?
  (let [data-empty (node [:table])
        data-non-empty (node [:table [:tr]])]
    (is (utils/children? data-non-empty))
    (is (not (utils/children? data-empty)))))

(deftest test-numeric?
  (let [numeric-1 "12340"
        numeric-2 "4"
        non-numeric-1 "arst"
        non-numeric-2 "arst123"
        empty-string ""]
    (is (utils/numeric? numeric-1))
    (is (utils/numeric? numeric-2))
    (is (not (utils/numeric? non-numeric-1)))
    (is (not (utils/numeric? non-numeric-2)))
    (is (not (utils/numeric? empty-string)))))

(deftest test-int?
  (let [int-1 1
        bad-1 NaN
        bad-2 "4"]
    (is (utils/int? int-1) "Check for valid int passing.")
    (is (not (utils/int? bad-1)) "Check that bloody NaN doesn't pass.")
    (is (not (utils/int? bad-2)) "Check that a string doesn't pass.")))

(deftest test-int-or-nil
  (is (= (utils/int-or-nil "2") 2) "Check for conversion success.")
  (is (= (utils/int-or-nil "blah") nil) "Check for failed conversion.")
  (is (= (utils/int-or-nil 2) 2) "Does no-conversion work?")
  (is (= (utils/int-or-nil nil) nil) "Check that nil works"))

(deftest test-int-or-nil?
  (is (utils/int-or-nil? 1) "Check int")
  (is (utils/int-or-nil? nil) "Check nil")
  (is (not (utils/int-or-nil? NaN)) "Check NaN")
  (is (not (utils/int-or-nil? "4")) "Check string"))

(deftest test-numeric-or-nil
  (is (= (utils/numeric-or-nil 1) "1") "Check number")
  (is (= (utils/numeric-or-nil "a") nil) "Check non-numeric")
  (is (= (utils/numeric-or-nil "42") "42") "Check numeric"))

(deftest test-distance-to-bottom
  (let [scrollDiv (node [:div {:style "max-height: 100px; overflow-y: auto;"}
                         [:div {:style "min-height: 300px"}]])]
    (dommy.core/append! (sel1 js/document :body) scrollDiv)
    (set! (.-scrollTop scrollDiv) 0)
    (is (= (utils/distance-to-bottom scrollDiv) 200) "Check at start; 200 = 300 - 100")
    (set! (.-scrollTop scrollDiv) 100) ; scroll down by 100 pixels
    (is (= (utils/distance-to-bottom scrollDiv) 100) "Check after scrolling")
    (set! (.-scrollTop scrollDiv) 200) ; scroll down by 100 pixels
    (is (= (utils/distance-to-bottom scrollDiv) 0) "Check after scrolling again")
    (set! (.-scrollTop scrollDiv) 300) ; scroll down by 100 pixels
    (is (= (utils/distance-to-bottom scrollDiv) 0) "Check after scrolling past the end")
    (dommy.core/remove! scrollDiv)))

(deftest test-map-to-url
  (let [root "/providers/test/"]
    (is (= (utils/map-to-url :boards :root root) "/providers/test/boards/"))
    (is (= (utils/map-to-url :board :id 2 :root root) "/providers/test/board/2/1/"))
    (is (= (utils/map-to-url :thread :id 2 :root root :page 3) "/providers/test/thread/2/3/"))
    (is (= (utils/map-to-url :post :id 2 :root root) "/providers/test/post/2/"))
    (is (nil? (utils/map-to-url :invalid)))))
