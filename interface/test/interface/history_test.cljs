(ns forumite.test.interface.history-test
  (:require [forumite.interface.history :as hist])
  (:require-macros [cemerick.cljs.test
                    :refer [is deftest with-test run-tests testing test-var]]))

(deftest test-get-path-prefix
  (let [path-1 "/read/lol/"
        path-2 "/read/lol/1/2/3/"
        path-3 "/read/d3/1/3/2/"]
    (is (= (hist/get-path-prefix path-1) "/read/lol/") "Works without threadspec")
    (is (= (hist/get-path-prefix path-2) "/read/lol/") "Works with threadspec")
    (is (= (hist/get-path-prefix path-3) "/read/d3/") "Works with alphanumeric")))

(deftest test-make-path-token
  (is (= (hist/make-path-token 1 2 3) "1/2/3/") "Works with all defined")
  (is (= (hist/make-path-token 1 2) "") "Empty string with only 2 defined")
  (is (= (hist/make-path-token 1 nil 2) "") "Empty string with a nil mixed in")
  (is (= (hist/make-path-token 1) "1/") "Works with only board defined")
  (is (= (hist/make-path-token 1 nil nil) "1/") "Works with only board defined"))

