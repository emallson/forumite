(ns forumite.test.interface.board-test
  (:require [forumite.interface.board :as board]
            [forumite.interface.utils :refer [child-count]]
            [dommy.attrs :refer [attr]]
            [dommy.core :refer [text]]
            [cemerick.cljs.test :as t])
  (:require-macros [cemerick.cljs.test
                    :refer [is deftest with-test run-tests testing test-var]]
                   [dommy.macros :refer [node sel sel1]]))

(deftest test-handle-load-board
  (let [data {:threads {:1 {:id 1
                            :name "Test 1"
                            :author "Nobody"
                            :position 1
                            :length 20}
                        :2 {:id 2
                            :name "Test 2"
                            :author "No-one"
                            :position 2
                            :length 20}}
              :board_id 2
              :page 1}
        data-2 {:threads {:1 {:id 1
                            :name "Test 1"
                            :author "Nobody"
                            :position 1
                            :length 20}
                          :3 {:id 3
                            :name "Test 3"
                            :author "No-one"
                            :position 2
                            :length 20}}
              :board_id 2
              :page 2}
        container (node [:div])]
    (board/handle-load-board data :container container)
    (is (= @board/*current-board-id* "2") "Correct board id")
    (is (= @board/*current-board-page-number* 1) "Correct page number")
    (is (= (child-count (sel1 container ".page tbody")) 2) "Check child count")
    (is (= @board/*thread-id-list* #{:1 :2}) "Thread ID List contains correct IDs")
    (board/handle-load-board data-2 :container container)
    (is (= @board/*current-board-page-number* 2) "Page number updated")
    (is (= (child-count (second (sel container ".page tbody"))) 1) "2nd page has only unique threads")
    (is (= @board/*thread-id-list* #{:1 :2 :3}) "Thread ID List contains correct IDs")))

(deftest test-make-thread-html
  (let [el (board/make-thread-html {:name "Test" 
                                  :length 20
                                  :author "Nobody"
                                  :id 9001
                                  :position 1})]
    (is (= (.-tagName el) "TR") "Check tag type")
    (is (= (int (attr el :data-length)) 20) "Check data attr: length")
    (is (= (int (attr el :data-id)) 9001) "Check data attr: id")
    (is (= (int (attr el :data-position)) 1) "Check data attr: position")
    (is (= (attr el :data-author) "Nobody") "Check data attr: author")
    (is (= (attr el :data-last-read-page) "1") "Check data attr: last read page")
    (is (= (count (sel el :td)) 2) "Check column count")
    (is (= (text (first (sel el :td))) "Test") "Check name column value")
    (is (= (text (second (sel el :td))) "Nobody") "Check author column value")))
