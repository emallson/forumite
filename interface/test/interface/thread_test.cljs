(ns forumite.test.interface.thread-test
  (:require [forumite.interface.thread :as thread]
            [forumite.interface.utils :refer [child-count]]
            [dommy.core :refer [text]]
            [dommy.attrs :refer [attr]])
  (:require-macros [cemerick.cljs.test
                    :refer [is deftest with-test run-tests testing test-var use-fixtures]]
                   [dommy.macros :refer [node sel sel1]]))


(deftest test-make-post-html
  (let [post (thread/make-post-html {:author "Nobody"
                                  :content "Nothing"
                                  :id 42
                                  :page 1
                                  :position 8})]
    (is (= (.-tagName post) "TR") "Check tag type")
    (is (= (int (attr post :data-id)) 42) "Check data attr: id")
    (is (= (int (attr post :data-position)) 8) "Check data attr: position")
    (is (nil? (attr post :data-page)) "Check that data-page DNE")
    (is (= (text (sel1 post :div.post-author)) "Nobody") "Check author contents")
    (is (= (text (sel1 post :div.post-content)) "Nothing") "Check contents contents")
    (is (not (nil? (sel1 post :div.author-column))) "Check for author-column div")
    (is (not (nil? (sel1 post :img.avatar))) "Check for avatar img")
    (is (not (nil? (sel1 post :div.signature))) "Check for signature div")))


(deftest test-handle-load-thread
  (let [data {:posts {:1 {:author "Nobody"
                          :content "Nothing"
                          :id 1
                          :page 1
                          :position 8}
                      :2 {:author "Nobody"
                          :content "Nothing"
                          :id 2
                          :page 1
                          :position 7}}
              :thread_id 2
              :page 1}
        container (node [:div])]
    (thread/handle-load-thread data :container container :update-url false :profiles? false)
    (is (= @thread/*current-thread-id* "2") "Check thread id")
    (is (= @thread/*current-thread-page-number* 1) "Correct page number")
    (is (= (child-count (sel1 container ".page tbody")) 2) "Check child count")))

(deftest test-update-page-map
  (let [page-map (atom {1 1 2 4 3 1})]
    (thread/update-page-map page-map 4 1) ; test adding a new key
    (is (= (get @page-map 4) 1) "Key added successfully")
    (thread/update-page-map page-map 2 2)
    (is (= (get @page-map 2) 4) "Key not changed when new < existing")
    (thread/update-page-map page-map 1 2)
    (is (= (get @page-map 1) 2) "Key changed")))

(defn page-map-fixture
  "Sets up the *last-read-page-map* variable."
  [f]
  (reset! thread/*last-read-page-map* {})
  (f))

(use-fixtures :once page-map-fixture)
