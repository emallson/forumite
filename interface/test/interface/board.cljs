(ns forumite.test.interface.board-test)

(deftest test-handle-load-board
  (let [data {:threads {:1 {:id 1
                            :name "Test 1"
                            :author "Nobody"
                            :position 1
                            :length 20}
                        :2 {:id 2
                            :name "Test 2"
                            :author "No-one"
                            :position 2
                            :length 20}}
              :board_id 2
              :page 1}
        data-2 {:threads {:1 {:id 1
                            :name "Test 1"
                            :author "Nobody"
                            :position 1
                            :length 20}
                          :3 {:id 3
                            :name "Test 3"
                            :author "No-one"
                            :position 2
                            :length 20}}
              :board_id 2
              :page 2}
        container (node [:div])]
    (core/handle-load-board data :container container)
    (is (= @core/*current-board-id* 2) "Correct board id")
    (is (= @core/*current-board-page-number* 1) "Correct page number")
    (is (= (child-count (sel1 container ".page tbody")) 2) "Check child count")
    (is (= @core/*thread-id-list* #{:1 :2}) "Thread ID List contains correct IDs")
    (core/handle-load-board data-2 :container container)
    (is (= @core/*current-board-page-number* 2) "Page number updated")
    (is (= (child-count (second (sel container ".page tbody"))) 1) "2nd page has only unique threads")
    (is (= @core/*thread-id-list* #{:1 :2 :3}) "Thread ID List contains correct IDs")))
