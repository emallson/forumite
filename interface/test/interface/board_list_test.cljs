(ns forumite.test.interface.board-list-test
  (:require [forumite.interface.utils :refer [child-count]]
            [forumite.interface.board-list :as bl]
            [forumite.interface.settings :as settings]
            [dommy.attrs :refer [attr]]
            [dommy.core :refer [value text fire! append! set-value! listen!]])
  (:require-macros [cemerick.cljs.test
                    :refer [is deftest with-test run-tests testing test-var use-fixtures]]
                   [dommy.macros :refer [node sel sel1]]))


(deftest test-make-board-html
  (let [el (bl/make-board-html {:id 2
                                :name "General Discussion"
                                :position 8})]
    (is (= (.-tagName el) "OPTION") "Check tag type")
    (is (= (int (attr el :data-id)) 2) "Check data attr: id")
    (is (= (int (attr el :data-position)) 8) "Check data attr: position")
    (is (= (int (value el)) 2) "Check value")
    (is (= (text el) "General Discussion"))))


(deftest test-handle-load-board-list
  (let [data {:1 {:id 1 :name "Test 1"}
              :2 {:id 2 :name "General Discussion"}}
        container (node [:select])]
    (bl/handle-load-board-list data :container container :trigger-change false)
    (is (= (child-count container) 2) "Check child count")
    (is (= (attr (first (sel container :option)) :data-id) "1")
        "Check first child")
    (is (= (attr (second (sel container :option)) :data-id) "2")
        "Check 2nd child")))

(deftest test-handle-board-change
  (let [board-list (node [:select
                          (bl/make-board-html {:id 2 :name "GD" :position 1})
                          (bl/make-board-html {:id 3 :name "Not GD" :position 2})])
        result-container (node [:div#threads "Some text"])]
    (append! (sel1 js/document :body) board-list)
    (listen! board-list :change #(bl/handle-board-change % :thread-container result-container))
    (append! (sel1 js/document :body) result-container)
    (set-value! board-list 2)
    (fire! board-list :change)
    (is (empty? (text result-container)))))

(defn provider-root-fixture
  [f]
  (set! settings/provider-root "http://localhost:8080/providers/lol_test/")
  (f))

(use-fixtures :once provider-root-fixture)
