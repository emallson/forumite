(ns forumite.test.interface.profiles-test
  (:require [forumite.interface.profiles :as prof]
            [forumite.interface.thread :as thread]
            [dommy.core :refer [text has-class?]]
            [dommy.attrs :refer [attr]])
  (:require-macros [cemerick.cljs.test
                    :refer [is deftest with-test run-tests testing test-var]]
                   [dommy.macros :refer [node sel sel1]]))

(deftest test-split-usernames
  (let [usernames '("Frosthaven" "MapV20" "Atlanis" "Pryotra")]
    (let [[known unknown] (prof/split-usernames usernames)]
      (is (empty? known) "Shouldn't know any yet.")
      (is (= (count unknown) 4) "All 4 should be unknown"))
    (swap! prof/*user-profiles* merge {:Atlanis nil})
    (let [[known unknown] (prof/split-usernames usernames)]
      (is (and (= (first known) '(:Atlanis nil)) (= (count known) 1))
          "Should know only Atlanis")
      (is (= unknown '("Frosthaven" "MapV20" "Pryotra"))
             "The other 3 should be unknown"))))

(deftest test-set-profile-for-user
  (let [container (node [:table
                         (thread/make-post-html {:author "Nobody"
                                                 :content "Nothing"
                                                 :id 1
                                                 :page 1
                                                 :position 6})
                         (thread/make-post-html {:author "Nobody-2"
                                              :content "Nothing"
                                              :id 2
                                              :page 1
                                              :position 7})
                         (thread/make-post-html {:author "Nobody"
                                              :content "Nothing"
                                              :id 3
                                              :page 1
                                              :position 8})])]
    (prof/set-profile-for-user "Nobody" nil :container container)
    (let [[av-1 av-2 av-3] (sel container :.avatar)
          [sig-1 sig-2 sig-3] (sel container :.signature)]
      (is (= (attr av-1 :src) (attr av-3 :src) prof/*default-avatar-url*)
          "Correct default avatar URL and multiple posts set correctly")
      (is (= (attr av-2 :src) nil) "Empty avatar for the unknown")
      (is (= (text sig-1) (text sig-2) (text sig-3) "") "All signatures remain empty")
      (is (and (has-class? sig-1 "borderless") (has-class? sig-2 "borderless")
               (has-class? sig-3 "borderless")) "All signatures remain borderless"))
    (prof/set-profile-for-user "Nobody-2" {:avatar "http://google.com/something.jpg"
                                           :signature "Test"}
                               :container container)
    (let [[av-1 av-2 av-3] (sel container :.avatar)
          [sig-1 sig-2 sig-3] (sel container :.signature)]
      (is (= (attr av-1 :src) (attr av-3 :src) prof/*default-avatar-url*)
          "Nobody's avatar remains untouched")
      (is (= (attr av-2 :src) "http://google.com/something.jpg")
          "Nobody-2's avatar set correctly")
      (is (= (text sig-1) (text sig-3) "") "Nobody's signature remains blank")
      (is (= (text sig-2) "Test") "Nobody-2's signature set correctly")
      (is (not (has-class? sig-2 "borderless")) "Nobody-2's signature has a border")
      (is (and (has-class? sig-1 "borderless") (has-class? sig-3 "borderless"))
          "Nobody's signatures remain borderless"))))
