(ns forumite.interface.thread
  (:require [dommy.core :refer [append! set-value! listen! unlisten! remove!]]
            [dommy.template :refer [html->nodes]]
            [dommy.attrs :refer [set-attr! attr]]
            [ajax.core :refer [GET]]
            [forumite.interface.history :as history]
            [forumite.interface.profiles :as profiles]
            [forumite.interface.settings :as settings]
            [forumite.interface.utils :refer [int-or-nil? numeric-or-nil? child-count children? make-reply-link 
                                              int-or-nil numeric-or-nil map-to-url distance-to-bottom distance-to-top]]
            [forumite.interface.manipulation :refer [insert-element get-current-page find-page remove-page!]]
            [shoreleave.browser.storage.localstorage :as localstorage])
  (:require-macros [atlanis.utils :refer [unless]]
                   [dommy.macros :refer [sel sel1 deftemplate]]))

(def *current-thread-id* (atom nil))
(def *current-thread-page-number* (atom nil :validator int-or-nil?))

(def *fetch-thread-running* (atom false))

(def local-storage (localstorage/storage))
(def *last-read-page-map* (atom nil))

(deftemplate make-post-html [post]
  [:tr {:data-id (:id post)
        :data-position (:position post)}
   [:td
    [:div.row
     [:div.author-column.col-lg-2.text-center
      [:div.avatar-wrapper [:span.avatar-helper] [:img.avatar]]
      [:div.post-author (:author post)]]
     [:div.content-column.col-lg-10
      [:div.post-content (html->nodes (:content post))]
      [:div.signature.borderless]]]]])

(defn update-page-map
  "Updates the page map to reflect the last-read page."
  [page-map thread-id page]
  (unless (nil? page-map)
    (when (or (nil? (get @page-map thread-id)) (> page (get @page-map thread-id)))
      (swap! page-map assoc-in [thread-id] page))))

(defn handle-load-thread
  "Handles loading thread data from JSON into the container."
  [response & {:keys [container update-url profiles? page-map]
               :or {container (sel1 js/document :#posts)
                    update-url true
                    profiles? true
                    page-map *last-read-page-map*}}]
  (reset! *fetch-thread-running* false)
  (remove-page! container (:page response))
  (doseq [[id post] (:posts response)]
    (insert-element container (make-post-html post) (:page response)))
                                 
  ;; when we are inserting above the current page, adjust the scroll position
  (when (< (:page response) @*current-thread-page-number*)
    (set! (.-scrollTop container)
          (+ (distance-to-top container)
             (.-scrollHeight (find-page container (:page response))))))

  ;; update state
  (reset! *current-thread-id* (numeric-or-nil (:thread_id response)))
  (when (= (child-count container) 1) ; only update the page number if we are adding the first page element
    (reset! *current-thread-page-number* (:page response)))

  (when update-url
    (history/update-url)                ; update url/history
    (set-attr! (sel1 js/document :#reply-link) :href (make-reply-link @*current-thread-id*))) ; TEMP update reply link

  (when profiles?                       ; grab profiles
    (profiles/fetch-profiles (distinct (map #(:author (second %)) (:posts response)))))

  (update-page-map page-map (:thread_id response) (:page response))
  ;; call scrollers!
  (handle-thread-scroll-past-top nil :post-container container))

(defn handle-load-thread-failed
  [{:keys [status status-text]}]
  (reset! *fetch-thread-running* false)
  (.log js/console status status-text)
  (unless (= status 404)
          (.alert js/window "Uh-oh! Couldn't load the post list!")))

(defn fetch-thread
  "Grabs the post list from the server."
  [id page-number]
  (unless (or (nil? id) @*fetch-thread-running*)
          (reset! *fetch-thread-running* true)
          (GET (map-to-url :thread :id id :page page-number)
               {:handler handle-load-thread
                :error-handler handle-load-thread-failed
                :response-format :json
                :keywords? true})))

(defn handle-thread-click
  "Handles a thread click event."
  [event & {:keys [post-container thread-row] :or {post-container (sel1 js/document :#posts)
                                                   thread-row (unless (nil? event) (.-selectedTarget event))}}]
  (set! (.-innerHTML post-container) "")
  (reset! *current-thread-id* (numeric-or-nil (attr thread-row :data-id)))
  (reset! *current-thread-page-number* (or (get @*last-read-page-map* @*current-thread-id*) 1))
  (fetch-thread @*current-thread-id* @*current-thread-page-number*))

(defn handle-thread-scroll-past-bottom
  "Handles scrolling in #posts."
  [event & {:keys [post-container refresh-incomplete?]
            :or {post-container (unless (nil? event) (.-currentTarget event))
                 refresh-incomplete? true}}]
  (when (and (< (distance-to-bottom post-container) settings/minimum-load-distance) (children? post-container))
    (let [page-container (find-page post-container @*current-thread-page-number*)]
      (if (>= (count (sel page-container :tr)) settings/posts-per-page)
        (fetch-thread @*current-thread-id* (inc @*current-thread-page-number*))
        (when refresh-incomplete?
          (fetch-thread @*current-thread-id* @*current-thread-page-number*))))))

(defn handle-thread-scroll-past-top
  "Handles scrolling up in #posts."
  [event & {:keys [post-container]
            :or {post-container (unless (nil? event) (.-currentTarget event))}}]
  (when (and (< (distance-to-top post-container) settings/minimum-load-distance)
             (> @*current-thread-page-number* 1))
    (fetch-thread @*current-thread-id* (dec @*current-thread-page-number*))))

(defn handle-thread-scroll
  [event & {:keys [post-container] :or {post-container (unless (nil? event) (.-currentTarget event))}}]
  (reset! *current-thread-page-number*
          (get-current-page (sel js/document "#posts table") (sel1 js/document :#posts)))
  (history/update-url))

(defn load-last-read-page-map
  [storage]
  (if (or (nil? storage) (nil? (:last-read-page-map storage)))
    {}
    (:last-read-page-map storage)))

(defn handle-unload
  [event]
  (assoc! local-storage :last-read-page-map @*last-read-page-map*)
  nil)

(defn enable-scroll-handlers
  []
  (listen! (sel1 js/document :#posts) :scroll handle-thread-scroll-past-bottom)
  (listen! (sel1 js/document :#posts) :scroll handle-thread-scroll-past-top)
  (listen! (sel1 js/document :#posts) :scroll handle-thread-scroll))

(defn disable-scroll-handlers
  []
  (unlisten! (sel1 js/document :#posts) :scroll handle-thread-scroll-past-bottom)
  (unlisten! (sel1 js/document :#posts) :scroll handle-thread-scroll-past-top)
  (unlisten! (sel1 js/document :#posts) :scroll handle-thread-scroll))

(defn ^:export init
  []
  (enable-scroll-handlers)
  (listen! [(sel1 js/document :#threads) :tr] :click handle-thread-click)
  (listen! js/window :beforeunload handle-unload)

  (reset! *last-read-page-map* (load-last-read-page-map local-storage)))
  
