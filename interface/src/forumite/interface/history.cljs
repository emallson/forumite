(ns forumite.interface.history
  (:require [shoreleave.browser.history :refer [init-history set-token! replace-token!]]))

(def history (init-history))
(def ref-board-id nil)
(def ref-thread-id nil)
(def ref-thread-page-number nil)

(defn make-path-token
  "Constructs a path token from the given values."
  ([board-id] (str board-id "/"))
  ([board-id thread-id] "")
  ([board-id thread-id post-id]
     (if (and (nil? thread-id) (nil? post-id))
       (make-path-token board-id)
       (if (nil? thread-id)
         ""
         (str board-id "/" thread-id "/" post-id "/")))))

(defn get-path-prefix
  "Gets the path prefix for the history object."
  [path]
  (first (re-find #"/read/(.+?)/" path)))

(defn update-url
  []
  (replace-token! history (make-path-token @ref-board-id
                                           @ref-thread-id
                                           @ref-thread-page-number)))

(defn ^:export init
  [board-id thread-id page-number]
  (set! ref-board-id board-id)
  (set! ref-thread-id thread-id)
  (set! ref-thread-page-number page-number)
  ; set up history object
  (.setUseFragment history false)
  (.setPathPrefix history (get-path-prefix (.-pathname (.-location js/window)))))
