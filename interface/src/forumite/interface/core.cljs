(ns forumite.interface.core
  (:require [forumite.interface.settings :as settings]
            [forumite.interface.board-list :as board-list]
            [forumite.interface.board :as board]
            [forumite.interface.thread :as thread]
            [forumite.interface.history :as history]
            [forumite.interface.utils :refer [numeric-or-nil int-or-nil]])
  (:require-macros [atlanis.utils :refer [unless]]))

(defn set-state-variables
  [board-id thread-id page-number]
  (reset! board/*current-board-id* board-id)
  (reset! thread/*current-thread-id* thread-id)
  (reset! thread/*current-thread-page-number* (int-or-nil page-number)))

(defn restore-state-from-path
  "Attempts to restore state using data in the path."
  [path]
  (let [[full board-id thread-id page-number]
        (re-find #"/([\w-]+)/([\w-]+)/([\w-]+)/$" path)]
    (if (nil? board-id)
      (let [[partial board-id] (re-find #"/([\w-]+)/$" path)]
        ; if board-id is still nil, that is what we want to set the state to
        (set-state-variables board-id nil nil))
      (set-state-variables board-id thread-id page-number))))

(defn ^:export init
  "Initializes everything once the page loads."
  []
  (board-list/init)
  (board/init)
  (thread/init)
  (set! settings/provider-root
        (str "/providers/" (last (re-find #"read/(.+?)/" (.-pathname (.-location js/window)))) "/"))
  (history/init board/*current-board-id*
                thread/*current-thread-id*
                thread/*current-thread-page-number*)
  ; now, where were we?
  (restore-state-from-path (.-pathname (.-location js/window)))
  (board-list/fetch-board-list)
  (unless (nil? @thread/*current-thread-page-number*)
     (thread/fetch-thread @thread/*current-thread-id* @thread/*current-thread-page-number*)))
