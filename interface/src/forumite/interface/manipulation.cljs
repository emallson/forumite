(ns forumite.interface.manipulation
  (:require [dommy.attrs :refer [attr]]
            [dommy.core :refer [append! insert-before! remove!]]
            [forumite.interface.utils :refer [int-or-nil]])
  (:require-macros [atlanis.utils :refer [unless]]
                   [dommy.macros :refer [sel sel1 deftemplate]]))

(deftemplate make-page-html [page-num & {:keys [body] :or {body nil}}]
  [:table.page.table.table-striped {:data-page page-num} [:tbody body]])

(defn find-page
  "Finds a page table in the given container which matches the given index"
  [container page]
  (loop [pages (sel container :table.page)]
    (unless (empty? pages)
      (if (= (attr (first pages) :data-page) (str page))
        (first pages)
        (recur (rest pages))))))

(defn remove-page!
  "Finds, removes and returns a page table in the given container. Returns nil if no page is found."
  [container page]
  (let [page-table (find-page container page)]
    (unless (nil? page-table)
            (remove! page-table))
    page-table))

(defn find-after
  "Finds the element that ought to be positioned after the given. `position`
  should be a number. `container` should be a table containing the positioned
  elements. Returns nil if there is no element after."
  [container position & {:keys [key selector] :or {key :data-position selector :tr}}]
  (loop [positions (sel container selector)]
    (unless (empty? positions)
            (if (> (int (attr (first positions) key)) position)
              (first positions)
              (recur (rest positions))))))

(defn find-or-add-page
  "Attempts to find a page table. If one does not exist, it is created and
  added. Returns the page."
  [container page]
  (let [table (find-page container page)]
    (if (nil? table)
      (let [next (find-after container page :key :data-page :selector :table)
            table (make-page-html page)]
        (if (nil? next)
          (append! container table)
          (insert-before! table next))
        table)
      table)))

(defn insert-element
  "Inserts an element into the given container on the given page. If the page
  does not exist, it is created. Should only be used on threads and
  posts. Returns the page table containing the element."
  [container element page]
  (let* [page-table (find-or-add-page container page)
         page-tbody (sel1 page-table :tbody)
         next-element (find-after page-tbody (int (attr element :data-position)))]
        (if (nil? next-element)
          (append! page-tbody element)
          (insert-before! element next-element))
        page-table))

(defn get-current-page
  [pages container]
  (unless (empty? pages)
    (loop [heights (map #(.-scrollHeight %) pages)
           height (+ (.-scrollTop container) (/ (.-offsetHeight container) 2))]
      (if (< (reduce + heights) height)
        (int-or-nil (attr (nth pages (count heights)) :data-page))
        (recur (butlast heights) height)))))
