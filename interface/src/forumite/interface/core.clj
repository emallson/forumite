(ns forumite.interface.core
  (:require [cemerick.austin.repls]
            [cemerick.piggieback]
            [cljs.repl.browser]))

(defn ^:export run-austin []
  (cemerick.austin.repls/exec))

(defn ^:export run-browser []
  (cemerick.piggieback/cljs-repl
    :repl-env (cljs.repl.browser/repl-env :port 9000)))
