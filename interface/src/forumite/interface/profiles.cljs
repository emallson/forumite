(ns forumite.interface.profiles
  (:require [ajax.core :refer [POST]]
            [dommy.core :refer [text set-text!]]
            [dommy.attrs :refer [remove-class! set-attr!]]
            [clojure.set :refer [intersection union difference]])
  (:require-macros [dommy.macros :refer [sel1 sel]]
                   [atlanis.utils :refer [unless]]))

(def *user-profiles* (atom {}))
(def *default-avatar-url* "http://media.atlanis.net/forumite/no-avatar-light.gif")

(defn split-usernames
  "Takes a list of usernames and returns a 2-tuple where the first entry is a
  list of known profiles and the second entry is a list of usernames with
  unknown profiles."
  [users]
  [(map #(list % (% @*user-profiles*))
        (intersection (set (map keyword users)) (set (keys @*user-profiles*))))
   (map name (difference (set (map keyword users)) (set (keys @*user-profiles*))))])

(defn set-profile-for-user
  "Sets the avatar for a given user."
  [user profile & {:keys [container] :or {container (sel1 js/document :#posts)}}]
  (doseq [el (filter #(= (text (sel1 % :.post-author)) user)
                     (sel container :.row))]
    (set-attr! (sel1 el :.avatar) :src (or (:avatar profile)
                                           *default-avatar-url*))
    (unless (nil? (:signature profile))
      (let [sig-div (sel1 el :.signature)]
        (set-text! sig-div (:signature profile))
        (remove-class! sig-div "borderless")))))

(defn set-profiles
  "Wrapper for set-profile-for-user"
  [list]
  (doseq [[user profile] list]
    (set-profile-for-user (name user) profile)))

(defn handle-load-profiles
  [response]
  (set-profiles response)
  (swap! *user-profiles* merge response))

(defn handle-load-profiles-failed
  [{:keys [status status-text]}]
  (.log js/console status status-text))

(defn fetch-profiles
  "Fetches profiles for the given list of users. Users whose profiles have
  already been loaded are taken from a cache rather than being fetched again."
  [users]
  (let [[known-usernames unknown-usernames] (split-usernames users)]
    (unless (empty? unknown-usernames)
      (POST "/user/aliases/lol/"
            {:params {:usernames (.stringify js/JSON (apply array unknown-usernames))}
             :format :raw
             :handler handle-load-profiles
             :error-handler handle-load-profiles-failed
             :response-format :json
             :keywords? true}))
    (set-profiles known-usernames)))
