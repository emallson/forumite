(ns forumite.interface.board
  (:require [dommy.attrs :refer [attr]]
            [dommy.core :refer [listen!]]
            [clojure.set :refer [intersection union]]
            [forumite.interface.manipulation :refer [insert-element]]
            [forumite.interface.utils :refer [numeric-or-nil? numeric-or-nil int-or-nil? int-or-nil map-to-url distance-to-bottom]]
            [forumite.interface.settings :as settings]
            [ajax.core :refer [GET]])
  (:require-macros [dommy.macros :refer [sel1 deftemplate]]
                   [atlanis.utils :refer [unless]]))

(def *current-board-id* (atom nil))
(def *current-board-page-number* (atom nil :validator int-or-nil?))
(def *fetch-board-running* (atom false))
(def *thread-id-list* (atom #{}))

(deftemplate make-thread-html [thread]
  [:tr {:data-length (:length thread)
        :data-id (:id thread)
        :data-author (:author thread)
        :data-position (:position thread)
        :data-last-read-page 1}
   [:td (:name thread)]
   [:td (:author thread)]])

(defn handle-load-board
  "Handles loading thread data from JSON into the container."
  [response & {:keys [container] :or {container (sel1 js/document :#threads)}}]
  (reset! *fetch-board-running* false)
  ; remove already-existing threads from the list
  (let [threads (apply dissoc (:threads response) 
                       (intersection (set (keys (:threads response))) @*thread-id-list*))]
    (swap! *thread-id-list* union (set (keys threads)))
    (doseq [[id thread] threads]
      (insert-element container (make-thread-html thread) (:page response))))
  (reset! *current-board-id* (numeric-or-nil (:board_id response)))
  (reset! *current-board-page-number* (:page response)))

(defn handle-load-board-failed
  [{:keys [status status-text]}]
  (reset! *fetch-board-running* false)
  (.log js/console status status-text)
  (.alert js/window "Uh-oh! Couldn't load the thread list!"))

(defn fetch-board
  "Grabs the thread list of a board from the server."
  [id page-number]
  (unless (or (nil? id) @*fetch-board-running*)
    (reset! *fetch-board-running* true)
    (GET (map-to-url :board :id id :page page-number)
         {:handler handle-load-board
          :error-handler handle-load-board
          :response-format :json
          :keywords? true})))

(defn handle-board-scroll-down
  "Handles scrolling in #threads."
  [event]
  (unless (nil? event)
    (let [thread-container (.-currentTarget event)]
      (when (< (distance-to-bottom thread-container) settings/minimum-load-distance)
        (fetch-board @*current-board-id* (inc @*current-board-page-number*))))))

(defn ^:export init
  []
  (listen! (sel1 js/document :#threads) :scroll handle-board-scroll-down))
