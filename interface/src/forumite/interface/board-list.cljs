(ns forumite.interface.board-list
  (:require [dommy.core :refer [append! listen! value set-value!]]
            [forumite.interface.utils :refer [numeric-or-nil int-or-nil map-to-url]]
            [forumite.interface.board :as board]
            [ajax.core :refer [GET]])
  (:require-macros [dommy.macros :refer [deftemplate sel1 sel]]
                   [atlanis.utils :refer [unless]]))

; don't need a state variable for this because the fetch is guaranteed to be called only once

(deftemplate make-board-html [board]
  [:option {:value (:id board)
            :data-id (:id board)
            :data-position (:position board)}
   (:name board)])

(defn handle-board-change
  "Handles clearing the thread list then loading new threads when a board is selected"
  [event & {:keys [board-container thread-container] :or {board-container (unless (nil? event) (.-target event))
                                                          thread-container (sel1 js/document :#threads)}}]
  ; dommy.core/clear! seems to be missing
  (set! (.-innerHTML thread-container) "")
  (reset! board/*thread-id-list* #{})
  (reset! board/*current-board-id* (value board-container))
  (board/fetch-board @board/*current-board-id* 1))

(defn handle-load-board-list
  "Loads board data from JSON into the container."
  [response & {:keys [container trigger-change] :or {container (sel1 js/document :#boards)
                                                     trigger-change true}}]
  (doseq [[id board] response]
    (append! container (make-board-html board)))
  ; this bit is for restoring the state from URL - or - setting the stage for loading threads
  (if (nil? @board/*current-board-id*)
    (reset! board/*current-board-id* (value container))
    (set-value! container @board/*current-board-id*))
  (when trigger-change
    (handle-board-change nil :board-container container)))

(defn handle-load-board-list-failed
  [{:keys [status status-text]}]
  (.log js/console status status-text)
  (.alert js/window "Uh-oh! Couldn't load the board list!"))

(defn fetch-board-list
  "Grabs the board list from the provider."
  []
  (GET (map-to-url :boards) {:handler handle-load-board-list
                             :error-handler handle-load-board-list-failed
                             :response-format :json
                             :keywords? true}))


(defn ^:export init
  []
  (listen! (sel1 js/document :#boards) :change handle-board-change))
