(ns forumite.interface.utils
  (:require [forumite.interface.settings :as settings]))

(defn child-count
  "Returns the count of child elements of a node."
  [node]
  (.-length (.-childNodes node)))

(defn children? 
  "Returns true if the given node has children."
  [node]
  (not (= (child-count node) 0)))

(defn numeric?
  "Returns true if the given string is numeric."
  [string]
  (if (= (type string) js/String)
    (if (= (count string) 0)
      false
      (= (count (filter #(#{\0 \1 \2 \3 \4 \5 \6 \7 \8 \9} %) string))
         (count string)))
    false))

(defn int?
  "Returns true if the given value is of type Number."
  [value]
  (and (= (type value) js/Number) (not (js/isNaN value))))

(defn int-or-nil
  "Converts the value to an int iff it passes `numeric?`"
  [value]
  (when (or (and (string? value) (numeric? value)) (int? value))
    (int value)))

(defn int-or-nil?
  [value]
  (or (int? value) (nil? value)))

(defn numeric-or-nil?
  [value]
  (or (nil? value) (numeric? value)))

(defn numeric-or-nil
  [value]
  (if (= (type value) js/Number)
    (str value)
    (when (and (= (type value) js/String) (numeric? value))
      value)))

(defn distance-to-bottom
  "Gets the distance of an element's scrollbar from the bottom of the element."
  [el]
  (- (.-scrollHeight el) (+ (.-offsetHeight el) (.-scrollTop el))))

(defn distance-to-top
  "Gets the distance of an element's scrollbar from the top of the element."
  [el]
  (.-scrollTop el))

(defn map-to-url
  "Maps type, id and page to a provider url."
  [type & {:keys [id page root] :or {page 1 root settings/provider-root}}]
  (case type
    :boards (str root "boards/")
    :board (str root "board/" id "/" page "/")
    :thread (str root "thread/" id "/" page "/")
    :post (str root "post/" id "/")
    nil))

(defn make-reply-link
  "This is a temporary function until I get a proper reply system in place."
  [thread-id]
  (str "http://forums.na.leagueoflegends.com/board/newreply.php?do=newreply&noquote=1&t=" thread-id))
