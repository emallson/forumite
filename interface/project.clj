(defproject interface "0.1.0-SNAPSHOT"
  :description "Interface code for forumite"
  :url "http://forumite.atlanis.net/"
  :dependencies [[org.clojure/clojure "1.5.1"]
                 [org.clojure/clojurescript "0.0-2030"]
                 [com.cemerick/piggieback "0.1.2"]
                 [prismatic/dommy "0.1.1"]
                 [cljs-ajax "0.2.2"]
                 [shoreleave/shoreleave-browser "0.3.0"]]
  :main ^:skip-aot forumite.interface.core
  :target-path "target/%s"
  :plugins [[lein-cljsbuild "1.0.0-alpha2"] [com.cemerick/clojurescript.test "0.2.1"]]
  :hooks [leiningen.cljsbuild]
  :cljsbuild {
              :builds {:development {
                                     :source-paths ["src" "test" "brepl"]
                                     :compiler {
                                                :output-to "../static/js/forumite.js"
                                                :output-dir "../static/js/out/development/"
                                                :warnings true
                                                :optimizations :whitespace}}
                       :production {
                                    :source-paths ["src"]
                                    :compiler {
                                               :output-to "../static/js/forumite.min.js"
                                               :output-dir "../static/js/out/production/"
                                               :warnings true
                                               :optimizations :advanced
                                               :source-map "../static/js/forumite.min.js.map"}}}
              :test-commands {"unit-tests" ["phantomjs" :runner
                                            "out/forumite.js"]}}
  :repl-options {:nrepl-middleware [cemerick.piggieback/wrap-cljs-repl]}
  :profiles {:uberjar {:aot :all}
             :dev {:plugins [[com.cemerick/austin "0.1.3"]]}})
